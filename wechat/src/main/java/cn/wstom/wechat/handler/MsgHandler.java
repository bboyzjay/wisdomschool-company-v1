package cn.wstom.wechat.handler;

import cn.wstom.wechat.builder.TextBuilder;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 微信消息处理器
 * @author lk
 * @create 2019/9/10
 **/
@Component
public class MsgHandler extends AbstractHandler {

    @Override
    public WxMpXmlOutMessage handle(WxMpXmlMessage wxMpXmlMessage,
                                    Map<String, Object> map,
                                    WxMpService wxMpService, WxSessionManager wxSessionManager) {

        if (!wxMpXmlMessage.getMsgType().equals(WxConsts.XmlMsgType.EVENT)) {
            //TODO 可以选择将消息保存到本地
        }
        // 微信接收到的消息
        String receivedStr = wxMpXmlMessage.getContent();
        // 返回到微信的消息
        String returnStr = "消息来自：" + wxMpXmlMessage.getFromUser() + "\n内容:" + receivedStr;
        return new TextBuilder().build(returnStr, wxMpXmlMessage, wxMpService);
    }
}
