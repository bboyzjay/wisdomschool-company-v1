package cn.wstom.wechat.handler;

import cn.wstom.wechat.builder.TextBuilder;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 上报地理位置处理器
 * @author lk
 * @create 2019/9/10
 **/
@Component
public class LocationHandler extends AbstractHandler {
    @Override
    public WxMpXmlOutMessage handle(WxMpXmlMessage wxMpXmlMessage,
                                    Map<String, Object> map, WxMpService wxMpService,
                                    WxSessionManager wxSessionManager) {
        if (wxMpXmlMessage.getMsgType().equals(WxConsts.XmlMsgType.LOCATION)) {
            //TODO 接收处理用户发送的地理位置消息
            try {
                String content = "感谢反馈，您的的地理位置已收到！";
                return new TextBuilder().build(content, wxMpXmlMessage, wxMpService);
            } catch (Exception e) {
                this.logger.error("【微信服务】位置消息接收处理失败,e={}", e);
                return null;
            }
        }

        // 地理位置
        this.logger.info("上报地理位置，纬度 : {}，经度 : {}，精度 : {}",
                wxMpXmlMessage.getLatitude(), wxMpXmlMessage.getLongitude(), String.valueOf(wxMpXmlMessage.getPrecision()));

        //TODO  可以将用户地理位置信息保存到本地数据库，以便以后使用
        return null;
    }
}
