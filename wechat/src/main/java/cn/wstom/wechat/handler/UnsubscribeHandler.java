package cn.wstom.wechat.handler;

import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author lk
 * @create 2019/9/10
 **/
@Component
public class UnsubscribeHandler extends AbstractHandler {
    @Override
    public WxMpXmlOutMessage handle(WxMpXmlMessage wxMpXmlMessage,
                                    Map<String, Object> map, WxMpService wxMpService,
                                    WxSessionManager wxSessionManager) {
        String openId = wxMpXmlMessage.getFromUser();
        this.logger.info("【微信服务】取消关注用户 openId: " + openId);
        //TODO 可以更新本地数据库为取消关注状态，或者删除
        return null;
    }
}
