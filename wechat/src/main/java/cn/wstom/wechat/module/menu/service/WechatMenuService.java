package cn.wstom.wechat.module.menu.service;

/**
 * 微信菜单业务
 * @author LK
 * @create 2019/9/23 0:00
 **/
public interface WechatMenuService {
    /**
     * 执行创建菜单
     * @param appId
     * @return
     */
    boolean executeMenu(String appId) throws Exception;
}
