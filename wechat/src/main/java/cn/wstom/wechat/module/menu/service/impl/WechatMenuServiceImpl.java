package cn.wstom.wechat.module.menu.service.impl;

import cn.wstom.wechat.module.menu.service.WechatMenuService;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.bean.menu.WxMenu;
import me.chanjar.weixin.common.bean.menu.WxMenuButton;
import me.chanjar.weixin.mp.api.WxMpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author LK
 * @create 2019/9/23 0:02
 **/
@Service
@Slf4j
public class WechatMenuServiceImpl implements WechatMenuService {


    @Autowired
    private WxMpService wxService;

    @Override
    public boolean executeMenu(String appId) throws Exception {
        log.info("【微信服务】微信菜单：创建，appid={}", appId);
        // 写死
        try {
            // 切换 appId
            wxService.switchover(appId);
            // 主布局菜单
            WxMenu menu = new WxMenu();
            // 微信文档规定一个主布局只能存在 3 个菜单，每个菜单最多 9 个子菜单，多于创建会报错
            // 因为写死，可以定义三个，left、center、right
            WxMenuButton leftButton = new WxMenuButton();
            // 按钮分为 click、view 两个类型,跳转页面使用 view 类型，click 按钮一般多于使用点击后返回图文、文本消息、多级菜单等
            // 左边创建一个 view 按钮
            leftButton.setType(WxConsts.MenuButtonType.VIEW);
            leftButton.setName("左边按钮");
            // 带 oauth2 认证，回调返回用户信息
            leftButton.setUrl(wxService.oauth2buildAuthorizationUrl("http://www.baidu.com",
                    WxConsts.OAuth2Scope.SNSAPI_USERINFO,
                    null));
            // 中间一个 click 按钮
            WxMenuButton centerButton = new WxMenuButton();
            centerButton.setType(WxConsts.MenuButtonType.CLICK);
            centerButton.setName("中间按钮");
            centerButton.setKey("center:click");
            // 添加一个子菜单,菜单类型为 view
            WxMenuButton chileButton = new WxMenuButton();
            chileButton.setType(WxConsts.MenuButtonType.VIEW);
            chileButton.setName("子按钮");
            // 该方式没有认证，一般来讲是需要认证，不然拿不到 openId
            chileButton.setUrl("http://www.qq.com");
            // 把子按钮添加到中间按钮
            centerButton.getSubButtons().add(chileButton);
            // 右边创建一个 click
            WxMenuButton rightButton = new WxMenuButton();
            rightButton.setType(WxConsts.MenuButtonType.CLICK);
            rightButton.setName("右边按钮");
            rightButton.setKey("right:clicked!!!!");
            // 添加所有菜单到主布局
            menu.getButtons().add(leftButton);
            menu.getButtons().add(centerButton);
            menu.getButtons().add(rightButton);
            // 执行创建
            String result = wxService.getMenuService().menuCreate(menu);
            log.info("【微信服务】微信菜单：创建成功，返回结果,e={}", result);
            return true;
        } catch (Exception e) {
            throw new Exception(e);
        }
    }
}
