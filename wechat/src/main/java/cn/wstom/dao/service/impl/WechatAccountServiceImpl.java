package cn.wstom.dao.service.impl;

import cn.wstom.dao.mapper.WechatAccountMapper;
import cn.wstom.dao.pojo.WechatAccount;
import cn.wstom.dao.service.WechatAccountService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.security.auth.login.AccountException;
import javax.security.auth.login.AccountNotFoundException;
import java.util.List;

/**
 *  微信账号业务
 * @author liniec
 * @since 2019年9月27日 15点11分
 */
@Transactional
@Service
public class WechatAccountServiceImpl implements WechatAccountService {

    @Resource
    private WechatAccountMapper wechatAccountMapper;

    @Override
    public List<WechatAccount> selectByExample(WechatAccount wechatAccount) {
        return wechatAccountMapper.selectByExample(wechatAccount);
    }

    @Override
    public WechatAccount selectByPrimaryKey(int id) {
        return wechatAccountMapper.selectByPrimaryKey(id);
    }

    @Override
    public WechatAccount selectByOpenId(String openid) {
        return wechatAccountMapper.selectByOpenId(openid);
    }

    @Override
    public int insertWechatStudent(WechatAccount wechatAccount) {
        if (selectByOpenId(wechatAccount.getOpenid()) != null)
            throw new RuntimeException();
        return wechatAccountMapper.insertWechatStudent(wechatAccount);
    }

    @Override
    public int deleteByOpenId(String openid) {
        return wechatAccountMapper.deleteByOpenId(openid);
    }

    @Override
    public int deleteByPrimaryKey(int id) {
        return wechatAccountMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int deleteByStudentId(String studentId) {
        return wechatAccountMapper.deleteByStudentId(studentId);
    }

    @Override
    public int updateByOpenId(WechatAccount wechatAccount) {
        return wechatAccountMapper.updateByOpenId(wechatAccount);
    }

    @Override
    public int checkStuIdAndDelByOpenId(String openId, String studentId) {
        WechatAccount account = selectByOpenId(openId);
        if (studentId.equals(account.getStudentId())) {
            throw new RuntimeException();
        }
        return wechatAccountMapper.deleteByOpenId(openId);
    }
}
