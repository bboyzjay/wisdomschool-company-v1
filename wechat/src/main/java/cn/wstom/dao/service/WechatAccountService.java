package cn.wstom.dao.service;

import cn.wstom.dao.pojo.WechatAccount;

import java.util.List;

/**
 *  微信账号业务
 * @author liniec
 * @since 2019年9月27日 15点11分
 */
public interface WechatAccountService {
    /**
     *  实体参数查询
     * @param wechatAccount
     * @return
     */
    List<WechatAccount> selectByExample(WechatAccount wechatAccount);

    /**
     *  主键id查询
     * @param id
     * @return
     */
    WechatAccount selectByPrimaryKey(int id);

    /**
     *  openid查询
     * @param openid
     * @return
     */
    WechatAccount selectByOpenId(String openid);

    /**
     *  添加微信账号关联
     * @param wechatAccount
     * @return
     */
    int insertWechatStudent(WechatAccount wechatAccount);

    /**
     *  openid删除关联
     * @param openid
     * @return
     */
    int deleteByOpenId(String openid);

    /**
     * 主键删除
     * @param id
     * @return
     */
    int deleteByPrimaryKey(int id);

    /**
     * 学生id删除
     * @param studentId
     * @return
     */
    int deleteByStudentId(String studentId);

    /**
     * openid进行更新
     */
    int updateByOpenId(WechatAccount wechatAccount);

    /**
     *  通过openId检验studentId并删除关联
     * @param openId
     * @param studentId
     * @return
     */
    int checkStuIdAndDelByOpenId(String openId, String studentId);
}
