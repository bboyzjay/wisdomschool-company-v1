package cn.wstom.system.service.impl;


import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.system.entity.SysDictData;
import cn.wstom.system.mapper.SysDictDataMapper;
import cn.wstom.system.service.SysDictDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 字典 业务层处理
 *
 * @author dws
 */
@Service
public class SysDictDataServiceImpl extends BaseServiceImpl<SysDictDataMapper, SysDictData> implements SysDictDataService {
    @Autowired
    private SysDictDataMapper dictDataMapper;
}
