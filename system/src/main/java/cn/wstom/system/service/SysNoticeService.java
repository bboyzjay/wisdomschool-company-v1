package cn.wstom.system.service;


import cn.wstom.common.base.service.BaseService;
import cn.wstom.system.entity.SysNotice;

/**
 * 公告 服务层
 *
 * @author dws
 */
public interface SysNoticeService extends BaseService<SysNotice> {
}
