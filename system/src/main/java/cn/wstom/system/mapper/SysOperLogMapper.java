package cn.wstom.system.mapper;


import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.system.entity.SysOperLog;

/**
 * 操作日志 数据层
 *
 * @author dws
 */
public interface SysOperLogMapper extends BaseMapper<SysOperLog> {

    /**
     * 清空操作日志
     */
    void cleanOperLog();
}
