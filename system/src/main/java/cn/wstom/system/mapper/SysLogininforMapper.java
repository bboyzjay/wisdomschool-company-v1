package cn.wstom.system.mapper;


import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.system.entity.SysLogininfor;

/**
 * 系统访问日志情况信息 数据层
 *
 * @author dws
 */
public interface SysLogininforMapper extends BaseMapper<SysLogininfor> {

    /**
     * 清空系统登录日志
     */
    int cleanLogininfor();
}
