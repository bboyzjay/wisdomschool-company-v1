package cn.wstom.system.mapper;


import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.system.entity.SysNotice;

/**
 * 公告 数据层
 *
 * @author dws
 */
public interface SysNoticeMapper extends BaseMapper<SysNotice> {
}