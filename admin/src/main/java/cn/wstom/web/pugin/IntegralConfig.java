package cn.wstom.web.pugin;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dws
 * @date 2019/03/28
 */
public class IntegralConfig {
    public static Map<String, String> getCondition() {
        Map<String, String> condition = new HashMap<>(16);
        //阅读文章
        condition.put("cn.wstom.web.controller.front.ArticleController#detail", "article_read");
        //添加文章
        condition.put("cn.wstom.web.controller.front.ArticleController#save", "article_edit");
        //评论文章
        condition.put("cn.wstom.web.controller.front.ArticleController#comment", "article_comment");
        //课程讨论
        condition.put("cn.wstom.web.controller.front.StudentController#comment", "course_comment");
        //观看视频
        condition.put("cn.wstom.web.controller.school.teacher.RecourseController#playVideo", "video_view");
        //下载课件
        condition.put("cn.wstom.web.controller.school.teacher.RecourseController#downloadFile", "download_file");
        //预览课件
        condition.put("cn.wstom.web.controller.school.teacher.RecourseController#review", "file_review");
        //提问
        condition.put("cn.wstom.web.controller.front.QuestionController#addQuestion", "question_editor");
        //回复问题
        condition.put("cn.wstom.web.controller.front.QuestionController#addAnswer", "answer_editor");
        return condition;
    }
}
