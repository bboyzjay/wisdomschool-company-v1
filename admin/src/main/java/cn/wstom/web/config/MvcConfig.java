package cn.wstom.web.config;

import cn.wstom.common.utils.FileUtils;
import cn.wstom.web.hook.ParamsInterceptor;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver;

import java.util.List;

/**
 * mvc配置
 *
 * @author dws
 */
@Configuration
public class MvcConfig implements WebMvcConfigurer {

    private final FastJsonHttpMessageConverter httpMessageConvertersConfig;

    private final StringHttpMessageConverter stringHttpMessageConverter;

    @Autowired
    private ParamsInterceptor paramsInterceptor;

    /**
     * Add intercepter
     *
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(paramsInterceptor).addPathPatterns("/**").excludePathPatterns("/dist/**", "/store/**", "/static/**");
    }

    /**
     * 首页地址
     */
    @Value("${shiro.user.indexUrl}")
    private String indexUrl;

    @Autowired
    public MvcConfig(FastJsonHttpMessageConverter httpMessageConvertersConfig, StringHttpMessageConverter stringHttpMessageConverter) {
        this.httpMessageConvertersConfig = httpMessageConvertersConfig;
        this.stringHttpMessageConverter = stringHttpMessageConverter;
    }

    /**
     * 默认首页的设置，当输入域名是可以自动跳转到默认指定的网页
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("forward:" + indexUrl);
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // 文件上传路径
        registry.addResourceHandler("/storage/**").addResourceLocations("file:" + FileUtils.getBaseDir());

        // swagger配置
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

    /**
     * 路径匹配配置
     *
     * @param configurer
     */
    @Override
    public void configurePathMatch(PathMatchConfigurer configurer) {
        configurer.setUseSuffixPatternMatch(false);
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(stringHttpMessageConverter);
        converters.add(httpMessageConvertersConfig);
    }

    /*private String getStorePath() {
        StringBuilder builder = new StringBuilder("file:");
        builder.append(appContext.getRoot());

        if (!appContext.getRoot().endsWith(File.separator)) {
            builder.append(File.separator);
        }

        builder.append("store/");

        return builder.toString();
    }*/

    /**
     * 添加对jsp的支持
     *
     * @return
     */
    /*@Bean
    public ViewResolver getJspViewResolver() {
        InternalResourceViewResolver internalResourceViewResolver =
                new InternalResourceViewResolver();

        internalResourceViewResolver.setOrder(1);
        return internalResourceViewResolver;
    }
*/

    /**
     * 添加对Freemarker支持
     *
     * @return
     */
    @Bean
    public FreeMarkerViewResolver getFreeMarkerViewResolver() {
        FreeMarkerViewResolver freeMarkerViewResolver = new FreeMarkerViewResolver();
        freeMarkerViewResolver.setCache(true);
        freeMarkerViewResolver.setPrefix("");
        freeMarkerViewResolver.setSuffix(".ftl");
        freeMarkerViewResolver.setRequestContextAttribute("request");
        freeMarkerViewResolver.setOrder(0);
        freeMarkerViewResolver.setContentType("text/html;charset=UTF-8");


        return freeMarkerViewResolver;

    }

}