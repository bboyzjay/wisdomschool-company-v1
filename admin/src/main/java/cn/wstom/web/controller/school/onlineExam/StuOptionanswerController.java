package cn.wstom.web.controller.school.onlineExam;

import cn.wstom.common.annotation.Log;
import cn.wstom.common.base.Data;
import cn.wstom.common.enums.ActionType;
import cn.wstom.main.web.base.BaseController;
import cn.wstom.main.web.page.TableDataInfo;
import cn.wstom.onlineexam.entity.StuOptionanswer;
import cn.wstom.onlineexam.service.StuOptionanswerService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 学生考试答案 信息操作处理
 *
 * @author hzh
 * @date 20190304
 */
@Controller
@RequestMapping("/school/onlineExam/stuOptionanswer")
public class StuOptionanswerController extends BaseController {
    private String prefix = "/school/onlineExam/stuOptionanswer";

    @Autowired
    private StuOptionanswerService stuOptionanswerService;

    @RequiresPermissions("school:onlineExam:stuOptionanswer:view")
    @GetMapping()
    public String toList() {
        return prefix + "list";
    }

    /**
     * 查询学生考试答案列表
     */
    @RequiresPermissions("school:onlineExam:stuOptionanswer:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(StuOptionanswer stuOptionanswer) {
        startPage();
        List<StuOptionanswer> list = stuOptionanswerService.list(stuOptionanswer);
        return wrapTable(list);
    }

    /**
     * 新增学生考试答案
     */
    @GetMapping("/add")
    public String toAdd() {
        return prefix + "/add";
    }

    /**
     * 新增保存学生考试答案
     */
    @RequiresPermissions("school:onlineExam:stuOptionanswer:add")
    @Log(title = "学生考试答案", actionType = ActionType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public Data add(StuOptionanswer stuOptionanswer) throws Exception {
        return toAjax(stuOptionanswerService.save(stuOptionanswer));
    }

    /**
     * 修改学生考试答案
     */
    @GetMapping("/edit/{id}")
    public String toEdit(@PathVariable("id") String id, ModelMap mmap) {
        StuOptionanswer stuOptionanswer = stuOptionanswerService.getById(id);
        mmap.put("stuOptionanswer", stuOptionanswer);
        return prefix + "/edit";
    }

    /**
     * 修改保存学生考试答案
     */
    @RequiresPermissions("school:onlineExam:stuOptionanswer:edit")
    @Log(title = "学生考试答案", actionType = ActionType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public Data edit(StuOptionanswer stuOptionanswer) throws Exception {
        return toAjax(stuOptionanswerService.update(stuOptionanswer));
    }

    /**
     * 删除学生考试答案
     */
    @RequiresPermissions("school:onlineExam:stuOptionanswer:remove")
    @Log(title = "学生考试答案", actionType = ActionType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public Data remove(String ids) throws Exception {
        return toAjax(stuOptionanswerService.removeById(ids));
    }
}
