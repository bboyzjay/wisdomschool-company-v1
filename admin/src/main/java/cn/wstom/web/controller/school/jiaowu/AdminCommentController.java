package cn.wstom.web.controller.school.jiaowu;

import cn.wstom.common.base.Data;
import cn.wstom.common.support.Convert;
import cn.wstom.main.web.base.BaseController;
import cn.wstom.main.web.page.TableDataInfo;
import cn.wstom.square.entity.Deck;
import cn.wstom.square.entity.Reply;
import cn.wstom.square.entity.TopicComment;
import cn.wstom.square.service.DeckService;
import cn.wstom.square.service.ReplyService;
import cn.wstom.square.service.TopicCommentService;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * @author liniec
 * @date 2020/01/20 17:47
 *  admin评论管理
 */
@Controller
@RequestMapping("/jiaowu/comment")
public class AdminCommentController extends BaseController {

    @Autowired
    private ReplyService replyService;
    @Autowired
    private DeckService deckService;
    @Autowired
    private TopicCommentService topicCommentService;

    @ApiOperation("用户评论管理")
    @GetMapping("/list")
    @RequiresPermissions("jiaowu:comment:view")
    public String listView() {
        return "/school/jiaowu/comment/list";
    }

    @ApiOperation("用户评论管理")
    @PostMapping("/list")
    @RequiresPermissions("jiaowu:comment:view")
    @ResponseBody
    public TableDataInfo list(TopicComment comment) {
        startPage();
        List<TopicComment> result = topicCommentService.list(comment);
        return wrapTable(result);
    }

    @ApiOperation("评论删除")
    @PostMapping("/remove")
    @RequiresPermissions("jiaowu:comment:view")
    @ResponseBody
    public Data remove(String ids, String type) throws Exception {
        if (type.equals("reply")) {
            return toAjax(replyService.removeById(ids));
        }
        if (type.equals("deck")) {
            return toAjax(deckService.removeById(ids));
        }
        return Data.error();
    }
}
