package cn.wstom.web.controller.wechat.demo;

import cn.wstom.common.base.Data;
import cn.wstom.main.web.base.BaseController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 微信公众号DEMO控制器
 * @author Liniec
 * @create 2019/9/24 16点26分
 **/
@RestController
@RequestMapping("/wx/demo/{appId}")
@Slf4j
public class WeChatDemoController extends BaseController {

    @GetMapping("/hello")
    public Data helloWorld(@PathVariable String appId) {
        System.out.println("【hello world!】");
        return success(appId + " hello world!");
    }

}
