package cn.wstom.web.controller.school.teacher;

import cn.wstom.common.annotation.Log;
import cn.wstom.common.base.Data;
import cn.wstom.common.config.Global;
import cn.wstom.common.constant.Constants;
import cn.wstom.common.constant.StorageConstants;
import cn.wstom.common.enums.ActionType;
import cn.wstom.common.exception.file.FileNameLimitExceededException;
import cn.wstom.common.utils.FileUtils;
import cn.wstom.common.utils.StringUtil;
import cn.wstom.jiaowu.data.TeacherCourseExamVo;
import cn.wstom.jiaowu.data.TeacherCourseVo;
import cn.wstom.jiaowu.entity.*;
import cn.wstom.jiaowu.service.*;
import cn.wstom.main.exception.ApplicationException;
import cn.wstom.main.util.ShiroUtils;
import cn.wstom.main.util.file.FileUploadUtils;
import cn.wstom.main.web.base.BaseController;
import cn.wstom.main.web.page.TableDataInfo;
import cn.wstom.square.entity.Forum;
import cn.wstom.square.entity.Reply;
import cn.wstom.square.entity.Topic;
import cn.wstom.square.service.ForumService;
import cn.wstom.square.service.ReplyService;
import cn.wstom.square.service.TopicService;
import cn.wstom.system.entity.SysMenu;
import cn.wstom.system.entity.SysUser;
import cn.wstom.system.service.SysMenuService;
import cn.wstom.system.service.SysUserService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.Assert;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.*;

/**
 * 教师课程
 * @author dws
 * @date 2019/02/18
 */
@Controller
@RequestMapping("/teacher/course")
public class CourseController extends BaseController {
    private String prefix = "/school/teacher/course";

    /**
     * 页面操作类型，展示或编辑
     */
    private static final int OPT_TYPE_VIEW = 0;
    private static final int OPT_TYPE_EDIT = 1;

    @Value("${wstom.contextPath}")
    private String contextPath;

    @Resource
    private TopicService topicService;

    private final TeacherCourseService teacherCourseService;
    private final TeacherCourseExamService teacherCourseExamService;
    private final CourseService courseService;
    private final TeacherService teacherService;
    private final SysUserService userService;
    private final IntegralDetailService integralDetailService;
    private final StudentService studentService;

    private final ForumService forumService;

    @Autowired
    private SysMenuService menuService;

    @Autowired
    private ReplyService replySerivce;
    @Autowired
    private ClbumService clbumService;
    @Autowired
    private ClbumCourseService clbumCourseService;


    @Autowired
    public CourseController(TeacherCourseService teacherCourseService,
                            TeacherCourseExamService teacherCourseExamService, CourseService courseService,
                            TeacherService teacherService,
                            SysUserService userService,
                            IntegralDetailService integralDetailService,
                            StudentService studentService, ForumService forumService) {
        this.teacherCourseService = teacherCourseService;
        this.teacherCourseExamService = teacherCourseExamService;
        this.courseService = courseService;
        this.teacherService = teacherService;
        this.userService = userService;
        this.integralDetailService = integralDetailService;
        this.studentService = studentService;
        this.forumService = forumService;
    }

    //prefix + "/list";
    @RequiresPermissions("teacher:course:view")
    @GetMapping("/{cid}")
    public String toList(ModelMap modelMap, @PathVariable String cid) {

        List<SysMenu> menus1 = getSysMenus(cid);

        Map<String, Object> params = new HashMap<>(2);
        params.put("cid", cid);
        params.put("tid", getUser().getUserAttrId());
        TeacherCourseVo tcVo = new TeacherCourseVo();
        TeacherCourse tc = teacherCourseService.getOne(params);
        Course course = courseService.getById(cid);
        modelMap.put("course", course);
        modelMap.put("teacherCourse", tc);
        modelMap.put("menus", menus1);
        modelMap.put("copyrightYear", Global.getCopyrightYear());
        return "/admin/course";
    }

    private List<SysMenu> getSysMenus(@PathVariable String cid) {
        SysUser sysUser = getUser();
        List<SysMenu> menus1 = menuService.selectMenusByUserId_teacher(sysUser.getId());
        menus1.forEach(menus -> {
            menus.getChildren().forEach( c -> {
                c.setUrl(c.getUrl() + "/" + cid);
            });
        });
        return menus1;
    }

    /**
     * 获取我的课程列表
     */
    @RequiresPermissions("teacher:course:view")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list() {
        TeacherCourse tc = new TeacherCourse();
        tc.setTid(getUser().getUserAttrId());
        startPage();
        List<TeacherCourse> list = teacherCourseService.list(tc);
        PageInfo<TeacherCourse> pageInfo = new PageInfo(list);
        List<TeacherCourseVo> tcVo = trans(list);
        return wrapTable(tcVo, pageInfo.getTotal(), pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getPages());
    }

    @RequiresPermissions(("teacher:course:edit"))
    @GetMapping("/edit/{cid}")
    public String toEdit(@PathVariable String cid, ModelMap modelMap) {
        Map<String, Object> params = new HashMap<>(2);
        params.put("tid", getUser().getUserAttrId());
        params.put("cid", cid);
        TeacherCourse teacherCourse = teacherCourseService.getOne(params);
        Assert.notNull(teacherCourse, "非法参数");

        TeacherCourseVo tcVo = new TeacherCourseVo();
        BeanUtils.copyProperties(teacherCourse, tcVo);

        fillCourse(tcVo);
        modelMap.put("teacherCourse", tcVo);
        return prefix + "/edit";
    }
    @RequiresPermissions(("teacher:course:edit"))
    @GetMapping("/editexam/{cid}")
    public String toEditExam(@PathVariable String cid, ModelMap modelMap) {
        Map<String, Object> params = new HashMap<>(2);
        params.put("tid", getUser().getUserAttrId());
        params.put("cid", cid);
        TeacherCourse teacherCourse = teacherCourseService.getOne(params);
        Assert.notNull(teacherCourse, "非法参数");
        TeacherCourseVo tcVo = new TeacherCourseVo();
        TeacherCourse tc = teacherCourseService.getOne(params);
        Assert.notNull(tc, "非法参数！");
        BeanUtils.copyProperties(tc, tcVo);
        Course course = courseService.getById(tcVo.getCid());
        tcVo.setCourse(course);
        modelMap.put("course",course);
        BeanUtils.copyProperties(teacherCourse, tcVo);

        fillCourse(tcVo);
        modelMap.put("teacherCourse", tcVo);
        return prefix + "/examedit";
    }

    @RequiresPermissions(("teacher:course:edit"))
    @GetMapping("/topicManager/{tcid}")
    public String topicManager(@PathVariable String tcid, ModelMap modelMap) {
        Map<String, Object> params = new HashMap<>(2);
        params.put("tid", getUser().getUserAttrId());
        params.put("tcid", tcid);
        TeacherCourse teacherCourse = teacherCourseService.getOne(params);
        Assert.notNull(teacherCourse, "非法参数");

        TeacherCourseVo tcVo = new TeacherCourseVo();
        BeanUtils.copyProperties(teacherCourse, tcVo);

        fillCourse(tcVo);
        modelMap.put("teacherCourse", tcVo);
        return prefix + "/edit";
    }

    /**
     * 修改课程
     */
    @RequiresPermissions("teacher:course:edit")
    @Log(title = "教师课程", actionType = ActionType.UPDATE)
    @PostMapping(value = "/edit", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Data edit(TeacherCourseVo teacherCourseVo, MultipartFile thumbnail) throws Exception {
        String filename = null;
        if (!thumbnail.isEmpty()) {
            try {
                //保存图片
                Data result = FileUploadUtils.upload(StorageConstants.STORAGE_THUMBNAIL, thumbnail, false, FileUtils.allowImage);
                System.out.println(result);
                if (StringUtil.nvl(result.get(Data.RESULT_KEY_CODE).toString(), "").equals(Constants.FAILURE)) {
                    return result;
                }
                filename = (String) result.get(StorageConstants.FILE_ID);
            } catch (FileNameLimitExceededException | IOException | ApplicationException e) {
                return Data.error(e.getMessage());
            }
        }
        TeacherCourse tc = new TeacherCourse();
        BeanUtils.copyProperties(teacherCourseVo, tc);


        tc.setCreateBy(ShiroUtils.getLoginName());
        tc.setThumbnailPath(filename);


        return toAjax(teacherCourseService.update(tc));
    }
    @RequiresPermissions("teacher:course:edit")
    @Log(title = "教师课程", actionType = ActionType.UPDATE)
    @PostMapping(value = "/editexam", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Data editExam(TeacherCourseExamVo teacherCourseExamVo,Integer testPaperOneId) throws Exception {
        TeacherCourseExam tc = new TeacherCourseExam();
        BeanUtils.copyProperties(teacherCourseExamVo, tc);
        tc.setCreateBy(ShiroUtils.getLoginName());
        tc.setTestPaperOneId(testPaperOneId);


        return toAjax(teacherCourseExamService.update(tc));
    }
    /**
     * 获取课程页面
     *
     * @param opt 操作类型 见变量说明
     * @return 课程页面或课程信息路径
     */
    @ApiOperation("课程信息")
    @RequiresPermissions("teacher:course:view")
    @GetMapping("/get/{opt}/{cid}")
    public String course(@PathVariable(required = false) Integer opt, @PathVariable String cid, ModelMap modelMap) {
        Map<String, Object> params = new HashMap<>(2);
        params.put("cid", cid);
        params.put("tid", getUser().getUserAttrId());
        TeacherCourseVo tcVo = new TeacherCourseVo();
        TeacherCourse tc = teacherCourseService.getOne(params);
        Assert.notNull(tc, "非法参数！");
        BeanUtils.copyProperties(tc, tcVo);
        Course course = courseService.getById(tcVo.getCid());
        tcVo.setCourse(course);
        modelMap.put("teacherCourse", tcVo);

        if (opt == OPT_TYPE_EDIT) {
            return prefix + "/info";
        }
        return prefix + "/course";
    }

    /**
     * lzj
     * @param opt
     * @param cid
     * @param modelMap
     * @return
     */
    @ApiOperation("课程信息")
    @RequiresPermissions("teacher:course:view")
    @GetMapping("/getExam/{opt}/{cid}")
    public String courseExam(@PathVariable(required = false) Integer opt, @PathVariable String cid, ModelMap modelMap) {
        Map<String, Object> params = new HashMap<>(2);
        params.put("cid", cid);
        params.put("tid", getUser().getUserAttrId());
        TeacherCourseVo tcVo = new TeacherCourseVo();
        TeacherCourse tc = teacherCourseService.getOne(params);
        Assert.notNull(tc, "非法参数！");
        BeanUtils.copyProperties(tc, tcVo);
        Course course = courseService.getById(tcVo.getCid());
        tcVo.setCourse(course);
        modelMap.put("course",course);
        modelMap.put("teacherCourse", tcVo);

        if (opt == OPT_TYPE_EDIT) {
            return prefix + "/infoexam";
        }
        return prefix + "/course";
    }
    /**
     *  课题内话题评论管理
     * @param cid
     * @return
     */
    @GetMapping("/comment/{cid}")
    @RequiresPermissions("teacher:course:view")
    public String toCourseComment(@PathVariable("cid") String cid, ModelMap modelMap) {
        TeacherCourse t = new TeacherCourse();
        t.setCid(cid);
        TeacherCourse t1 = teacherCourseService.list(t).get(0);
        if (t1 != null) {
            List<Topic> topics = topicService.getCourseAllTopic(t1.getId(), 1);
            modelMap.put("topics", topics);
        }
        return "/school/comment/list";
    }

    @PostMapping("/comment/{cid}")
    @RequiresPermissions("teacher:course:view")
    @ResponseBody
    public String toCourseCommentList(@PathVariable("cid") String cid) {


        return "/school/comment/list";
    }


    @GetMapping("/toStatis/{cid}")
    @Log(title = "学习统计")
    @RequiresPermissions("teacher:course:view")
    public String toCourseStatis(@PathVariable("cid") String cid, ModelMap modelMap) {
        modelMap.addAttribute("cid", cid);
        modelMap.addAttribute("tid", getUser().getUserAttrId());
        TeacherCourse tcI = new TeacherCourse();
        tcI.setCid(cid);
        tcI.setTid(getUser().getUserAttrId());
        TeacherCourse tc = teacherCourseService.list(tcI).get(0);
        modelMap.put("tcId", tc.getId());

        return "/school/teacher/statis/info";
    }

    @GetMapping("/toVideoStatis/{cid}")
    @Log(title = "观看统计")
    @RequiresPermissions("teacher:course:view")
    public String toVideoStatis(@PathVariable("cid") String cid, ModelMap modelMap) {
        TeacherCourse tc = new TeacherCourse();
        tc.setCid(cid);
        tc.setTid(userService.getById(getUserId()).getUserAttrId());
        tc = teacherCourseService.list(tc).get(0);
        modelMap.put("tcid", tc.getId());
        return "/school/teacher/statis/video";
    }

    /**
     * 学习统计 获取班级
     */
    @GetMapping("/findClbumByTcid")
    @RequiresPermissions("teacher:course:view")
    @ResponseBody
    public List<Clbum> findClbums(@RequestParam("tcId") String tcId) {
        ClbumCourse clbumCourse = new ClbumCourse();
        clbumCourse.setTcid(tcId);
        List<ClbumCourse> clbumCourses = clbumCourseService.list(clbumCourse);
        List<Clbum> clbums = new ArrayList<Clbum>();
        for(ClbumCourse c :clbumCourses){
            Clbum index = clbumService.getById(c.getCid());
            clbums.add(index);
        }
        return clbums;
    }

    @GetMapping("/forum/toList/{cid}")
    @RequiresPermissions("teacher:course:view")
    public String toForum(@PathVariable("cid") String cid, ModelMap modelMap, HttpServletRequest request) {
        if (!"".equals(cid)) {
            Map<String, Object> params = new HashMap<>(2);
            params.put("cid", cid);
            params.put("tid", getUser().getUserAttrId());
            TeacherCourse tc = teacherCourseService.getOne(params);
            modelMap.put("tcid", tc.getId());
            modelMap.put("id", cid);
        }
        return "/school/teacher/course/forum";
    }

    @GetMapping("/forum/add")
    @RequiresPermissions("teacher:course:view")
    public String toForumAdd(ModelMap modelMap, HttpServletRequest request) {
        String cid = ServletRequestUtils.getStringParameter(request, "cid", "");
        if (!"".equals(cid)) {
            Map<String, Object> params = new HashMap<>(2);
            params.put("cid", cid);
            params.put("tid", getUser().getUserAttrId());
            TeacherCourse tc = teacherCourseService.getOne(params);
            modelMap.put("tcid", tc.getId());
        }
        return "/school/teacher/course/forumAdd";
    }

    @PostMapping("/forum/list")
    @RequiresPermissions("teacher:course:view")
    @ResponseBody
    public TableDataInfo forumList(HttpServletRequest request, ModelMap modelMap) {
        String cid = ServletRequestUtils.getStringParameter(request, "cid", "");
        Forum forum = new Forum();
        if (!"".equals(cid)) {
            Map<String, Object> params = new HashMap<>(2);
            params.put("cid", cid);
            params.put("tid", getUser().getUserAttrId());
            TeacherCourse tc = teacherCourseService.getOne(params);
            modelMap.put("tcid", tc.getId());
            forum.setTcid(tc.getId());
        }

        startPage();
        List<Forum> list = forumService.list(forum);
        return wrapTable(list);
    }

    @PostMapping("/forum/add")
    @RequiresPermissions("teacher:course:view")
    @ResponseBody
    public Data forumAdd(HttpServletRequest request, Forum forum) throws Exception {
        forum.setCreateBy(getLoginName());
        if (forum.getTcid() == null || "".equals(forum.getTcid())) {
            forum.setTcid(null);
        }
        return toAjax(forumService.save(forum));
    }

    @PostMapping("/forum/remove")
    @RequiresPermissions("teacher:course:view")
    @Log(title = "节的试卷", actionType = ActionType.DELETE)
    @ResponseBody
    public Data forumMove(String ids) throws Exception {
        return toAjax(forumService.removeById(ids));
    }

    /**
     * 评论管理
     */
    @ApiOperation("评论管理")
    @RequiresPermissions("teacher:course:view")
    @GetMapping("/comment/list")
    public String commentList() {
        return "/school/teacher/comment/list";
    }

    @ApiOperation("评论管理")
    @RequiresPermissions("teacher:course:view")
    @PostMapping("/comment/list")
    public TableDataInfo commentList(Reply reply) {
        return wrapTable(replySerivce.list(reply));
    }

    @PostMapping("/weekStatis/{cid}/{tid}")
    @RequiresPermissions("teacher:course:view")
    @ResponseBody
    public TableDataInfo courseWeekStatis(@PathVariable("cid") String cid, @PathVariable String tid) {

        List<SysUser> users = userService.selectStudentByCourseIdAndTeacherId(cid, tid);

        List<IntegralDetail> integralDetails;
        integralDetails = integralDetailService.selectBatchIntegral();
        return wrapTable(integralDetails);
    }

    /**
     * 批量转换teacherVo类型
     *
     * @param teacherCourses 将<code>TeacherCourse</code>类型转为<code>TeacherCourseVo</code>
     * @return <code>TeacherCourseVo</code>类型数据
     */
    private List<TeacherCourseVo> trans(List<TeacherCourse> teacherCourses) {
        List<TeacherCourseVo> tcVos = new LinkedList<>();
        Map<String, Course> courseMap = courseService.map(null);

        teacherCourses.forEach(u -> {
            TeacherCourseVo teacherCourseVo = new TeacherCourseVo();
            BeanUtils.copyProperties(u, teacherCourseVo);
            teacherCourseVo.setCourse(courseMap.get(u.getCid()));
            tcVos.add(teacherCourseVo);
        });
        return tcVos;
    }

    private void fillCourse(TeacherCourseVo teacherCourse) {
        String cid = teacherCourse.getCid();
        Course course = courseService.getById(cid);
        teacherCourse.setCourse(course);
    }

    private void fillTeacher(TeacherCourseVo teacherCourse) {
        String tid = teacherCourse.getTid();
        Teacher teacher = teacherService.getById(tid);
        teacherCourse.setTeacher(teacher);
    }
}
