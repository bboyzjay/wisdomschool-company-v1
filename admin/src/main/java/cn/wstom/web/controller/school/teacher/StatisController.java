package cn.wstom.web.controller.school.teacher;

import cn.wstom.common.constant.Constants;
import cn.wstom.jiaowu.entity.*;
import cn.wstom.jiaowu.service.*;
import cn.wstom.main.web.base.BaseController;
import cn.wstom.main.web.page.TableDataInfo;
import cn.wstom.onlineexam.entity.TestPaper;
import cn.wstom.onlineexam.entity.TestPaperOne;
import cn.wstom.onlineexam.entity.UserExam;
import cn.wstom.onlineexam.entity.UserTest;
import cn.wstom.onlineexam.service.TestPaperOneService;
import cn.wstom.onlineexam.service.TestPaperService;
import cn.wstom.onlineexam.service.UserExamService;
import cn.wstom.onlineexam.service.UserTestService;
import cn.wstom.square.entity.PptChapterUser;
import cn.wstom.square.entity.VideoChapter;
import cn.wstom.square.entity.VideoChapterUser;
import cn.wstom.square.entity.VideoChapterUserVo;
import cn.wstom.square.service.PptChapterUserService;
import cn.wstom.square.service.VideoChapterService;
import cn.wstom.square.service.VideoChapterUserService;
import cn.wstom.system.entity.SysUser;
import cn.wstom.system.service.SysUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.util.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

@Controller
@RequestMapping("/teacher/statis")
public class StatisController extends BaseController {

    @Autowired
    private SysUserService userService;
    @Autowired
    private StudentService studentService;
    @Autowired
    private TeacherCourseService teacherCourseService;
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private TestPaperOneService testPaperOneService;
    @Autowired
    private UserExamService userExamService;
    @Autowired
    private ClbumService clbumService;
    @Autowired
    private UserTestService userTestService;
    @Autowired
    private VideoChapterService videoChapterService;
    @Autowired
    private VideoChapterUserService videoChapterUserService;
    @Autowired
    private TestPaperService testPaperService;
    @Autowired
    private PptChapterUserService pptChapterUserService;
    @Autowired
    private ChapterService chapterService;
    @Autowired
    private RecourseService recourseService;
    @Autowired
    private ChapterResourceService chapterResourceService;
    /**
     * 学习统计
     * @param cid
     * @param tid
     * @param model
     * @return
     */
    @PostMapping("/{cid}/{tid}")
    @RequiresPermissions("teacher:course:view")
    @ResponseBody
    public TableDataInfo courseStatis(@PathVariable("cid") String cid,
                                      @PathVariable("tid") String tid,
                                      SysUser user,
                                      Model model) throws Exception{
        TeacherCourse tcI = new TeacherCourse();
        tcI.setCid(cid);
        tcI.setTid(tid);
        TeacherCourse tc = teacherCourseService.list(tcI).get(0);
        model.addAttribute("tid", tid);
        model.addAttribute("tcId", tc.getId());
        String clbumId = user.getNation();
        String loginName = user.getLoginName();
        if (user.getLoginName() == null || user.getLoginName().equals("")) {
            loginName = null;
        }
        if (user.getNation() == null || user.getNation().equals("")) {
            clbumId = null;
        }

        ChapterResource chapterResource = new ChapterResource();
        chapterResource.setTcId(tc.getId());
        System.out.println("tcId====="+tc.getId());
        List<ChapterResource> crlist = chapterResourceService.list(chapterResource);
        List<String> list=new ArrayList<>();
        crlist.forEach(c->{
            list.add(c.getId());

        });
        System.out.println("list"+list);
        //  课堂班级学生列表
        startPage();
        List<SysUser> users = userService.selectStudentByCourseIdAndTeacherId(cid, tid, loginName, clbumId);
        user.setNation(null);
        users.forEach(u -> {
            u.setPassword("");
            Student student = studentService.getById(u.getUserAttrId());
            Clbum clbum = clbumService.getById(student.getCid());

            AtomicReference<Integer> watchVideoCount = new AtomicReference<>(0);
            VideoChapter vc = new VideoChapter();
            vc.setCourseTeacherId(Integer.valueOf(tc.getId()));
            List<VideoChapter> vcs = videoChapterService.list(vc);
            VideoChapterUser videoUser = new VideoChapterUser();
            videoUser.setUserId(Integer.valueOf(u.getId()));
            vcs.forEach(v -> {
                videoUser.setVideoChapterId(Integer.valueOf(v.getId()));
                if (!videoChapterUserService.list(videoUser).isEmpty()) {
                    watchVideoCount.getAndSet(watchVideoCount.get() + 1);
                }
            });

            //TestPaper Count
            UserTest param = new UserTest();
            param.setcId(cid);
            param.setUserId(u.getId());
            param.setType(Constants.TEST_PAPER_TYPE_CHAPTER);
            List userTests = userTestService.selectListBase(param);


            //PPT
            PptChapterUser pptChapterUser=new PptChapterUser();
            pptChapterUser.setUserId(Integer.valueOf(u.getId()));
            pptChapterUser.setCrids(list);
            List<PptChapterUser> pptChapterUserList=pptChapterUserService.list(pptChapterUser);
            AtomicInteger pptsize= new AtomicInteger();
            pptChapterUserList.forEach(p->{
//                int pptsize2=pptsize;
                pptsize.set(p.getCout() + pptsize.get());
            });

            System.out.println(pptsize);
            //考试
            UserExam param1 = new UserExam();
            param1.setcId(cid);
            param1.setUserId(u.getId());
            param1.setType(Constants.TEST_PAPER_TYPE_CHAPTER);
            List userExams = userExamService.selectListBase(param1);

            Map<String, Object> object = new HashMap<>();
            object.put("clbumName", clbum.getName());
            object.put("questionCount", userTests.size());
            object.put("pptCount", pptsize);
            object.put("questionCount1", userExams.size());
            object.put("watchVideoCount", watchVideoCount);
            u.setParams(object);
        });

        return wrapTable(users);
    }


    @RequiresPermissions("teacher:chapter:view")
    @GetMapping("/showDetail")
    public String toList(@RequestParam("id") String userId, @RequestParam("tcId") String tcId, ModelMap modelMap) {
        modelMap.put("userId", userId);
        modelMap.put("tcId", tcId);
        return "school/teacher/statis/detailPaper";
    }






    @RequiresPermissions("teacher:chapter:view")
    @GetMapping("/showExamDetail")
    public String toExamList(@RequestParam("id") String userId, @RequestParam("tcId") String tcId, ModelMap modelMap) {
        modelMap.put("userId", userId);
        modelMap.put("tcId", tcId);
        return "school/teacher/statis/ExamdetailPaper";
    }

    @RequiresPermissions("teacher:chapter:view")
    @PostMapping("/showDetail/{userId}/{tcId}")
    @ResponseBody
    public TableDataInfo toDetail(@PathVariable("userId") String userId, @PathVariable("tcId") String tcId) throws Exception {
        SysUser stu = sysUserService.getById(userId);
        TeacherCourse tc = teacherCourseService.getById(tcId);
        System.out.println("tc"+tc);
        TestPaper paper = new TestPaper();
        paper.setCoursrId(tc.getCid());
        paper.setCreateId(tc.getTid());
        paper.setType(Constants.TEST_PAPER_TYPE_CHAPTER);
        startPage();
        List<TestPaper> testPapers = testPaperService.list(paper);
        testPapers.forEach(p -> {
            Map<String, Object> params = new HashMap<>();
            params.put("stuNum", stu.getLoginName());
            params.put("stuName", stu.getUserName());
            params.put("userId", stu.getId());
            UserTest ut = new UserTest();
            ut.setcId(tc.getCid());
            ut.setUserId(userId);
            System.out.println(p.getId());
            ut.setTestPaperId(p.getId());
            List uts = userTestService.selectListBase(ut);
            if (!uts.isEmpty()) {
                params.put("userTest", uts.get(0));
            } else {
                params.put("userTest", null);
            }

            p.setParams(params);
        });
        return wrapTable(testPapers);
    }


    @RequiresPermissions("teacher:chapter:view")
    @PostMapping("/showExamDetail/{userId}/{tcId}")
    @ResponseBody
    public TableDataInfo toExamDetail(@PathVariable("userId") String userId, @PathVariable("tcId") String tcId) throws Exception {
        SysUser stu = sysUserService.getById(userId);
        TeacherCourse tc = teacherCourseService.getById(tcId);

        TestPaperOne paper = new TestPaperOne();
        paper.setCoursrId(tc.getCid());
        paper.setCreateId(tc.getTid());
        paper.setType(Constants.TEST_PAPER_TYPE_CHAPTER);
        startPage();
        List<TestPaperOne> testPapers = testPaperOneService.list(paper);

        testPapers.forEach(p -> {
            Map<String, Object> params = new HashMap<>();
            params.put("stuNum", stu.getLoginName());
            params.put("stuName", stu.getUserName());
            params.put("userId", stu.getId());

            UserExam ut = new UserExam();
            ut.setcId(tc.getCid());
            ut.setUserId(userId);
            ut.setTestPaperOneId(p.getId());
            List uts = userExamService.selectListBase(ut);
            if (!uts.isEmpty()) {
                params.put("userTest", uts.get(0));
            } else {
                params.put("userTest", null);
            }

            p.setParams(params);
        });
        return wrapTable(testPapers);
    }
}
