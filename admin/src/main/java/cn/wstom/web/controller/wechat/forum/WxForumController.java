package cn.wstom.web.controller.wechat.forum;
import cn.wstom.common.base.Data;
import cn.wstom.common.constant.UserConstants;
import cn.wstom.common.utils.AtomicIntegerUtil;
import cn.wstom.dao.pojo.WechatAccount;
import cn.wstom.dao.service.WechatAccountService;
import cn.wstom.main.web.base.BaseController;
import cn.wstom.onlineexam.service.TestPaperService;
import cn.wstom.square.entity.*;
import cn.wstom.square.service.*;
import cn.wstom.system.entity.SysUser;
import cn.wstom.system.service.SysUserService;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Controller
@RequestMapping("/wx/forum")
@Slf4j
public class WxForumController extends BaseController{
    private final ForumService forumService;
    private final TopicService topicService;
    private final SysUserService sysUserService;
    private final ReplyService replyService;
    private final DeckService deckService;
    private final ArticleService articleService;
    @Autowired
    TestPaperService testPaperService;
    @Value("${wstom.storageContextPath}")
    private String storageContextPath;
    @Autowired
    private WechatAccountService wechatAccountService;
    @Autowired
    public WxForumController(
                             ForumService forumService,
                             TopicService topicService,
                             ReplyService replyService,
                             DeckService deckService,
                             ArticleService articleService,
                             SysUserService sysUserService
                             ) {
        this.forumService = forumService;
        this.topicService = topicService;
        this.sysUserService = sysUserService;
        this.replyService = replyService;
        this.deckService = deckService;
        this.articleService = articleService;
    }
    @RequestMapping("/index")
    public String index(@RequestParam String openId,String forumid,String sort, ModelMap modelMap)  {
        System.out.println("index控制器获取了openID"+openId);
        System.out.println("index控制器获取了"+forumid);
        List<Forum> forums = null;
        List<Topic> list;
        Map<String, Object> params = Maps.newLinkedHashMap();
        forums = forumService.list(new Forum());
        try {// 判断显示板块方式，0表示全部
            if ("0".equals(forumid)) {
                Topic topic = new Topic();
                params.put("sort", sort);
                topic.setParams(params);
                startPage();
                list = topicService.list(topic);
                list.forEach( t -> {
                    t.setBrowse((long) AtomicIntegerUtil.getInstance(t.getClass(), t.getId(), t.getBrowse()).get());
                });
            } else {
                Forum forum = new Forum();
                forum.setId(forumid);
                Topic topic = new Topic();
                params.put("sort", sort);
                topic.setParams(params);
                startPage();
                topic.setForum(forum);
                list = topicService.list(topic);
            }
        } catch (Exception e) {
            Topic topic = new Topic();
            params.put("sort", 1);
            topic.setParams(params);
            startPage();
            list = topicService.list(topic);
            forumid = "0";
            sort = "1";
        }
        modelMap.put("forumid", forumid);
        modelMap.put("ForumList", forums);
        modelMap.put("pageModel", wrapTable(list));
        modelMap.put("sort", sort);
        modelMap.put("openId",openId);
        return "/wx/forum/index";
    }

    @RequestMapping("/indexlist")
    public String indexlist (HttpServletRequest request, ModelMap modelMap)
    {
        String course = ServletRequestUtils.getStringParameter(request, "course", "1");
        String forumid = ServletRequestUtils.getStringParameter(request, "forumid", "0");
        String openId = ServletRequestUtils.getStringParameter(request, "openId", "0");
        int sort = ServletRequestUtils.getIntParameter(request, "sort", 1);
        // 当前页
        System.out.println("indexlist控制器获取了"+forumid);
        System.out.println("indexlist控制器获取了openID"+openId);
        System.out.println("indexlist控制器获取了"+sort);
        List<Forum> forums = null;
        List<Topic> list;
        Map<String, Object> params = Maps.newLinkedHashMap();
        forums = forumService.list(new Forum());
        try {
            // 判断显示板块方式，0表示全部
            if ("0".equals(forumid)) {
                Topic topic = new Topic();
                params.put("sort", sort);
                topic.setParams(params);
                startPage();
                list = topicService.list(topic);
                /*  Atomic __ */
                list.forEach(t -> {
                    t.setBrowse((long) AtomicIntegerUtil.getInstance(t.getClass(), t.getId(), t.getBrowse()).get());
                });
            } else {
                Forum forum = new Forum();
                forum.setId(forumid);
                Topic topic = new Topic();
                params.put("sort", sort);
                topic.setParams(params);
                startPage();
                topic.setForum(forum);
                list = topicService.list(topic);
            }
        } catch (Exception e) {
            Topic topic = new Topic();
            params.put("sort", 1);
            topic.setParams(params);
            startPage();
            list = topicService.list(topic);
            forumid = "0";
            sort = 1;
        }
        modelMap.put("forumid", forumid);
        modelMap.put("ForumList", forums);
        modelMap.put("pageModel", wrapTable(list));
        modelMap.put("sort", sort);
        modelMap.put("openId",openId);
        return "/wx/forum/index";
    }
    @RequestMapping("/indexlist/toPush")//发布
    public String toPush(ModelMap modelMap,HttpServletRequest request) {
        /*
         * 判断权限
         * 如果是学生用户,就过滤 forumList
         */
        String course = ServletRequestUtils.getStringParameter(request, "course", "1");
        String forumid = ServletRequestUtils.getStringParameter(request, "forumid", "0");
        String openId = ServletRequestUtils.getStringParameter(request, "openId", "0");
        int sort = ServletRequestUtils.getIntParameter(request, "sort", 1);
        WechatAccount account = wechatAccountService.selectByOpenId(openId);
        SysUser sysUser=sysUserService.selectUserByUserAttrId(account.getStudentId());
        List<Forum> forumList = forumService.list(new Forum());
        System.out.println("toPush控制器获取到了用户类型"+sysUser.getUserType());
        System.out.println("toPush控制器获取到了openID"+openId);
        if (sysUser.getUserType() == UserConstants.USER_STUDENT) {
            List<Forum> tempForum = new ArrayList<>();
            modelMap.put("forumList", forumList);
            modelMap.put("forumid", forumid);
            modelMap.put("sort", sort);
            modelMap.put("openId",openId);
        } else {
            modelMap.put("forumList", forumList);
            modelMap.put("forumid", forumid);
            modelMap.put("sort", sort);
            modelMap.put("openId",openId);
        }
        return "/wx/forum/wxpush";
    }

    @RequestMapping("/indexadd")
    @ResponseBody
    public Data indexadd(HttpServletRequest request) throws Exception {
        /*
         * 判断权限
         * 如果是学生用户,就过滤 forumList
         */
        Topic topic = new Topic();
        org.apache.commons.beanutils.BeanUtils.populate(topic, request.getParameterMap());
        String talktitle = ServletRequestUtils.getStringParameter(request, "talktitle");
        String talkcontent = ServletRequestUtils.getStringParameter(request, "talkcontent");
        String forumId = ServletRequestUtils.getStringParameter(request, "forumid");
        String tcid = ServletRequestUtils.getStringParameter(request, "tcid", null);
        String openId = ServletRequestUtils.getStringParameter(request, "openId");
        System.out.println("indexadd控制器获取到了openID"+openId);
        //  topic title 为空
        if (talktitle == null || talktitle.equals("")) {
            return Data.error("标题为空");
        }

        Forum f = forumService.getById(forumId);

        topic.setForum(f);
        if (tcid != null && !"".equals(tcid.trim())) {
            topic.setTcid(tcid);
        }// >> 当前可以直接获取的信息

        WechatAccount account = wechatAccountService.selectByOpenId(openId);
        SysUser sysUser=sysUserService.selectUserByUserAttrId(account.getStudentId());
        System.out.println(sysUser.getUserName());
        topic.setUserType(sysUser.getUserType() + "");
        topic.setIpAddr(request.getRemoteAddr());// IP地址，当前请求中的IP信息
        topic.setThumbsUp(0L);
        topic.setBrowse(0L);
        topic.setContent(talkcontent);
        topic.setTitle(talktitle);
        topic.setCreateName(sysUser.getUserName());
        topic.setCreateBy(sysUser.getId());
        return toAjax(topicService.save(topic));// 保存
    }
    @RequestMapping("/indexdetail/{id}")
    public String indexdetail(ModelMap modelMap, @PathVariable String id, String openId) throws Exception {
        // 准备数据：topic
        Topic topic = topicService.getById(id);
        System.out.println("indexdetail控制器获取了openId："+openId);
        if (!AtomicIntegerUtil.isEmplyInMap(topic.getClass(), id))
            AtomicIntegerUtil.getInstance(topic.getClass(), id, topic.getBrowse());
        topic.setBrowse((long) AtomicIntegerUtil.getInstance(topic.getClass(), id).getAndIncrement());
        modelMap.put("topic", topic);
        //准备被采纳数据：replyList
        Map<String, Object> params = Maps.newLinkedHashMap();
        params.put("adopt", 1);
        params.put("topic", topic);
        params.put("order_by", "r.create_time asc");
        Reply replyAdopt = replyService.getOne(params);
        modelMap.put("replyAdopt", replyAdopt);
        Reply reply = new Reply();
        reply.setAdopt(0);
        reply.setTopic(topic);
        params.clear();
        params.put("orderBy", "create_time ASC");
        reply.setParams(params);
        //准备其他回复：
        startPage();
        List<Reply> pageModel = replyService.list(reply);
        Deck deck = new Deck();
        pageModel.forEach(p -> {
            deck.setReply(p);
            p.setDecks(new HashSet<>(deckService.list(deck)));
        });
        modelMap.put("pageModel", wrapTable(pageModel));
        modelMap.put("openId",openId);
        return "/wx/forum/wxreply";// 保存
    }
    @RequestMapping("/replyadd")
    @ResponseBody
    public Data replyadd(HttpServletRequest request, String replycontent, String topicId) throws Exception {
        /*
         * 判断权限
         * 如果是学生用户,就过滤 forumList
         */
        String tcid = ServletRequestUtils.getStringParameter(request, "tcid", null);
        String openId = ServletRequestUtils.getStringParameter(request, "openId");
        //先维护更新Topic
        System.out.println("replyadd控制器获取了openid："+openId);
        WechatAccount account = wechatAccountService.selectByOpenId(openId);
        SysUser sysUser=sysUserService.selectUserByUserAttrId(account.getStudentId());
        Topic topic = topicService.getById(topicId);
        topic.setReplyCount(topic.getReplyCount() + 1);
        Reply reply = new Reply();
        // 1，封装（已经封装了title, content, faceIcon）
        System.out.println("topicId::" + topicId);
        reply.setTopic(topic);
        if (tcid != null && !"".equals(tcid.trim())) {
            reply.setTcid(tcid);
        }
        reply.setCreateBy(sysUser.getId());
        reply.setUserType(sysUser.getUserType() + "");
        reply.setContent(replycontent);
        reply.setIpAddr(request.getRemoteAddr());
        reply.setThumbsUp(0L);
        reply.setCreateName(sysUser.getUserName());
        // 2，保存
        topic.setLastReply(reply);
        replyService.save(reply);
        topicService.update(topic);
        return Data.success();
    }
    @RequestMapping("/deckadd")
    @ResponseBody
    public Data deckadd(HttpServletRequest request, ModelMap modelMap) throws Exception {
        try {
            String content = ServletRequestUtils.getStringParameter(request, "content");
            String replyId = ServletRequestUtils.getStringParameter(request, "replyId");
            String toUserId = ServletRequestUtils.getStringParameter(request, "toUserId");
            String toUserName = ServletRequestUtils.getStringParameter(request, "toUserName");
            String toUserType = ServletRequestUtils.getStringParameter(request, "toUserType");
            String tcid = ServletRequestUtils.getStringParameter(request, "tcid");
            String openId = ServletRequestUtils.getStringParameter(request, "openId");
            //先维护更新Topic
            System.out.println("deckadd控制器获取了："+openId);
            WechatAccount account = wechatAccountService.selectByOpenId(openId);
            SysUser sysUser=sysUserService.selectUserByUserAttrId(account.getStudentId());
            //准备数据
            Reply reply = replyService.getById(replyId);
            Deck deck = new Deck();
            deck.setContent(content);
            deck.setReply(reply);
            //设置作者的类型
            deck.setUserType(sysUser.getUserType() + "");
            //设置回复对象信息
            deck.setToUserId(toUserId);
            deck.setToUserName(toUserName);
            deck.setToUserType(toUserType);
            if (tcid != null && !"".equals(tcid)) {
                deck.setTcid(tcid);
            }
            deck.setIpAddr(request.getRemoteAddr());
            deck.setThumbsUp(0L);
            return toAjax(deckService.save(deck));
        } catch (Exception e) {
            e.printStackTrace();
            return Data.error();
        }
    }
    /* 回复点赞
     *  - reply 类型 : 2
            * @param replyId
     * @return
             */
    @PostMapping("/replylike")
    @ResponseBody
    public Data replylike(String replyId,String openId) {
        System.out.println("replylike控制器获取了："+openId);
        WechatAccount account = wechatAccountService.selectByOpenId(openId);
        SysUser sysUser=sysUserService.selectUserByUserAttrId(account.getStudentId());
        ArticleVotes articleVotes = new ArticleVotes();
        articleVotes.setUserId(sysUser.getId());
        articleVotes.setInfoType(2);
        articleVotes.setInfoId(replyId);
        String msg = null;
        //  查询是否已点赞
        if (articleService.checkArticleVotes(2, replyId, sysUser.getId())) {
            articleVotes = articleService.findArticleVotes(articleVotes).get(0);

            if (articleVotes.getDig() == 0) {
                articleVotes.setDig(1);
                msg = "+1success";
            } else {
                articleVotes.setDig(0);
                msg = "-1success";
            }
            articleService.updateArticleVotesById(articleVotes);
        } else {
            articleVotes.setDig(1);
            articleService.addArticleVotes(articleVotes);
            msg = "+1success";
        }
        //更新点赞数
        try {
            long digCount = articleService.selectDigCount(2, replyId);
            Reply reply = replyService.getById(replyId);
            reply.setThumbsUp(digCount);
            replyService.update(reply);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Data.success(msg);
    }
    /**
     * 提问点赞
     *  - topic 类型 : 1
     * @param topicId
     * @return
     */
    @PostMapping("/indexlike")
    public Data indexlike(String topicId,String openId) {
        System.out.println("indexlike控制器获取了："+openId);
        WechatAccount account = wechatAccountService.selectByOpenId(openId);
        SysUser sysUser=sysUserService.selectUserByUserAttrId(account.getStudentId());
        ArticleVotes articleVotes = new ArticleVotes();
        articleVotes.setUserId(sysUser.getId());
        articleVotes.setInfoType(1);
        articleVotes.setInfoId(topicId);
        String msg = null;
        //  查询是否已点赞
        if (articleService.checkArticleVotes(1, topicId, sysUser.getId())) {
            articleVotes = articleService.findArticleVotes(articleVotes).get(0);
            if (articleVotes.getDig() == 0) {
                articleVotes.setDig(1);
                msg = "+1success";
            } else {
                articleVotes.setDig(0);
                msg = "-1success";
            }
            articleService.updateArticleVotesById(articleVotes);
        } else {
            articleVotes.setDig(1);
            articleService.addArticleVotes(articleVotes);
            msg = "+1success";
        }
        // 更新点赞数
        try {
            long digCount = articleService.selectDigCount(1, topicId);
            Topic topic = topicService.getById(topicId);
            topic.setThumbsUp(digCount);
            topicService.update(topic);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Data.success(msg);
    }

    @RequestMapping("/courseindexList/{tcid}")
    public String courseindexList(HttpServletRequest request, ModelMap modelMap, @PathVariable String tcid,String openId) throws ServletRequestBindingException {
        // 加载用户信息
        // 论坛的排序方式
        System.out.println("courseindexList控制器获取了："+openId);
        String forumid = ServletRequestUtils.getStringParameter(request, "forumid", "0");
//        String openId = ServletRequestUtils.getStringParameter(request, "openId", "0");
        int sort = ServletRequestUtils.getIntParameter(request, "sort", 1);
        // 当前页
        List<Forum> forums;
        List<Topic> list;
        Map<String, Object> params = Maps.newLinkedHashMap();
        Forum f = new Forum();
        f.setTcid(tcid);
        forums = forumService.list(f);
        Collections.reverse(forums);
        try {
            // 判断显示板块方式，0表示全部
            if ("0".equals(forumid)) {
                Topic topic = new Topic();
                topic.setTcid(tcid);
                params.put("sort", sort);
                topic.setParams(params);
                startPage();
                list = topicService.list(topic);
                Collections.reverse(list);
            } else {
                Forum forum = new Forum();
                forum.setId(forumid);
                Topic topic = new Topic();
                topic.setTcid(tcid);
                params.put("sort", sort);
                topic.setParams(params);
                startPage();
                topic.setForum(forum);
                list = topicService.list(topic);
                Collections.reverse(list);
            }
        } catch (Exception e) {
            Topic topic = new Topic();
            topic.setTcid(tcid);
            params.put("sort", 1);
            topic.setParams(params);
            startPage();
            list = topicService.list(topic);
            Collections.reverse(list);
            forumid = "0";
            sort = 1;
        }
        modelMap.put("forumid", forumid);
        modelMap.put("selected", "forum");
        modelMap.put("ForumList", forums);
        modelMap.put("pageModel", wrapTable(list));
        modelMap.put("sort", sort);
        modelMap.put("openId", openId);
        return "/wx/forum/wxlearn";
    }
    @RequestMapping("/courseindex/{tcid}/detail/{id}")
    public String courseindexDtail(ModelMap modelMap, @PathVariable String id, @PathVariable String tcid,String openId) throws Exception {
        // 准备数据：topic
        Topic topic = topicService.getById(id);
        System.out.println("courseindexDtailn控制器获取了："+openId);
        /*topic.setBrowse(topic.getBrowse() + 1);
        topicService.update(topic);*/
        //浏览数+1
        /*  Atomic __ */
        if (!AtomicIntegerUtil.isEmplyInMap(topic.getClass(), id))
            AtomicIntegerUtil.getInstance(topic.getClass(), id, topic.getBrowse());
        topic.setBrowse((long) AtomicIntegerUtil.getInstance(topic.getClass(), id).getAndIncrement());

        modelMap.put("topic", topic);
        //准备被采纳数据：replyList
        Map<String, Object> params = Maps.newLinkedHashMap();
        params.put("adopt", 1);
        params.put("topic", topic);
        params.put("tcid", tcid);
        params.put("order_by", "r.create_time asc");
        Reply replyAdopt = replyService.getOne(params);
        modelMap.put("replyAdopt", replyAdopt);

        Reply reply = new Reply();
        reply.setAdopt(0);
        reply.setTopic(topic);
        reply.setTcid(tcid);
        params.clear();
        params.put("orderBy", "create_time ASC");
        reply.setParams(params);

        //准备其他回复：
        startPage();
        List<Reply> pageModel = replyService.list(reply);
        Collections.reverse(pageModel);
        Deck deck = new Deck();
        pageModel.forEach(p -> {
            deck.setReply(p);
            deck.setTcid(tcid);
            p.setDecks(new HashSet<>(deckService.list(deck)));
        });
        modelMap.put("openId",openId);
        modelMap.put("pageModel", wrapTable(pageModel));
        modelMap.put("selected", "detail");

        // 保存
        return "/wx/forum/wxdetail";
    }
    @RequestMapping("/CourseindextoPush/{tcid}")
    public String CourseindextoPush(ModelMap modelMap, @PathVariable String tcid) {
        /*
         * 判断权限
         * 如果是学生用户,就过滤 forumList
         */
        SysUser user = getUser();
        Forum f = new Forum();
        f.setTcid(tcid);
        List<Forum> forumList = forumService.list(f);
        Collections.reverse(forumList);
        if (user.getUserType() == UserConstants.USER_STUDENT) {
            List<Forum> tempForum = new ArrayList<>();
            for (Forum forum : forumList) {
                if (StringUtils.equalsIgnoreCase("1", forum.getType())) {
                    tempForum.add(forum);
                }
            }
            modelMap.put("forumList", tempForum);
        } else {
            modelMap.put("forumList", forumList);
        }
        modelMap.put("selected", "editor");
        return "/wx/front/wxlearn";
    }
}