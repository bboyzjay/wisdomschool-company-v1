package cn.wstom.web.controller.system;

import cn.wstom.common.base.Data;
import cn.wstom.common.constant.HttpConstants;
import cn.wstom.common.utils.DecryptionUtil;
import cn.wstom.common.utils.JSONUtils;
import cn.wstom.common.utils.KeyUtil;
import cn.wstom.common.utils.SendEmailUtil;
import cn.wstom.main.async.AsyncManager;
import cn.wstom.main.util.ServletUtils;
import cn.wstom.main.util.ShiroUtils;
import cn.wstom.main.web.base.BaseController;
import cn.wstom.system.data.LoginInfo;
import cn.wstom.system.entity.SysUser;
import cn.wstom.system.service.SysUserService;
import cn.wstom.system.vo.CaptchaVO;
import cn.wstom.web.hook.async.AsyncIntegral;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * 登录验证
 *
 * @author dws
 */
@Controller
public class SysLoginController extends BaseController {

    //
    public static final Map<String, CaptchaVO> captchaMap = new HashMap<>();

    @Autowired
    private SysUserService sysUserService;

    @GetMapping("/remind")
    public  String remind(){
        return  "remind";
    }


    /**
     * 登陆超时时间：30s
     */
    private static final long TIME_OUT = 30 * 1000;

    @GetMapping("/login")
    public String login(HttpServletRequest request, HttpServletResponse response) {
        // 如果是Ajax请求，返回Json字符串。
        if (ServletUtils.isAjaxRequest(request)) {
            return ServletUtils.renderString(response, "{\"code\":\"" + HttpConstants.CODE_NOT_LOGIN + "\",\"msg\":\"未登录或登录超时。请重新登录\"}");
        }

        return "login";
    }

    @PostMapping("/login")
    @ResponseBody
    public Data ajaxLogin(String encrypted) {
        final String loginInfo = DecryptionUtil.decryption(encrypted, KeyUtil.getPrivateKey());
        final LoginInfo info = JSONUtils.readValue(loginInfo, LoginInfo.class);
        if (info == null) {
            return Data.error("用户名和密码不能为空！");
        }
        if (System.currentTimeMillis() - Long.parseLong(info.getTime()) > TIME_OUT) {
            return Data.error("登陆超时");
        }
        UsernamePasswordToken token = new UsernamePasswordToken(info.getUsername(), info.getPassword(), info.isRememberMe());

        Subject subject = SecurityUtils.getSubject();
        try {
            subject.login(token);
            AsyncManager.async().execute(
                    AsyncIntegral.recordIntegral(ShiroUtils.getUser(), "login",
                            "用户【" + ShiroUtils.getUser().getUserName() + "】登陆"));
            return success();
        } catch (AuthenticationException e) {
            String msg = "用户或密码错误";
            if (StringUtils.isNotEmpty(e.getMessage())) {
                msg = e.getMessage();
            }
            return error(msg);
        }
    }

    @RequestMapping("/publicKey")
    @ResponseBody
    public Data getPublicKey() {
        return Data.success().put("k", KeyUtil.getPublicKey()).put("t", System.currentTimeMillis());
    }

    @GetMapping("/unauth")
    public String unauth() {
        return "/error/unauth";
    }

    @RequestMapping("/profile")
    @ResponseBody
    public SysUser loginInfo() {
        return getUser();
    }


    /**
     * 请求发送邮件，目前来说只有找回密码有发送邮件功能，如果多个功能都有，就要考虑复用性和url名字可识性
     *
     * @param email
     * @return
     */
    @PostMapping("/sendemail")
    @ResponseBody
    public Data sendEmail(String email) {
        //根据给定的邮箱发送邮件
        SysUser sysUser = sysUserService.selectUserByEmail(email);

        if (sysUser == null) {
            //用户为空，说明填写的邮箱就不对
            return Data.error("邮箱账号不存在");
        }

        //先生成验证码，先发送邮件，发送成功后保存验证码
        String captcha = RandomStringUtils.randomNumeric(4);

        //先发送验证码，发送成功了再保存到数据库
        boolean b = SendEmailUtil.sendPasswordCaptchaEmail(email, captcha);
        if (b) {
            CaptchaVO captchaVO = new CaptchaVO();
            captchaVO.setCaptcha(captcha);
            captchaVO.setIp(ShiroUtils.getIp());
            captchaMap.put(email, captchaVO);

            return Data.success("验证码已发送到您的邮箱，请注意查收");
        } else {
            //发送失败，提醒
            return Data.error("验证码发送失败，请检查邮箱账号");
        }
    }



    @PostMapping("/updatePasswordByCaptcha")
    @ResponseBody
    public Data updatePasswordByCaptcha(String email, String repassword, String captcha) {
        CaptchaVO captchaVO = captchaMap.get(email);

        if (captchaVO == null) {
            return Data.error("邮箱账号不存在");

        } else if (!StringUtils.equals(captchaVO.getIp(), ShiroUtils.getIp())) {
            return Data.error("地址异常，请重新发送邮件");

        } else if (!StringUtils.equalsIgnoreCase(captchaVO.getCaptcha(), captcha)) {
            return Data.error("验证码不正确");

        } else {
            // 根据给定的邮箱发送邮件
            SysUser sysUser = sysUserService.selectUserByEmail(email);

            if (sysUser == null) {
                //用户为空，说明填写的邮箱就不对
                return Data.error("邮箱账号不存在");
            }

            sysUser.setSalt(ShiroUtils.randomSalt());
            sysUser.setPassword(ShiroUtils.encryptPassword(sysUser.getLoginName(), repassword, sysUser.getSalt()));

            int result = sysUserService.updateUserPassword(sysUser);
            if (result > 0) {

                //验证成功了，返回重置密码的页面
                return Data.success("密码修改成功");
            }

            return Data.error("密码修改异常");
        }

    }



}
