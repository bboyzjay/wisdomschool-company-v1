package cn.wstom.web.controller.school.examsystem;

import cn.wstom.common.annotation.Log;
import cn.wstom.common.base.Data;
import cn.wstom.common.enums.ActionType;
import cn.wstom.main.web.base.BaseController;
import cn.wstom.main.web.page.TableDataInfo;
import cn.wstom.onlineexam.entity.TestpaperOneTestquestions;
import cn.wstom.onlineexam.entity.TestpaperTestquestions;
import cn.wstom.onlineexam.service.TestpaperOneTestquestionsService;
import cn.wstom.onlineexam.service.TestpaperTestquestionsService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 试卷与试题关联 信息操作处理
 *
 * @author
 * @date
 */
@Controller
@RequestMapping("/module/testpaperOneTestquestions")
public class TestpaperOneTestquestionsController extends BaseController {
    private String prefix = "module/testpaperOneTestquestions";

    @Autowired
    private TestpaperOneTestquestionsService testpaperTestquestionsService;

    @RequiresPermissions("module:testpaperOneTestquestions:view")
    @GetMapping()
    public String toList() {
        return prefix + "list";
    }

    /**
     * 查询试卷做的题目答案列表
     */
    @RequiresPermissions("module:testpaperOneTestquestions:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(TestpaperOneTestquestions testpaperTestquestions) {
        startPage();
        List<TestpaperOneTestquestions> list = testpaperTestquestionsService.list(testpaperTestquestions);
        return wrapTable(list);
    }

    /**
     * 新增试卷做的题目答案
     */
    @GetMapping("/add")
    public String toAdd() {
        return prefix + "/add";
    }

    /**
     * 新增保存试卷做的题目答案
     */
    @RequiresPermissions("module:testpaperOneTestquestions:add")
    @Log(title = "试卷做的题目答案", actionType = ActionType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public Data add(TestpaperOneTestquestions testpaperTestquestions) throws Exception {
        return toAjax(testpaperTestquestionsService.save(testpaperTestquestions));
    }

    /**
     * 修改试卷做的题目答案
     */
    @GetMapping("/edit/{id}")
    public String toEdit(@PathVariable("id") String id, ModelMap mmap) {
        TestpaperOneTestquestions testpaperTestquestions = testpaperTestquestionsService.getById(id);
        mmap.put("testpaperOneTestquestions", testpaperTestquestions);
        return prefix + "/edit";
    }

    /**
     * 修改保存试卷做的题目答案
     */
    @RequiresPermissions("module:testpaperOneTestquestions:edit")
    @Log(title = "试卷做的题目答案", actionType = ActionType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public Data edit(TestpaperOneTestquestions testpaperTestquestions) throws Exception {
        return toAjax(testpaperTestquestionsService.update(testpaperTestquestions));
    }

    /**
     * 删除试卷做的题目答案
     */
    @RequiresPermissions("module:testpaperOneTestquestions:remove")
    @Log(title = "试卷做的题目答案", actionType = ActionType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public Data remove(String ids) throws Exception {
        return toAjax(testpaperTestquestionsService.removeById(ids));
    }
}
