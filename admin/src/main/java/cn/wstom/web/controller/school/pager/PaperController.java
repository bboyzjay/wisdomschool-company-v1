package cn.wstom.web.controller.school.pager;

import cn.wstom.common.base.Data;
import cn.wstom.common.constant.Constants;
import cn.wstom.main.util.ShiroUtils;
import cn.wstom.main.web.base.BaseController;
import cn.wstom.onlineexam.entity.*;
import cn.wstom.onlineexam.service.*;
import cn.wstom.system.entity.SysUser;
import cn.wstom.system.service.SysUserService;
import cn.wstom.web.controller.school.onlineExam.common.util.Uuid;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Controller
@RequestMapping("/school/paper")
public class PaperController extends BaseController {
    private String prefix = "/school/paper";

    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private TestPaperService testPaperService;
    @Autowired
    private MyQuestionsService myQuestionsService;
    @Autowired
    private TestpaperQuestionsService testpaperQuestionsService;
    @Autowired
    private MyOptionAnswerService myOptionAnswerService;
    @Autowired
    private TestpaperOptinanswerService testpaperOptinanswerService;
    @Autowired
    private TestpaperTestquestionsService testpaperTestquestionsService;

    @Autowired
    private TestpaperOneTestquestionsService testpaperOneTestquestionsService;
    @Autowired
    private TestPaperOneService testPaperOneService;//考试系统组卷
    /**
     * 组卷页面跳转
     */
    @GetMapping("/build")
    @RequiresPermissions("teacher:course:view")
    public String build(String id, ModelMap mmap) {
        TestPaper testPaper = testPaperService.getById(id);
        mmap.put("testPaper", testPaper);
        mmap.put("tid", getUser().getUserAttrId());
        mmap.put("cid",testPaper.getCoursrId());
        mmap.put("id", id);
        mmap.put("rule",testPaper.getRule());
        return prefix + "/build";
    }
    /**
     * 考试系统组卷页面跳转
     */
    @GetMapping("/buildOne")
    @RequiresPermissions("teacher:course:view")
    public String buildOne(String id, ModelMap mmap) {
        TestPaperOne testPaper = testPaperOneService.getById(id);
        mmap.put("testPaper", testPaper);
        mmap.put("tid", getUser().getUserAttrId());
        mmap.put("cid",testPaper.getCoursrId());
        mmap.put("id", id);
        mmap.put("rule",testPaper.getRule());
        return prefix + "/buildOne";
    }
    /**
     * 添加题目界面
     */
    @GetMapping("/question/add")
    @RequiresPermissions("teacher:course:view")
    public String add(String paperId, String myQuestionId, ModelMap modelMap) {
        MyQuestions questions = myQuestionsService.getById(myQuestionId);
        modelMap.put("question", questions);
        modelMap.put("paperId", paperId);
        return prefix + "/add";
    }
    /**
     * 添加题目界面
     */
    @GetMapping("/question/addOne")
    @RequiresPermissions("teacher:course:view")
    public String addOne(String paperId, String myQuestionId, ModelMap modelMap) {
        MyQuestions questions = myQuestionsService.getById(myQuestionId);
        modelMap.put("question", questions);
        modelMap.put("paperId", paperId);
        return prefix + "/addOne";
    }
    /**
     * 添加题目
     * @param paperId
     * @param myQuestionId
     * @param questions
     * @return
     * @throws Exception
     */
    @PostMapping("/question/add/{paperId}/{myQuestionId}")
    @RequiresPermissions("teacher:course:view")
    @ResponseBody
    @Transactional
    public Data add(@PathVariable("paperId") String paperId,
                    @PathVariable("myQuestionId") String myQuestionId,
                    MyQuestions questions) throws Exception {
        String score = String.valueOf(questions.getQuestionscore());
        SysUser sysUser = sysUserService.getById(getUserId());
        String attrId = sysUser.getUserAttrId();
        /*  查询题目 */
        MyQuestions myq = myQuestionsService.getById(myQuestionId);
        int questionExposed = Integer.parseInt(myq.getQexposed());
        questionExposed += 1;
        MyQuestions myQuestions2 = new MyQuestions();
        myQuestions2.setQexposed(String.valueOf(questionExposed));
        myQuestions2.setId(myQuestionId);
        myQuestions2.setUpdateBy(ShiroUtils.getLoginName());
        myQuestionsService.update(myQuestions2);
        /*  保存题目和试卷的关联 */
        TestpaperQuestions tpq = testpaperQuestionsService.selectByPersonalQuestionId(Integer.valueOf(myq.getId()));

        Uuid uuid = new Uuid();
        if (tpq == null) {
            TestpaperQuestions save = new TestpaperQuestions();
            save.setId(uuid.UId());
            save.setCreateId(getUserId());
            save.setCreateBy(ShiroUtils.getLoginName());
            save.setPersonalQuestionId(Integer.valueOf(myq.getId()));
            testpaperQuestionsService.save(save);
            tpq = testpaperQuestionsService.selectByPersonalQuestionId(Integer.valueOf(myq.getId()));
        }
        tpq.setQuestionScore(Integer.parseInt(score));
        tpq.setTitleTypeId(myq.getTitleTypeId());
        tpq.setDifficulty(myq.getDifficulty());
        tpq.setTitle(myq.getTitle());
        tpq.setQexposed(myQuestions2.getQexposed());
        tpq.setQmaxexposure(myq.getQmaxexposure());
        tpq.setParsing(myq.getParsing());
        tpq.setYear(myq.getYear());

            /*      原来的题目选项   */
//            Map<String, Object> params = new HashMap<>();
//            params.put("question_id", tpq.getId());
            TestpaperOptinanswer testpaperOptinanswer = new TestpaperOptinanswer();
            testpaperOptinanswer.setQuestionId(tpq.getId());
            List answers = testpaperOptinanswerService.list(testpaperOptinanswer);


//            params = new HashMap<>();
//            params.put("test_questions_id", tpq.getId());
//            params.put("test_paper_id", paperId);

            TestpaperTestquestions testquestions = new TestpaperTestquestions();
            testquestions.setTestPaperId(paperId);
            testquestions.setTestQuestionsId(tpq.getId());
            List ttq = testpaperTestquestionsService.list(testquestions);


        /*  复制MyQuestionAnswer 到 testPaperQuestionAnswer */
        String myoastr = myq.getMyoptionAnswerArr();//  题目选项
        if (myoastr != null && !"".equals(myoastr) && answers.isEmpty()) {
            String[] myoaArr = myoastr.substring(0, myoastr.length() - 1).split(";");
            List<MyOptionAnswer> myoalist = new ArrayList<MyOptionAnswer>();
            for (String aMyoaArr : myoaArr) {
                MyOptionAnswer myOptionAnswer = myOptionAnswerService.getById(aMyoaArr);
                myoalist.add(myOptionAnswer);
            }
            StringBuilder tpoastr = new StringBuilder();
            for (MyOptionAnswer myoa : myoalist) {
                TestpaperOptinanswer toa = new TestpaperOptinanswer();
                Uuid uu = new Uuid();
                toa.setId(uu.UId());
                toa.setCreateId(attrId);
                toa.setCreateBy(ShiroUtils.getLoginName());
                toa.setQuestionId(tpq.getId());
                toa.setStanswer(myoa.getStanswer());
                toa.setStoption(myoa.getStoption());
                testpaperOptinanswerService.save(toa);
                tpoastr.append(toa.getId()).append(";");
            }
            tpq.setTestPaperOptionAnswerArr(tpoastr.toString());
        }

        if (ttq == null || ttq.isEmpty()) {
            TestpaperTestquestions testPaperTestQuestions = new TestpaperTestquestions();
            testPaperTestQuestions.setTestQuestionsId(tpq.getId());
            testPaperTestQuestions.setTestPaperId(paperId);
            Uuid tpUuid = new Uuid();
            testPaperTestQuestions.setId(tpUuid.UId());
            testPaperTestQuestions.setCreateId(attrId);
            testPaperTestQuestions.setCreateBy(ShiroUtils.getLoginName());
            testpaperTestquestionsService.save(testPaperTestQuestions);
        }

        /*  更新testpaperquestion */
        testpaperQuestionsService.update(tpq);

        TestPaper testPaper = testPaperService.getById(paperId);
        String time = new Date().getYear() + 1900 + "";
        testPaper.setTestYear(time);
        testPaper.setState(Constants.EXAM_TYPE_WAIT);
        int so1 = Integer.valueOf(testPaper.getScore());
        int so2 = Integer.valueOf(score);
        testPaper.setScore(String.valueOf(so1 + so2));

        return toAjax(testPaperService.update(testPaper));
    }
    /**
     * 添加题目
     * @param paperId
     * @param myQuestionId
     * @param questions
     * @return
     * @throws Exception
     */
    @PostMapping("/question/addOne/{paperId}/{myQuestionId}")
    @RequiresPermissions("teacher:course:view")
    @ResponseBody
    @Transactional
    public Data addOne(@PathVariable("paperId") String paperId,
                    @PathVariable("myQuestionId") String myQuestionId,
                    MyQuestions questions) throws Exception {
        String score = String.valueOf(questions.getQuestionscore());
        SysUser sysUser = sysUserService.getById(getUserId());
        String attrId = sysUser.getUserAttrId();
        /*  查询题目 */
        MyQuestions myq = myQuestionsService.getById(myQuestionId);
        int questionExposed = Integer.parseInt(myq.getQexposed());
        questionExposed += 1;
        MyQuestions myQuestions2 = new MyQuestions();
        myQuestions2.setQexposed(String.valueOf(questionExposed));
        myQuestions2.setId(myQuestionId);
        myQuestions2.setUpdateBy(ShiroUtils.getLoginName());
        myQuestionsService.update(myQuestions2);
        /*  保存题目和试卷的关联 */
        TestpaperQuestions tpq = testpaperQuestionsService.selectByPersonalQuestionId(Integer.valueOf(myq.getId()));

        Uuid uuid = new Uuid();
        if (tpq == null) {
            TestpaperQuestions save = new TestpaperQuestions();
            save.setId(uuid.UId());
            save.setCreateId(getUserId());
            save.setCreateBy(ShiroUtils.getLoginName());
            save.setPersonalQuestionId(Integer.valueOf(myq.getId()));
            testpaperQuestionsService.save(save);
            tpq = testpaperQuestionsService.selectByPersonalQuestionId(Integer.valueOf(myq.getId()));
        }
        tpq.setQuestionScore(Integer.parseInt(score));
        tpq.setTitleTypeId(myq.getTitleTypeId());
        tpq.setDifficulty(myq.getDifficulty());
        tpq.setTitle(myq.getTitle());
        tpq.setQexposed(myQuestions2.getQexposed());
        tpq.setQmaxexposure(myq.getQmaxexposure());
        tpq.setParsing(myq.getParsing());
        tpq.setYear(myq.getYear());

        /*      原来的题目选项   */
//            Map<String, Object> params = new HashMap<>();
//            params.put("question_id", tpq.getId());
        TestpaperOptinanswer testpaperOptinanswer = new TestpaperOptinanswer();
        testpaperOptinanswer.setQuestionId(tpq.getId());
        List answers = testpaperOptinanswerService.list(testpaperOptinanswer);


//            params = new HashMap<>();
//            params.put("test_questions_id", tpq.getId());
//            params.put("test_paper_id", paperId);

        TestpaperOneTestquestions testquestions = new TestpaperOneTestquestions();
        testquestions.setTestPaperOneId(paperId);
        testquestions.setTestQuestionsId(tpq.getId());
        List ttq = testpaperOneTestquestionsService.list(testquestions);


        /*  复制MyQuestionAnswer 到 testPaperQuestionAnswer */
        String myoastr = myq.getMyoptionAnswerArr();//  题目选项
        if (myoastr != null && !"".equals(myoastr) && answers.isEmpty()) {
            String[] myoaArr = myoastr.substring(0, myoastr.length() - 1).split(";");
            List<MyOptionAnswer> myoalist = new ArrayList<MyOptionAnswer>();
            for (String aMyoaArr : myoaArr) {
                MyOptionAnswer myOptionAnswer = myOptionAnswerService.getById(aMyoaArr);
                myoalist.add(myOptionAnswer);
            }
            StringBuilder tpoastr = new StringBuilder();
            for (MyOptionAnswer myoa : myoalist) {
                TestpaperOptinanswer toa = new TestpaperOptinanswer();
                Uuid uu = new Uuid();
                toa.setId(uu.UId());
                toa.setCreateId(attrId);
                toa.setCreateBy(ShiroUtils.getLoginName());
                toa.setQuestionId(tpq.getId());
                toa.setStanswer(myoa.getStanswer());
                toa.setStoption(myoa.getStoption());
                testpaperOptinanswerService.save(toa);
                tpoastr.append(toa.getId()).append(";");
            }
            tpq.setTestPaperOptionAnswerArr(tpoastr.toString());
        }

        if (ttq == null || ttq.isEmpty()) {
            TestpaperOneTestquestions testPaperTestQuestions = new TestpaperOneTestquestions();
            testPaperTestQuestions.setTestQuestionsId(tpq.getId());
            testPaperTestQuestions.setTestPaperOneId(paperId);
            Uuid tpUuid = new Uuid();
            testPaperTestQuestions.setId(tpUuid.UId());
            testPaperTestQuestions.setCreateId(attrId);
            testPaperTestQuestions.setCreateBy(ShiroUtils.getLoginName());
            testpaperOneTestquestionsService.save(testPaperTestQuestions);
        }

        /*  更新testpaperquestion */
        testpaperQuestionsService.update(tpq);

        TestPaperOne testPaper = testPaperOneService.getById(paperId);
        String time = new Date().getYear() + 1900 + "";
        testPaper.setTestYear(time);
        testPaper.setState(Constants.EXAM_TYPE_WAIT);
        int so1 = Integer.valueOf(testPaper.getScore());
        int so2 = Integer.valueOf(score);
        testPaper.setScore(String.valueOf(so1 + so2));

        return toAjax(testPaperOneService.update(testPaper));
    }
}
