package cn.wstom.web.controller.school.onlineExam.common.util;

import cn.wstom.common.annotation.Excel;
import cn.wstom.jiaowu.entity.Chapter;
import cn.wstom.jiaowu.entity.Course;
import cn.wstom.onlineexam.entity.MyOptionAnswer;
import cn.wstom.onlineexam.entity.MyQuestions;
import cn.wstom.onlineexam.entity.MyTitleType;
import lombok.Data;

@Data
public class MyQuestionVO<Srting> extends MyQuestions {
    private static final long serialVersionUID = -4765542647341463846L;

    /**
     * 课程
     */
//    @Excel(name = "课程(必填)", comboField = "name", targetAttr = "name", height= 40)
    private Course course;

    /**
     * 题型
     */
    @Excel(name = "题型",prompt = "单项选择题|多项选择题|判断题",height= 40)
    private String myTitleType;
//    /**
//     * 所在章节
//     */
//    @Excel(name = "章节（必填）", comboField = "id", targetAttr = "id", height= 40)
//    private Chapter chapter;
    /**
     * 题目信息
     */
    private MyQuestions myQuestions;

    /**
     * 题目信息
     */

    @Excel(name = "选项A", height= 40)
    Srting myOptionA;
    @Excel(name = "选项B", height= 40)
    Srting myOptionB;
    @Excel(name = "选项C", height= 40)
    Srting myOptionC;
    @Excel(name = "选项D", height= 40)
    Srting myOptionD;
    @Excel(name = "选项E", height= 40)
    Srting myOptionE;
    @Excel(name = "选项F", height= 40)
    Srting myOptionF;

    public Srting getMyOptionA() {
        return myOptionA;
    }

    public void setMyOptionA(Srting myOptionA) {
        this.myOptionA = myOptionA;
    }

    public Srting getMyOptionB() {
        return myOptionB;
    }

    public void setMyOptionB(Srting myOptionB) {
        this.myOptionB = myOptionB;
    }

    public Srting getMyOptionC() {
        return myOptionC;
    }

    public void setMyOptionC(Srting myOptionC) {
        this.myOptionC = myOptionC;
    }

    public Srting getMyOptionD() {
        return myOptionD;
    }

    public void setMyOptionD(Srting myOptionD) {
        this.myOptionD = myOptionD;
    }



    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public MyQuestions getMyQuestions() {
        return myQuestions;
    }

    public void setMyQuestions(MyQuestions myQuestions) {
        this.myQuestions = myQuestions;
    }

    public String getMyTitleType() {
        return myTitleType;
    }

    public void setMyTitleType(String myTitleType) {
        this.myTitleType = myTitleType;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

//    public Chapter getChapter() {
//        return chapter;
//    }
//
//    public void setChapter(Chapter chapter) {
//        this.chapter = chapter;
//    }

    public Srting getMyOptionE() {
        return myOptionE;
    }

    public void setMyOptionE(Srting myOptionE) {
        this.myOptionE = myOptionE;
    }

    public Srting getMyOptionF() {
        return myOptionF;
    }

    public void setMyOptionF(Srting myOptionF) {
        this.myOptionF = myOptionF;
    }
}
