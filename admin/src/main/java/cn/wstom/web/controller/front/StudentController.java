package cn.wstom.web.controller.front;

import cn.wstom.common.annotation.Log;
import cn.wstom.common.base.Data;
import cn.wstom.common.base.entity.BaseEntity;
import cn.wstom.common.constant.Constants;
import cn.wstom.common.constant.EventConstants;
import cn.wstom.common.constant.ForumConstants;
import cn.wstom.common.constant.StorageConstants;
import cn.wstom.common.constant.UserConstants;
import cn.wstom.common.entity.Comment;
import cn.wstom.common.service.CommentService;
import cn.wstom.common.utils.AtomicIntegerUtil;
import cn.wstom.common.utils.FileUtils;
import cn.wstom.common.utils.StringUtil;
import cn.wstom.jiaowu.data.ChapterVo;
import cn.wstom.jiaowu.data.ClbumCourseVo;
import cn.wstom.jiaowu.data.StudentVo;
import cn.wstom.jiaowu.data.TeacherVo;
import cn.wstom.jiaowu.entity.*;
import cn.wstom.jiaowu.service.ChapterResourceService;
import cn.wstom.jiaowu.service.ChapterService;
import cn.wstom.jiaowu.service.ClbumCourseService;
import cn.wstom.jiaowu.service.ClbumService;
import cn.wstom.jiaowu.service.CourseService;
import cn.wstom.jiaowu.service.DepartmentService;
import cn.wstom.jiaowu.service.GradesService;
import cn.wstom.jiaowu.service.IntegralDetailService;
import cn.wstom.jiaowu.service.IntegralService;
import cn.wstom.jiaowu.service.LeadService;
import cn.wstom.jiaowu.service.MajorService;
import cn.wstom.jiaowu.service.OutlineService;
import cn.wstom.jiaowu.service.OutpeizhiService;
import cn.wstom.jiaowu.service.RecourseService;
import cn.wstom.jiaowu.service.RecourseTypeService;
import cn.wstom.jiaowu.service.StudentService;
import cn.wstom.jiaowu.service.TeacherCourseService;
import cn.wstom.jiaowu.service.TeacherService;
import cn.wstom.main.util.ShiroUtils;
import cn.wstom.main.util.file.FileUploadUtils;
import cn.wstom.main.web.base.BaseController;
import cn.wstom.main.web.page.TableDataInfo;
import cn.wstom.onlineexam.entity.*;
import cn.wstom.onlineexam.service.*;
import cn.wstom.square.data.ArticleCommentVo;
import cn.wstom.square.data.PageVo;
import cn.wstom.square.entity.*;
import cn.wstom.square.service.*;
import cn.wstom.system.entity.SysUser;
import cn.wstom.system.service.SysUserService;
import cn.wstom.web.event.NotifyEvent;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.Assert;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author dws
 * @date 2019/02/23
 */
@Controller
@RequestMapping("/user")
public class StudentController extends BaseController {
    private final String FRONT_USER_PREFIX = "/front/user";

    private final FavoriteService favoriteService;
    private final SysUserService userService;
    private final StudentService studentService;
    private final TestPaperOneService testPaperOneService;
    private final ClbumService clbumService;
    private final MajorService majorService;
    private final DepartmentService departmentService;
    private final GradesService gradesService;
    private final TeacherService teacherService;
    private final ClbumCourseService clbumCourseService;

    private final TeacherCourseService teacherCourseService;
    private final CourseService courseService;
    private final ChapterService chapterService;
    private final CommentService commentService;
    private final OutlineService outlineService;
    private final OutpeizhiService outpeizhiService;
    private final LeadService leadService;
    private final ChapterResourceService chapterResourceService;
    private final RecourseService recourseService;
    private final ForumService forumService;
    private final TopicService topicService;
    private final ReplyService replyService;
    private final DeckService deckService;
    private final IntegralService integralService;
    private final IntegralDetailService integralDetailService;
    private final RecourseTypeService recourseTypeService;
    private final ArticleService articleService;

    @Autowired
    private MyQuestionsService myQuestionsService;
    @Autowired
    private MyOptionAnswerService myOptionAnswerService;
    @Autowired
    private UserTestService userTestService;
    @Autowired
    private UserExamService userExamService;
    @Autowired
    TestPaperService testPaperService;
    @Autowired
    private ArticleVotesService articleVotesService;
    @Autowired
    private ShuatiHistoryService shuatiHistoryService;



    @Value("${wstom.storageContextPath}")
    private String storageContextPath;

    @Autowired
    public StudentController(FavoriteService favoriteService,
                             ArticleService articleService,
                             CommentService commentService, SysUserService userService,
                             StudentService studentService, TestPaperOneService testPaperOneService, ClbumService clbumService,
                             MajorService majorService, DepartmentService departmentService,
                             GradesService gradesService,
                             TeacherService teacherService, CourseService courseService,
                             ClbumCourseService clbumCourseService,
                             TeacherCourseService teacherCourseService,
                             ChapterService chapterService,
                             OutlineService outlineService,
                             OutpeizhiService outpeizhiService,
                             LeadService leadService,
                             ChapterResourceService chapterResourceService,
                             RecourseService recourseService,
                             ForumService forumService, TopicService topicService, ReplyService replyService, DeckService deckService, IntegralService integralService, IntegralDetailService integralDetailService, RecourseTypeService recourseTypeService) {

        this.favoriteService = favoriteService;
        this.commentService = commentService;
        this.userService = userService;
        this.studentService = studentService;
        this.testPaperOneService = testPaperOneService;
        this.clbumService = clbumService;
        this.majorService = majorService;
        this.departmentService = departmentService;
        this.gradesService = gradesService;
        this.teacherService = teacherService;
        this.clbumCourseService = clbumCourseService;
        this.teacherCourseService = teacherCourseService;
        this.courseService = courseService;
        this.chapterService = chapterService;
        this.outlineService = outlineService;
        this.outpeizhiService = outpeizhiService;
        this.leadService = leadService;
        this.chapterResourceService = chapterResourceService;
        this.recourseService = recourseService;
        this.forumService = forumService;
        this.topicService = topicService;
        this.replyService = replyService;
        this.deckService = deckService;
        this.integralService = integralService;
        this.integralDetailService = integralDetailService;
        this.recourseTypeService = recourseTypeService;
        this.articleService = articleService;
    }

    /**
     * 查看课程列表列表
     *
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value = "/course", method = RequestMethod.GET)
    public String home(HttpServletRequest request, ModelMap model) {
        int pageNum = ServletRequestUtils.getIntParameter(request, "pn", 1);
        model.put("pn", pageNum);
        return FRONT_USER_PREFIX + "/course";
    }

    /**
     * 获取学生课程列表
     * 获取我的课程列表
     */
    @PostMapping("/course/list")
    @ResponseBody
    public TableDataInfo list() {
        Student student = studentService.getById(getUser().getUserAttrId());
        ClbumCourse cc = new ClbumCourse();
        cc.setCid(student.getCid());
        startPage();
        List<ClbumCourse> list = clbumCourseService.list(cc);
        PageInfo<ClbumCourse> pageInfo = new PageInfo(list);
        //填充教师信息
        List<ClbumCourseVo> clbumCourseVos = new LinkedList<>();
        fillTeacherCourse(list, clbumCourseVos);
        return wrapTable(clbumCourseVos, pageInfo.getTotal(), pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getPages());
    }

    /**
     * 获取课程详情页面
     * 学生课程
     *
     * @param tcid 教师课程id
     */
    @GetMapping("/course/learn/{tcid}")
    public String toCourseDetail(@PathVariable String tcid, HttpServletRequest request, ModelMap map) {
        TeacherCourse teacherCourse = teacherCourseService.getById(tcid);
        Course course = courseService.getById(teacherCourse.getCid());
        Teacher teacher = teacherService.getById(teacherCourse.getTid());
        Map<String, Object> params = new LinkedHashMap<>();
        params.put("userType", UserConstants.USER_TEACHER);
        params.put("userAttrId", teacher.getId());
        SysUser user = userService.getOne(params);
        Chapter chapter = new Chapter();
        chapter.setCid(course.getId());
        List<Chapter> courseList = chapterService.list(chapter);
        System.out.println(transTree(courseList).toString());
        map.put("chapter", transTree(courseList));
        map.put("teacher", teacher);
        map.put("teacherUser", user);
        map.put("course", course);
        map.put("selected", "");
        map.put("studentId", getUser().getUserAttrId());

        return "/front/learn";
    }
    /**
     * 获取课程详情页面
     * 学生课程
     *
     * @param tcid 教师课程id
     */
    @GetMapping("/course/learnone/{tcid}")
    public String toCourseOneDetail(@PathVariable String tcid, HttpServletRequest request, ModelMap map) {
        TeacherCourse teacherCourse = teacherCourseService.getById(tcid);
        Course course = courseService.getById(teacherCourse.getCid());
        Teacher teacher = teacherService.getById(teacherCourse.getTid());
        Map<String, Object> params = new LinkedHashMap<>();
        params.put("userType", UserConstants.USER_TEACHER);
        params.put("userAttrId", teacher.getId());
        SysUser user = userService.getOne(params);
        Chapter chapter = new Chapter();
        chapter.setCid(course.getId());
        List<Chapter> courseList = chapterService.list(chapter);
        ChapterResource chapterResource = new ChapterResource();
        chapterResource.setTcId(tcid);
        chapterResource.setCid("1");
        List<ChapterResource> chapterResourceList = chapterResourceService.list(chapterResource);
        System.out.println(chapterResourceList);
//        UserExam userExam = new UserExam();
//        userExam.setcId(teacherCourse.getCid());
//        userExam.setUserId(getUserId());
//        List<UserExam> userExamList = userExamService.list(userExam);
//        System.out.println(userExamList);
//        System.out.println(transTree(courseList).toString());
        map.put("chapter", transTree(courseList));
        map.put("teacher", teacher);
        map.put("teacherUser", user);
//        map.put("userExam", userExamList);
        map.put("course", course);
        map.put("chapterResourceList", chapterResourceList);
        map.put("selected", "");
        map.put("studentId", getUser().getUserAttrId());

        return "/front/learnone";
    }

    /**
     * 获取课程详情页面
     * 学生课程
     *
     * @param tcid 教师课程id
     */
    @GetMapping("/course/info/{tcid}")
    public String toCourseInfo(@PathVariable String tcid, HttpServletRequest request, ModelMap map) {
        TeacherCourse teacherCourse = teacherCourseService.getById(tcid);
        Course course = courseService.getById(teacherCourse.getCid());
        Teacher teacher = teacherService.getById(teacherCourse.getTid());
        Map<String, Object> params = new LinkedHashMap<>();
        params.put("userType", UserConstants.USER_TEACHER);
        params.put("userAttrId", teacher.getId());
        SysUser user = userService.getOne(params);
        map.put("teacher", teacher);
        map.put("teacherUser", user);
        map.put("course", course);
        map.put("selected", "");
        request.getSession().setAttribute("tcid", tcid);
        request.getSession().setAttribute("teacherCourse", teacherCourse);
        request.getSession().setAttribute("course", course);
        map.put("studentId", getUser().getUserAttrId());

        map.put("types", recourseTypeService.list(new RecourseType()));
        return "/front/info";
    }

    @RequestMapping("/shouye/talk")
    @ResponseBody
    public Data findshouyeTalk() throws IOException {
        startPage();
        List<Topic> topics = topicService.list(new Topic());


        TableDataInfo pageInfo = wrapTable(topics);
        List<Topic> list = new ArrayList<>();
        for (Topic topic : topics) {
            Topic temp = new Topic();
            temp.setId(topic.getId());
            temp.setTitle(topic.getTitle());
            temp.setCreateBy(topic.getCreateBy());
            temp.setCreateName(topic.getCreateName());
            // 这里用内容来装转换后的发布时间
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String strDate = sdf.format(topic.getCreateTime());
            temp.setContent(strDate);
            list.add(temp);
        }
        return Data.success().put(Data.RESULT_KEY_DATA, list);
    }

    @RequestMapping("/personal/{id}")
    public String personal(@PathVariable String id, ModelMap modelMap) throws IOException {

        List<IntegralDetail> integralDetails = integralDetailService.selectBatchIntegral(Arrays.asList(id));
        List<Object[]> list = null;
        /*int count1=allintegralService.attentionme(userId);
        int count2=allintegralService.attentionother(userId);*/

        modelMap.put("allintegral", integralDetails);
        //如果是点开自己的个人中心
        if (id.equals(getUserId())) {

            Student student = studentService.getById(getUser().getUserAttrId());
//           String email = userService.selectUserByUserAttrId(getUser().getUserAttrId()).getEmail();
//           System.out.printf(email);
            //  累加积分值
            int sum = 0;
            IntegralDetail integralDetail = new IntegralDetail();
            integralDetail.setUserId(getUserId());
            List<IntegralDetail> integralDetailList = integralDetailService.list(integralDetail);
            for (int j = 0; j < integralDetailList.size(); j++) {
                System.out.println(integralDetailList.get(j).getUserName()+integralDetailList.get(j).getUserId()+",,"+integralDetailList.get(j).getCredit());
                sum += integralDetailList.get(j).getCredit();
            }
            getUser().setCredit(sum);

            if (student != null) {
                try {
                    /*list = testManageService.getAllTestTask(userIds);
                    List<Topic> topiclist=allintegralService.findalltopiclistbyid(userIds,"2");
                    List<Reply> replylist=allintegralService.findallreplybyid(userIds,"2");
                    List<StuReadAnnounce> proclamation=this.stuPublishService.getAllUserRead(userIds);//选出已发布的和该学生有关的公告
*/

                    Reply reply = new Reply();
                    reply.setAdopt(null);
                    reply.setCreateBy(getUserId());
                    List<Reply> replyList = replyService.list(reply);

                    Topic topic = new Topic();
                    topic.setCreateBy(getUserId());
                    List<Topic> topicList = topicService.list(topic);

                    /*  Atomic __ */
                    topicList.forEach( t -> {
                        t.setBrowse((long) AtomicIntegerUtil.getInstance(t.getClass(), t.getId(), t.getBrowse()).get());
                    });


                    modelMap.put("replylist", replyList);
                    modelMap.put("topiclist", topicList);
                    /*this.getRequest().setAttribute("topiclist", topiclist);
                    this.getRequest().setAttribute("replylist", replylist);
                    this.getRequest().setAttribute("student", student);
                    this.getRequest().setAttribute("allTestPaperList", list);
                    this.getRequest().setAttribute("proclamation", proclamation);
                    this.getRequest().setAttribute("size", proclamation.size());
                    if(list!=null){
                        this.getRequest().setAttribute("count3", list.size());
                    }
                    else{
                        this.getRequest().setAttribute("count3", 0);
                    }*/
                    modelMap.put("student", student);
                    modelMap.put("credit", sum);

                } catch (Exception e) {
                    System.out.println(e.getMessage() + "++++++++++++++");
                    e.printStackTrace();
                }
                return "/front/personal";
            } else {
                return "personal2";
            }

        }
        return "/front/personal";

        //点开其他人的个人中心
        /*else{
            JwStudentInfo student=studentService.get(JwStudentInfo.class, userId);
            if(student!=null){
                Attention attention=allintegralService.findattentionlist(userIds,userId);
                List<Topic> topiclist=allintegralService.findalltopiclistbyid(userId,"2");
                List<Reply> replylist=allintegralService.findallreplybyid(userId,"2");
                list = testManageService.getAllTestTask(userId);
                this.getRequest().setAttribute("topiclist", topiclist);
                this.getRequest().setAttribute("replylist", replylist);
                this.getRequest().setAttribute("userId", userId);
                this.getRequest().setAttribute("student", student);
                this.getRequest().setAttribute("attention", attention);
                this.getRequest().setAttribute("allTestPaperList", list);
                if(list!=null){
                    this.getRequest().setAttribute("count3", list.size());
                }
                else{
                    this.getRequest().setAttribute("count3", 0);
                }
                return "personal1";
            }
            else{
                return "personal2";
            }
        }*/
    }

    /**
     * 获取学习指导内容
     *
     * @param type 学习指导类型
     */
    @GetMapping("/guide/{type}")
    public String toCourseGuide(@PathVariable String type, HttpServletRequest request, ModelMap map) {
        TeacherCourse teacherCourse = (TeacherCourse) request.getSession().getAttribute("teacherCourse");
        Map<String, Object> params = new HashMap<>(3);
        params.put("tcId", teacherCourse.getId());
        map.put("type", type);

        if ("lead".equals(type)) {
            Lead lead = leadService.getOne(params);
            map.put("guide", lead);
        } else if ("outline".equals(type)) {
            Outline outline = outlineService.getOne(params);
            map.put("guide", outline);
        }else if ("outpeizhi".equals(type)) {
            Outpeizhi outpeizhi = outpeizhiService.getOne(params);
            map.put("guide", outpeizhi);
        }

        return "/front/learnZhidao";
    }

    /**
     * 获取学习指导内容
     *
     * @param typeId 学习指导类型
     */
    @GetMapping("/resource/{typeId}")
    public String toCourseResource(@PathVariable String typeId, HttpServletRequest request, ModelMap map) {

        TeacherCourse teacherCourse = (TeacherCourse) request.getSession().getAttribute("teacherCourse");
        String search = ServletRequestUtils.getStringParameter(request, "search", null);

        map.put("list", getResource(teacherCourse, typeId, search));
        map.put("type", recourseTypeService.map(new RecourseType()).get(typeId));
        RecourseType recourseType = new RecourseType();
        recourseType.setTcId(teacherCourse.getId());
        map.put("types", recourseTypeService.list(recourseType));
        return "/front/resource";
    }

    /**
     * 获取学习指导内容
     *
     * @param typeId 学习指导类型
     */
    @PostMapping("/newResource/{typeId}")
    @ResponseBody
    public TableDataInfo toNewCourseResource(@PathVariable String typeId, HttpServletRequest request) {

        TeacherCourse teacherCourse = (TeacherCourse) request.getSession().getAttribute("teacherCourse");
        return getResource(teacherCourse, typeId, null);
    }

    private TableDataInfo getResource(TeacherCourse teacherCourse, String typeId, String search) {

        Recourse recourse = new Recourse();
        RecourseType recourseType = new RecourseType();
        if (!"0".equals(typeId)) {
            recourseType.setId(typeId);
        }
        recourse.setCategory(recourseType);
        recourse.setTcId(teacherCourse.getId());
        recourse.setName(search);
        startPage();
        List<Recourse> list = recourseService.list(recourse);
//        Collections.reverse(list);
//        list = list.stream().sorted(Comparator.comparing(Recourse::getCreate_time)).collect(Collectors.toList());
        list.forEach( r -> {
            long count = r.getCount() != null ? r.getCount() : 0;
            r.setCount(AtomicIntegerUtil.getInstance(r.getClass(), r.getId(), count).get());
        });

        PageInfo pageInfo = new PageInfo(list);
        return wrapTable(list, pageInfo.getTotal(), pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getPages());
    }

    /*public String list(HttpServletRequest request) {
        // 准备数据：topic
        String id = getRequest().getParameter("id");
        Topic topic = topicService.get(Topic.class,id);
        //浏览数+1
        topic.setBrowse(topic.getBrowse()+1);
        topicService.update(topic);
        getRequest().setAttribute("topic",topic);
        //准备被采纳数据：replyList
        List<Reply>  replyAdopt = replyService.findByTopic(topic);
        if(replyAdopt!=null&&replyAdopt.size()>0){
            getRequest().setAttribute("replyAdopt",replyAdopt.get(0));
        }
        //准备其他回复：
        String numb = getRequest().getParameter("pageNum");
        int pageNum;
        if(StringUtils.isNumeric(numb)&&StringUtils.isNotEmpty(numb)){
            pageNum = Integer.valueOf(numb);
        }
        else{
            pageNum=1;
        }
        int pageSize = 10;//每页显示页数
        PageModel<Reply>  pageModel = replyService.findByTopic(pageNum, topic, pageSize);
        //设置头像
        for(Reply reply : pageModel.getResult()){
            //学生添加头像其他不添加
            if(UserType.STUDENT.equals(reply.getUserType())){
                JwStudentInfo studentInfo = this.userService.get(JwStudentInfo.class, reply.getCreateId());
                if(studentInfo.getHeadImage()!=null){
                    //给已上传头像学生设置头像
                    reply.getUser().setHeadImage(studentInfo.getHeadImage());
                }
            }
        }
        getRequest().setAttribute("pageModel",pageModel);
        return "show";
    }*/

    @RequestMapping("/topicList")
    public String topicList(HttpServletRequest request, ModelMap modelMap) throws ServletRequestBindingException {
        // 加载用户信息
        // 论坛的排序方式
        String course = ServletRequestUtils.getStringParameter(request, "course", "1");
        String forumid = ServletRequestUtils.getStringParameter(request, "forumid", "0");
        int sort = ServletRequestUtils.getIntParameter(request, "sort", 1);
        // 当前页
        List<Forum> forums = null;

        List<Topic> list;
        Map<String, Object> params = Maps.newLinkedHashMap();
        forums = forumService.list(new Forum());
        try {
            // 判断显示板块方式，0表示全部
            if ("0".equals(forumid)) {
                Topic topic = new Topic();
                params.put("sort", sort);
                topic.setParams(params);
                startPage();
                list = topicService.list(topic);
                /*  Atomic __ */
                list.forEach( t -> {
                    t.setBrowse((long) AtomicIntegerUtil.getInstance(t.getClass(), t.getId(), t.getBrowse()).get());
                });
            } else {
                Forum forum = new Forum();
                forum.setId(forumid);

                Topic topic = new Topic();
                params.put("sort", sort);
                topic.setParams(params);
                startPage();
                topic.setForum(forum);
                list = topicService.list(topic);
            }
        } catch (Exception e) {
            Topic topic = new Topic();
            params.put("sort", 1);
            topic.setParams(params);
            startPage();
            list = topicService.list(topic);
            forumid = "0";
            sort = 1;
        }

//        list.forEach( t -> {
//            System.out.println(t.getThumbsUp());
//        });

        modelMap.put("forumid", forumid);
        modelMap.put("ForumList", forums);
        modelMap.put("pageModel", wrapTable(list));
        modelMap.put("sort", sort);
        modelMap.put("userId", getUserId());
        return "/front/topic";
    }

    @RequestMapping("/courseTopicList/{tcid}")
    public String courseTopicList(HttpServletRequest request, ModelMap modelMap, @PathVariable String tcid) throws ServletRequestBindingException {
        // 加载用户信息
        // 论坛的排序方式
        String forumid = ServletRequestUtils.getStringParameter(request, "forumid", "0");
        int sort = ServletRequestUtils.getIntParameter(request, "sort", 1);
        // 当前页
        List<Forum> forums;

        List<Topic> list;
        Map<String, Object> params = Maps.newLinkedHashMap();
        Forum f = new Forum();
        f.setTcid(tcid);
        forums = forumService.list(f);
        Collections.reverse(forums);
        try {
            // 判断显示板块方式，0表示全部
            if ("0".equals(forumid)) {
                Topic topic = new Topic();
                topic.setTcid(tcid);
                params.put("sort", sort);
                topic.setParams(params);
                startPage();
                list = topicService.list(topic);
                Collections.reverse(list);
            } else {
                Forum forum = new Forum();
                forum.setId(forumid);

                Topic topic = new Topic();
                topic.setTcid(tcid);
                params.put("sort", sort);
                topic.setParams(params);
                startPage();
                topic.setForum(forum);
                list = topicService.list(topic);
                Collections.reverse(list);
            }
        } catch (Exception e) {
            Topic topic = new Topic();
            topic.setTcid(tcid);
            params.put("sort", 1);
            topic.setParams(params);
            startPage();
            list = topicService.list(topic);
            Collections.reverse(list);
            forumid = "0";
            sort = 1;
        }
        modelMap.put("forumid", forumid);
        modelMap.put("selected", "forum");
        modelMap.put("ForumList", forums);
        modelMap.put("pageModel", wrapTable(list));
        modelMap.put("sort", sort);
        return "/front/learn";
    }

    @RequestMapping("/courseTopicList/toPush/{tcid}")
    public String toCourseTopicPush(ModelMap modelMap, @PathVariable String tcid) {
        /*
         * 判断权限
         * 如果是学生用户,就过滤 forumList
         */
        SysUser user = getUser();
        Forum f = new Forum();
        f.setTcid(tcid);
        List<Forum> forumList = forumService.list(f);
        Collections.reverse(forumList);
        if (user.getUserType() == UserConstants.USER_STUDENT) {
            List<Forum> tempForum = new ArrayList<>();
            for (Forum forum : forumList) {
                if (StringUtils.equalsIgnoreCase("1", forum.getType())) {
                    tempForum.add(forum);
                }
            }
            modelMap.put("forumList", tempForum);
        } else {
            modelMap.put("forumList", forumList);
        }
        modelMap.put("selected", "editor");
        return "/front/learn";
    }

    @RequestMapping("/topicList/toPush")
    public String toPush(ModelMap modelMap) {
        /*
         * 判断权限
         * 如果是学生用户,就过滤 forumList
         */
        SysUser user = getUser();
        List<Forum> forumList = forumService.list(new Forum());
        if (user.getUserType() == UserConstants.USER_STUDENT) {
            List<Forum> tempForum = new ArrayList<>();
            /*for (Forum forum : forumList) {
                if (StringUtils.equalsIgnoreCase("1", forum.getType())) {
                    tempForum.add(forum);
                }
            }*/
            modelMap.put("forumList", forumList);
        } else {
            modelMap.put("forumList", forumList);
        }
        return "/front/push";
    }

    @RequestMapping("/topic/add")
    @ResponseBody
    public Data topicAdd(HttpServletRequest request) throws Exception {
        /*
         * 判断权限
         * 如果是学生用户,就过滤 forumList
         */
        Topic topic = new Topic();
        org.apache.commons.beanutils.BeanUtils.populate(topic, request.getParameterMap());
        String talktitle = ServletRequestUtils.getStringParameter(request, "talktitle");
        String talkcontent = ServletRequestUtils.getStringParameter(request, "talkcontent");
        String forumId = ServletRequestUtils.getStringParameter(request, "forumid");
        String tcid = ServletRequestUtils.getStringParameter(request, "tcid", null);

        //  topic title 为空
        if (talktitle == null || talktitle.equals("")) {
            return Data.error("标题为空");
        }

        Forum f = forumService.getById(forumId);

        topic.setForum(f);
        if (tcid != null && !"".equals(tcid.trim())) {
            topic.setTcid(tcid);
        }
        // >> 当前可以直接获取的信息
        topic.setUserType(getUser().getUserType() + "");
        // IP地址，当前请求中的IP信息
        topic.setIpAddr(request.getRemoteAddr());
        topic.setThumbsUp(0L);
        topic.setBrowse(0L);
        topic.setContent(talkcontent);
        topic.setTitle(talktitle);
        topic.setCreateName(getUser().getUserName());
        topic.setCreateBy(getUserId());

        // 保存
        return toAjax(topicService.save(topic));
    }

    @RequestMapping("/reply/add")
    @ResponseBody
    public Data replyAdd(HttpServletRequest request, String replycontent, String topicId) throws Exception {
        /*
         * 判断权限
         * 如果是学生用户,就过滤 forumList
         */
        String tcid = ServletRequestUtils.getStringParameter(request, "tcid", null);
        //先维护更新Topic
        Topic topic = topicService.getById(topicId);

        topic.setReplyCount(topic.getReplyCount() + 1);
        Reply reply = new Reply();
        // 1，封装（已经封装了title, content, faceIcon）
        System.out.println("topicId::" + topicId);
        reply.setTopic(topic);
        if (tcid != null && !"".equals(tcid.trim())) {
            reply.setTcid(tcid);
        }
        reply.setCreateBy(getUserId());
        reply.setUserType(getUser().getUserType() + "");
        reply.setContent(replycontent);
        reply.setIpAddr(request.getRemoteAddr());
        reply.setThumbsUp(0L);
        reply.setCreateName(getUser().getUserName());
        // 2，保存
        topic.setLastReply(reply);
        replyService.save(reply);
        topicService.update(topic);
        return Data.success();
    }

    @RequestMapping("/topic/detail/{id}")
    public String topicDtail(ModelMap modelMap, @PathVariable String id) throws Exception {
        // 准备数据：topic
        Topic topic = topicService.getById(id);


        /*  Atomic __ */
        if (!AtomicIntegerUtil.isEmplyInMap(topic.getClass(), id))
            AtomicIntegerUtil.getInstance(topic.getClass(), id, topic.getBrowse());
        topic.setBrowse((long) AtomicIntegerUtil.getInstance(topic.getClass(), id).getAndIncrement());


        /*topic.setBrowse(topic.getBrowse() + 1);
        topicService.update(topic);*/

        modelMap.put("topic", topic);
        //准备被采纳数据：replyList
        Map<String, Object> params = Maps.newLinkedHashMap();
        params.put("adopt", 1);
        params.put("topic", topic);
        params.put("order_by", "r.create_time asc");
        Reply replyAdopt = replyService.getOne(params);
        modelMap.put("replyAdopt", replyAdopt);

        Reply reply = new Reply();
        reply.setAdopt(0);
        reply.setTopic(topic);
        params.clear();
        params.put("orderBy", "create_time ASC");
        reply.setParams(params);

        //准备其他回复：
        startPage();
        List<Reply> pageModel = replyService.list(reply);
        Collections.reverse(pageModel);
        Deck deck = new Deck();
        pageModel.forEach(p -> {
            deck.setReply(p);
            p.setDecks(new HashSet<>(deckService.list(deck)));
//            System.out.println(" reply dig count: " + p.getThumbsUp());
        });

        modelMap.put("pageModel", wrapTable(pageModel));
        // 保存
        return "/front/reply";
    }


    /**
     * 采纳评论
     * @param replyId
     * @return
     */
    @RequestMapping("/reply/adopt")
    public Data adopt(String replyId) {
        Reply reply = new Reply();
        reply.setId(replyId);
        reply.setAdopt(1);
        try {
            if (replyService.update(reply)) {
                return Data.success("success");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Data.success("failure");
    }

    /**
     *  - 提问与回复的点赞没有对应映射的数据表
     *  - 对点赞信息的持久化,在文章点赞数据表中实现
     */

    /**
     * 回复点赞
     *  - reply 类型 : 2
     * @param replyId
     * @return
     */
    @PostMapping("/reply/like")
    @ResponseBody
    public Data replyLike(String replyId) {
        SysUser sysUser = getUser();
        ArticleVotes articleVotes = new ArticleVotes();
        articleVotes.setUserId(sysUser.getId());
        articleVotes.setInfoType(2);
        articleVotes.setInfoId(replyId);

        String msg = null;
        //  查询是否已点赞
        if (articleService.checkArticleVotes(2, replyId, sysUser.getId())) {
            articleVotes = articleService.findArticleVotes(articleVotes).get(0);

            if (articleVotes.getDig() == 0) {
                articleVotes.setDig(1);
                msg = "+1success";
            } else {
                articleVotes.setDig(0);
                msg = "-1success";
            }
            articleService.updateArticleVotesById(articleVotes);
        } else {
            articleVotes.setDig(1);
            articleService.addArticleVotes(articleVotes);
            msg = "+1success";
        }
        //更新点赞数
        try {
            long digCount = articleService.selectDigCount(2, replyId);
            Reply reply = replyService.getById(replyId);
            reply.setThumbsUp(digCount);
            replyService.update(reply);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Data.success(msg);
    }

    /**
     * 提问点赞
     *  - topic 类型 : 1
     * @param topicId
     * @return
     */
    @PostMapping("/topic/like")
    public Data topicLike(String topicId) {
        SysUser sysUser = getUser();
        ArticleVotes articleVotes = new ArticleVotes();
        articleVotes.setUserId(sysUser.getId());
        articleVotes.setInfoType(1);
        articleVotes.setInfoId(topicId);

        String msg = null;
        //  查询是否已点赞
        if (articleService.checkArticleVotes(1, topicId, sysUser.getId())) {

            articleVotes = articleService.findArticleVotes(articleVotes).get(0);
            if (articleVotes.getDig() == 0) {
                articleVotes.setDig(1);
                msg = "+1success";
            } else {
                articleVotes.setDig(0);
                msg = "-1success";
            }
            articleService.updateArticleVotesById(articleVotes);
        } else {
            articleVotes.setDig(1);
            articleService.addArticleVotes(articleVotes);
            msg = "+1success";
        }
        // 更新点赞数
        try {
            long digCount = articleService.selectDigCount(1, topicId);
            Topic topic = topicService.getById(topicId);
            topic.setThumbsUp(digCount);
            topicService.update(topic);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Data.success(msg);
    }

    @RequestMapping("/courseTopic/{tcid}/detail/{id}")
    public String courseTopicDtail(ModelMap modelMap, @PathVariable String id, @PathVariable String tcid) throws Exception {
        // 准备数据：topic
        Topic topic = topicService.getById(id);

        /*topic.setBrowse(topic.getBrowse() + 1);
        topicService.update(topic);*/
        //浏览数+1
        /*  Atomic __ */
        if (!AtomicIntegerUtil.isEmplyInMap(topic.getClass(), id))
            AtomicIntegerUtil.getInstance(topic.getClass(), id, topic.getBrowse());
        topic.setBrowse((long) AtomicIntegerUtil.getInstance(topic.getClass(), id).getAndIncrement());

        modelMap.put("topic", topic);
        //准备被采纳数据：replyList
        Map<String, Object> params = Maps.newLinkedHashMap();
        params.put("adopt", 1);
        params.put("topic", topic);
        params.put("tcid", tcid);
        params.put("order_by", "r.create_time asc");
        Reply replyAdopt = replyService.getOne(params);
        modelMap.put("replyAdopt", replyAdopt);

        Reply reply = new Reply();
        reply.setAdopt(0);
        reply.setTopic(topic);
        reply.setTcid(tcid);
        params.clear();
        params.put("orderBy", "create_time ASC");
        reply.setParams(params);

        //准备其他回复：
        startPage();
        List<Reply> pageModel = replyService.list(reply);
        Collections.reverse(pageModel);
        Deck deck = new Deck();
        pageModel.forEach(p -> {
            deck.setReply(p);
            deck.setTcid(tcid);
            p.setDecks(new HashSet<>(deckService.list(deck)));
        });

        modelMap.put("pageModel", wrapTable(pageModel));
        modelMap.put("selected", "detail");

        // 保存
        return "/front/learn";
    }

    @RequestMapping("/deck/add")
    @ResponseBody
    public Data deckAdd(HttpServletRequest request, ModelMap modelMap) throws Exception {
        try {
            String content = ServletRequestUtils.getStringParameter(request, "content");
            String replyId = ServletRequestUtils.getStringParameter(request, "replyId");
            String toUserId = ServletRequestUtils.getStringParameter(request, "toUserId");
            String toUserName = ServletRequestUtils.getStringParameter(request, "toUserName");
            String toUserType = ServletRequestUtils.getStringParameter(request, "toUserType");
            String tcid = ServletRequestUtils.getStringParameter(request, "tcid");
            //准备数据
            Reply reply = replyService.getById(replyId);
            Deck deck = new Deck();
            deck.setContent(content);
            deck.setReply(reply);
            //设置作者的类型
            deck.setUserType(getUser().getUserType() + "");
            //设置回复对象信息
            deck.setToUserId(toUserId);
            deck.setToUserName(toUserName);
            deck.setToUserType(toUserType);
            deck.setCreateBy(getUserId());
            if (tcid != null && !"".equals(tcid)) {
                deck.setTcid(tcid);
            }
            deck.setIpAddr(request.getRemoteAddr());
            deck.setThumbsUp(0L);
            return toAjax(deckService.save(deck));
        } catch (Exception e) {
            e.printStackTrace();
            return Data.error();
        }
    }

    /**
     * 获取试卷列表
     * @param cid
     * @param studentId
     * @param chapterId
     * @param map
     * @return
     */
    @GetMapping("/course/exam/{cid}/{studentId}")
    public String toExam(@PathVariable String cid, @PathVariable String studentId, String chapterId, ModelMap map) {
        //获取课程作业的试卷信息
        UserTest userTest = new UserTest();
        userTest.setcId(cid);
        userTest.setUserId(studentId);
        userTest.setType("0");
        List<UserTest> userTestList = userTestService.getTcoExamPaper(userTest);
        map.put("paperList", userTestList);
        UserTest userTest1 = new UserTest();
        userTest1.setcId(cid);
        userTest1.setUserId(studentId);
        userTest1.setType("2");
        userTest1.setSetExam("1");
        List<UserTest> examTestList = userTestService.getTcoExamPaper(userTest1);
        map.put("examTestList", examTestList);
        map.put("studentId", getUser().getUserAttrId());
        return "/front/personalExamList";
    }


    /*     获取作业   */
    @Log(title = "获取作业")
    @PostMapping("/course/work/{studentId}")
    @ResponseBody
    public List<UserTest> workList(@PathVariable String studentId) {
        UserTest userTest = new UserTest();
        userTest.setUserId(studentId);
        userTest.setSumbitState("0");//    只显示未提交的作业
        userTest.setType(Constants.TEST_PAPER_TYPE_WORK);
        return userTestService.list(userTest);
    }

    /*     获取刷题   */
    @PostMapping("/course/shuati/{studentId}")
    @ResponseBody
    public List<ClbumCourseVo> shuatiList(@PathVariable String studentId) {
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        Student student = studentService.getById(studentId);
        ClbumCourse cc = new ClbumCourse();
        cc.setCid(student.getCid());
        startPage();
        List<ClbumCourse> list = clbumCourseService.list(cc);
        PageInfo<ClbumCourse> pageInfo = new PageInfo(list);
        //填充教师信息
        List<ClbumCourseVo> clbumCourseVos = new LinkedList<>();
        fillTeacherCourse(list, clbumCourseVos);
        return clbumCourseVos;
    }

    /*     获取试卷  */
    @PostMapping("/course/exam/{studentId}")
    @ResponseBody
    public List<UserTest> examList(@PathVariable String studentId) {
        UserTest userTest = new UserTest();
        userTest.setUserId(studentId);
        userTest.setSumbitState("0");//    只显示未提交的试卷
        userTest.setType(Constants.TEST_PAPER_TYPE_EXAM);
        return userTestService.list(userTest);
    }

    /*     获取试卷  */
    @PostMapping("/course/keqian/{studentId}")
    @ResponseBody
    public List<UserTest> frontList(@PathVariable String studentId) {
        UserTest userTest = new UserTest();
        userTest.setUserId(studentId);
        userTest.setSumbitState("0");//    只显示未提交的试卷
        userTest.setType("3");
        return userTestService.list(userTest);
    }


    /**
     * 保存用户评论内容
     *
     * @param
     * @return
     */
    @PostMapping("/course/comment/submit")
    @ResponseBody
    public Data comment(String parentId, String replyId, String replyUserId, String content, String typeId) throws Exception {
        Comment courseComment = new Comment();

        courseComment.setParentId(parentId);
        courseComment.setContent(content);
        courseComment.setReplyId(replyId);
        courseComment.setReplyUserId(replyUserId);
        courseComment.setType(ForumConstants.INFO_TYPE_COURSE);
        courseComment.setUserId(getUserId());
        courseComment.setUserType(UserConstants.USER_STUDENT);

        courseComment.setTypeId(typeId);

        return toAjax(commentService.save(courseComment));
    }

    /**
     * 获取用户文章评论内容
     *
     * @param typeId
     * @return
     */
    @RequestMapping("/course/comments/{typeId}")
    @ResponseBody
    public Data articleComment(@PathVariable("typeId") String typeId, HttpServletRequest request) throws Exception {
        int pageSize = ServletRequestUtils.getIntParameter(request, "pageSize", defaultPageSize);
        int pageNum = ServletRequestUtils.getIntParameter(request, "pageNum", defaultPageNum);

        //用户id
        String userId = ServletRequestUtils.getStringParameter(request, "userId");
        String createTime = ServletRequestUtils.getStringParameter(request, "createTime");
        String orderBy = ServletRequestUtils.getStringParameter(request, "orderBy");
        String order = ServletRequestUtils.getStringParameter(request, "order");

        String parentId = "0";

        //分页获取父级信息
        //分页数据量以父级数据量为基础
        PageVo<Comment> pageVo = courseService.getCourseCommentListPage(
                typeId, userId, createTime, parentId, orderBy, order, pageNum, pageSize);
        List<Comment> list = pageVo.getList();

        List<String> userIds = new LinkedList<>();
        List<ArticleCommentVo> commentVos = fillChildrenCommen(list, userIds);

        //先填充评论信息再处理用户信息
        fillUser(commentVos, userIds);

        PageVo<ArticleCommentVo> page = new PageVo<>(pageVo.getPageNum());
        BeanUtils.copyProperties(pageVo, page);
        page.setList(commentVos);
        return Data.success().put(Data.RESULT_KEY_DATA, page);
    }

    /**
     * @param list 父评论信息
     * @return
     */
    private List<ArticleCommentVo> fillChildrenCommen(List<Comment> list, List<String> userIds) {

        if (list.isEmpty()) {
            return new LinkedList<>();
        }

        //获取子级评论
        Set<String> parentIds = Sets.newLinkedHashSet();
        list.forEach(l -> parentIds.add(l.getId()));
        List<Comment> childrenCommentList = commentService.listByPids(Lists.newArrayList(parentIds));

        //将子评论转为vo类型
        List<ArticleCommentVo> childrenCommentVos = new LinkedList<>();
        childrenCommentList.forEach(l -> {
            userIds.add(l.getUserId());
            ArticleCommentVo vo = new ArticleCommentVo();
            BeanUtils.copyProperties(l, vo);
            childrenCommentVos.add(vo);
        });

        //将父评论转为vo类型
        List<ArticleCommentVo> parentCommentVos = new LinkedList<>();
        list.forEach(l -> {
            userIds.add(l.getUserId());
            ArticleCommentVo vo = new ArticleCommentVo();
            BeanUtils.copyProperties(l, vo);
            parentCommentVos.add(vo);
        });

        //构建评论结构
        parentCommentVos.forEach(l ->
                l.setChildren(
                        childrenCommentVos.stream().filter(articleComment -> l.getId()
                                .equals(articleComment.getParentId())).sorted(Comparator
                                .comparing(BaseEntity::getCreateTime)).collect(Collectors.toList())
                ));
        return parentCommentVos;
    }

    private void fillUser(List<ArticleCommentVo> list, List<String> userIds) {
        Map<String, SysUser> userMap = userService.mapByIds(userIds);

        list.forEach(l -> {
            l.setUser(userMap.get(l.getUserId()));
            List<ArticleCommentVo> children = l.getChildren();
            if (children != null) {
                children.forEach(c -> {
                    c.setUser(userMap.get(c.getUserId()));
                    c.setParentUser(userMap.get(c.getReplyUserId()));
                });
            }
        });

    }

    /**
     * 文章详细页面
     *
     * @param cid
     * @param p
     * @param modelMap
     * @return
     */
    @GetMapping(value = "/course/{cid}/comment")
    public String detail(@PathVariable(value = "cid") String cid, @RequestParam(value = "p", defaultValue = "1") int p, ModelMap modelMap) {
        Course course = courseService.getById(cid);
        modelMap.put("course", course);
        TeacherCourse teacherCourse = courseService.getTeacherCourse(getUser().getUserAttrId(), cid);
        Teacher teacher = teacherService.getById(teacherCourse.getTid());
        Map<String, Object> params = new LinkedHashMap<>();
        params.put("userType", UserConstants.USER_TEACHER);
        params.put("userAttrId", teacher.getId());
        SysUser user = userService.getOne(params);
        modelMap.put("teacher", teacher);
        modelMap.put("teacherUser", user);
        modelMap.addAttribute("typeId", teacherCourse.getId());
        modelMap.addAttribute("p", p);
        modelMap.addAttribute("type", ForumConstants.INFO_TYPE_COURSE);
        return FRONT_USER_PREFIX + "/course_forum";
    }

    /**
     * 文章详细页面
     *
     * @param cid
     * @param modelMap
     * @return
     */
    @GetMapping(value = "/course/{cid}/outline")
    public String outline(@PathVariable(value = "cid") String cid, ModelMap modelMap) {
        Course course = courseService.getById(cid);
        modelMap.put("course", course);
        TeacherCourse teacherCourse = courseService.getTeacherCourse(getUser().getUserAttrId(), cid);
        Teacher teacher = teacherService.getById(teacherCourse.getTid());
        Map<String, Object> params = new LinkedHashMap<>();
        params.put("userType", UserConstants.USER_TEACHER);
        params.put("userAttrId", teacher.getId());
        SysUser user = userService.getOne(params);
        modelMap.put("teacher", teacher);
        modelMap.put("teacherUser", user);
        modelMap.addAttribute("typeId", teacherCourse.getId());

        params.clear();
        params.put("tcId", teacherCourse.getId());

        Outline outline = outlineService.getOne(params);
        modelMap.addAttribute("outline", outline);
        modelMap.addAttribute("type", ForumConstants.INFO_TYPE_COURSE);
        return FRONT_USER_PREFIX + "/outline";
    }

    private List<ChapterVo> transTree(List<Chapter> chapters) {
        Map<String, ChapterVo> chapterMap = new LinkedHashMap<>();
        chapters.forEach(c -> {
            ChapterVo chapterVo = new ChapterVo();
            BeanUtils.copyProperties(c, chapterVo);
            chapterMap.put(c.getId(), chapterVo);
        });
        Map<String, ChapterVo> tree = new LinkedHashMap<>();


        chapters.forEach(c -> {
            if ("0".equals(c.getPid())) {
                tree.put(c.getId(), chapterMap.get(c.getPid()));
            } else {
                ChapterVo chapterVo = chapterMap.get(c.getPid());
                tree.putIfAbsent(chapterVo.getId(), chapterVo);
                tree.get(chapterVo.getId()).getChildren().add(c);
            }

        });
        return Lists.newArrayList(tree.values());
    }

    private void fillTeacherCourse(List<ClbumCourse> list, List<ClbumCourseVo> clbumCourseVos) {
        if (list == null || list.isEmpty()) {
            return;
        }
        List<String> tcId = new LinkedList<>();
        list.forEach(l -> tcId.add(l.getTcid()));
        // 根据课程id查找教师信息
        Map<String, TeacherCourse> teacherCourseMap = teacherCourseService.mapByIds(tcId);
        tcId.clear();
        teacherCourseMap.forEach((k, v) -> tcId.add(v.getTid()));

        Map<String, SysUser> sysUserMap = userService.listByTids(tcId);

        List<String> cid = new LinkedList<>();
        list.forEach(l -> cid.add(l.getCid()));

        Map<String, Course> courseMap = courseService.mapByClbumId(cid);

        List<String> tid = new LinkedList<>();
        teacherCourseMap.forEach((k, v) -> tid.add(v.getTid()));
        Map<String, Teacher> teachers = teacherService.mapByIds(tid);

        list.forEach(l -> {
            ClbumCourseVo cc = new ClbumCourseVo();
            cc.setTeacher(teachers.get(teacherCourseMap.get(l.getTcid()).getTid()));
            TeacherCourse teacherCourse = teacherCourseMap.get(l.getTcid());
            cc.setTeacherCourse(teacherCourse);
            cc.setCourse(courseMap.get(teacherCourse.getCid()));
            cc.setSysUser(sysUserMap.get(teacherCourse.getTid()));
            clbumCourseVos.add(cc);
        });
    }


    @GetMapping(value = "/forum/{infoType}")
    public String people(@RequestParam(value = "p", defaultValue = "1") int p, ModelMap modelMap, @PathVariable int infoType) {
        modelMap.addAttribute("p", p);
        modelMap.addAttribute("count", studentService.statisticsById(getUserId()));
        switch (infoType) {
            case ForumConstants.INFO_TYPE_ANSWER:
                infoType = ForumConstants.INFO_TYPE_ANSWER;
                break;
            case ForumConstants.INFO_TYPE_QUESTION:
                infoType = ForumConstants.INFO_TYPE_QUESTION;
                break;
            case ForumConstants.INFO_TYPE_ARTICLE:
                infoType = ForumConstants.INFO_TYPE_ARTICLE;
                break;
            case ForumConstants.INFO_TYPE_SHARE:
                infoType = ForumConstants.INFO_TYPE_SHARE;
                break;
            default:
                infoType = ForumConstants.INFO_TYPE_QUESTION;
                break;
        }
        modelMap.addAttribute("infoType", infoType);
        return FRONT_USER_PREFIX + "/content";
    }


    /**
     * 查看文章列表
     *
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public String posts(HttpServletRequest request, ModelMap model) {
        int pageNum = ServletRequestUtils.getIntParameter(request, "pn", 1);
        model.put("pn", pageNum);
        return FRONT_USER_PREFIX + "/posts";
    }

    /**
     * 查看收藏列表
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/favors", method = RequestMethod.GET)
    public String favors(ModelMap model) {
        Favorite favorite = new Favorite();
        /*favorite.setUid(getUserId());*/

        startPage();
        List<Favorite> list = favoriteService.list(favorite);
        model.put("page", list);
        return FRONT_USER_PREFIX + "/favors";
    }

    @RequestMapping("/checkFavor/{id}")
    @ResponseBody
    public Data checkFavor(@PathVariable String id) {
        if (id != null) {
            boolean b = favoriteService.checkFavoriteByUser(getUserId(), ForumConstants.INFO_TYPE_ARTICLE, id);
            if (b) {
                return Data.success();
            }
        }
        return Data.error();
    }


    @RequestMapping("/comment/delete/{id}")
    @ResponseBody
    public Data commentDel(@PathVariable String id) throws Exception {
        if (id != null) {
            Comment comment = commentService.getById(id);

            if (null != comment) {
                Assert.isTrue(getUserId().equals(comment.getUserId()), "认证失败");
                commentService.removeById(id);
            }
            return Data.success("删除成功");
        }
        return Data.error("删除失败");
    }

    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public String profile(ModelMap model) {
        SysUser user = userService.getById(getUserId());
        Student student = studentService.getById(user.getUserAttrId());
        StudentVo studentVo = trans(user, student);
        //填充班级信息
        fillClbum(studentVo);
        fillGrades(studentVo);
        fillMajor(studentVo);
        fillDepartment(studentVo);

        model.put("user", user);
        return FRONT_USER_PREFIX + "/profile";
    }

    @RequestMapping("/profile/update")
    public String updateProfile(SysUser user, ModelMap model) {

        SysUser sysUser = null;
        try {
            sysUser = new SysUser();
            sysUser.setId(getUserId());
            sysUser.setEmail(user.getEmail());
            if (user.getPassword() != null) {
                sysUser.setPassword(ShiroUtils.encryptPassword(user.getLoginName(), user.getPassword(), user.getSalt()));
            }
            sysUser.setSignature(user.getSignature());
            userService.update(sysUser);
        } catch (Exception e) {
            model.put("message", e.getMessage());
        }

        return FRONT_USER_PREFIX + "/profile";
    }

    /*@RequestMapping("/notifies")
    public String notifies(ModelMap model) {
        Message message = new Message();
        message.setSourceUserId(getUserId());

        startPage();
        List<Message> list = messageService.list(message);

        list.forEach(l -> {
            l.setStatus(Constants.MSG_STATUS_READED);
            messageService.update(l);
        });
        model.put("page", list);
        return FRONT_USER_PREFIX + "/notifies";
    }*/


    @RequestMapping("/avatar/upload")
    @ResponseBody
    public Data upload(@RequestParam(value = "file") MultipartFile file, HttpServletRequest request) {
        Map<String, Object> data = new HashMap<>(4);
        String originPath = ServletRequestUtils.getStringParameter(request, "o", null);
        int crop = ServletRequestUtils.getIntParameter(request, "crop", 0);
        int size = 265;

        String path;
        Data result = null;
        try {
            //储存图片
            if (crop == 1) {
                result = FileUploadUtils.upload(StorageConstants.STORAGE_THUMBNAIL, file, false, FileUtils.allowImage);
            }
            if (StringUtil.nvl(result.get(Data.RESULT_KEY_CODE).toString(), "").equals(Constants.FAILURE)) {
                return result;
            }
            String filename = file.getOriginalFilename();
            data.put("name", filename);
            data.put("type", getSuffix(filename != null ? filename : ""));
            data.put("path", result.get(StorageConstants.FILE_ID));
            data.put("size", file.getSize());
        } catch (Exception e) {
            return Data.error(e.getMessage());
        }
        Data success = Data.success();
        success.putAll(data);
        return success;
    }

    @RequestMapping(value = "/avatar/update", method = RequestMethod.POST)
    @ResponseBody
    public Data avatarUpdate(String path, Float x, Float y, Float width, Float height) throws Exception {

        Data data = Data.success("修改成功");
        if (StringUtils.isEmpty(path)) {
            return Data.error("请选择图片");
        }

        SysUser user = userService.getById(getUserId());
        user.setAvatar(path);
        userService.update(user);
        return data;
    }

    @RequestMapping(value = "/toPassword", method = RequestMethod.GET)
    public String password() {
        return FRONT_USER_PREFIX + "/password";
    }

    @RequestMapping(value = "/password", method = RequestMethod.POST)
    @ResponseBody
    public Data post(@Param("oldPassword") String oldPassword,@Param("password") String password, RedirectAttributesModelMap modelMap) {
        Data data = null;
        try {
            SysUser user = userService.getById(getUserId());

            Assert.hasLength(password, "密码不能为空！");
            if (null != user) {
                SysUser sysUser = new SysUser();
                sysUser.setPassword(ShiroUtils.encryptPassword(user.getLoginName(), oldPassword, user.getSalt()));
                sysUser.setId(getUserId());
                List<SysUser> list = userService.list(sysUser);
                Assert.isTrue(list != null && list.size() == 1, "旧密码不正确");
                user.setSalt(ShiroUtils.randomSalt());
                user.setPassword(ShiroUtils.encryptPassword(user.getLoginName(), password, user.getSalt()));
                boolean update = userService.update(user);
                data = Data.success("success");
                //  修改成功，清除subject
                Subject currentUser = SecurityUtils.getSubject();
                if (SecurityUtils.getSubject().getSession() != null)
                    currentUser.logout();
            }
        } catch (Exception e) {
            e.printStackTrace();
            data = Data.error(e.getMessage());
        }
        modelMap.addFlashAttribute("data", data);
//        return redirect(storageContextPath + "/user/toPassword");
        return data;
    }
    @RequestMapping(value = "/email", method = RequestMethod.POST)
    @ResponseBody
    public Data post(@Param("stuemail") String stuemail, RedirectAttributesModelMap modelMap) {
        Data data = null;
        try {
            SysUser user = userService.getById(getUserId());
            if (null != user) {
                SysUser sysUser = new SysUser();
                user.setEmail(stuemail);
                System.out.printf("获得邮箱"+stuemail);
//                sysUser.setPassword(ShiroUtils.encryptPassword(user.getLoginName(), oldPassword, user.getSalt()));
                sysUser.setId(getUserId());
                boolean update = userService.update(user);
                data = Data.success("success");
                //  修改成功，清除subject
                Subject currentUser = SecurityUtils.getSubject();
                if (SecurityUtils.getSubject().getSession() != null)
                    currentUser.logout();
            }
        } catch (Exception e) {
            e.printStackTrace();
            data = Data.error(e.getMessage());
        }
        modelMap.addFlashAttribute("data", data);
        return data;
    }
    @RequestMapping("/log")
    public String log() {
        return FRONT_USER_PREFIX + "/log";
    }


    /**
     * 页面跳转
     *
     * @return
     */
    @GetMapping("/stuToTest")
    public String stuToTest(String testPaperId, String studentId, String tutId, ModelMap modelMap) {
        TestPaper testPaper = new TestPaper();
        testPaper = testPaperService.getById(testPaperId);
        modelMap.put("testPaper", testPaper);
        modelMap.put("paperId", testPaperId);
        modelMap.put("studentId", studentId);
        modelMap.put("tutId", tutId);
        modelMap.put("stuName", ShiroUtils.getLoginName());
        modelMap.put("name", getUser().getUserName());
        return "/front/stuTest";
    }


    /**
     * 页面跳转 章节测试
     *
     * @return
     */
    @GetMapping("/chapterTest")
    public String chapterTest(String testPaperId, String studentId, String tutId, ModelMap modelMap) {
        TestPaper testPaper = new TestPaper();
        testPaper = testPaperService.getById(testPaperId);
        System.out.println("testPaper:" + testPaper);
        modelMap.put("testPaper", testPaper);
        modelMap.put("paperId", testPaperId);
        modelMap.put("studentId", getUser().getUserAttrId());
        modelMap.put("stuName", ShiroUtils.getLoginName());
        modelMap.put("name", getUser().getUserName());
        return "front/exercise/chapterWork";
    }
    /**
     * 页面跳转 在线考试
     *
     * @return
     */
    @GetMapping("/chapterExam")
    public String chapterExam(String testPaperOneId, String studentId, String tutId, ModelMap modelMap) {
        TestPaperOne testPaper = new TestPaperOne();
        testPaper = testPaperOneService.getById(testPaperOneId);
        System.out.println("testPaper:" + testPaper);
        modelMap.put("testPaper", testPaper);
        modelMap.put("paperId", testPaperOneId);
        modelMap.put("studentId", getUser().getUserAttrId());
        modelMap.put("stuName", ShiroUtils.getLoginName());
        modelMap.put("name", getUser().getUserName());
        return "front/exercise/chapterExamWork";
    }
    /**
     * 发送通知
     *
     * @param postId
     */
    private void sendMessage(String postId) {
        NotifyEvent event = new NotifyEvent("MessageEvent" + System.currentTimeMillis());
        event.setSourceUserId(getUserId());
        event.setEventType(EventConstants.EVENT_ARTICLE_FAVORITES);
        // 此处不知道文章作者, 让通知事件补全
        event.setTargetId(postId);
    }

    /**
     * 批量转换teacherVo类型
     *
     * @param users
     * @param teachers
     * @return
     */
    private List<TeacherVo> trans(List<SysUser> users, Map<String, Teacher> teachers) {
        List<TeacherVo> teacherVos = new LinkedList<>();
        users.forEach(u -> teacherVos.add(trans(u, teachers.get(u.getUserAttrId()))));
        return teacherVos;
    }

    /**
     * 转换teacherVo类型
     *
     * @param teacher
     * @return
     */
    private TeacherVo trans(SysUser users, Teacher teacher) {
        TeacherVo teacherVo = new TeacherVo();
        BeanUtils.copyProperties(users, teacherVo);
        teacherVo.setTeacher(teacher);
        return teacherVo;
    }

    /**
     * 转换studentVo类型
     *
     * @param student
     * @return
     */
    private StudentVo trans(SysUser users, Student student) {
        StudentVo studentVo = new StudentVo();
        BeanUtils.copyProperties(users, studentVo);
        studentVo.setStudent(student);
        return studentVo;
    }

    /**
     * 获取班级信息
     *
     * @param studentVos
     */
    private void fillClbum(StudentVo studentVos) {
        Clbum clbum = clbumService.getById(studentVos.getStudent().getCid());
        studentVos.setClbum(clbum);
    }

    /**
     * 获取年级信息
     *
     * @param studentVos
     */
    private void fillGrades(StudentVo studentVos) {
        Grades grades = gradesService.getById(studentVos.getStudent().getGid());
        studentVos.setGrades(grades);
    }

    /**
     * 获取专业信息
     *
     * @param studentVos
     */
    private void fillMajor(StudentVo studentVos) {
        Major major = majorService.getById(studentVos.getClbum().getMid());
        studentVos.setMajor(major);
    }

    /**
     * 获取系部信息
     *
     * @param studentVos
     */
    private void fillDepartment(StudentVo studentVos) {
        Department department = departmentService.getById(studentVos.getMajor().getDid());
        studentVos.setDepartment(department);
    }

    private String getSuffix(String path) {
        int pos = path.lastIndexOf(".");
        return path.substring(pos);
    }

    /**
     * 进入刷题页面
     */
    @GetMapping("/personal/shuati/{cid}")
    private String shuati(@PathVariable String cid,HttpServletRequest request, ModelMap map){
        System.out.println("????????????????????????"+cid);
        Course course = courseService.getById(cid);
        System.out.println("????????????????????????"+course.toString());
        Chapter chapter = new Chapter();
        chapter.setCid(course.getId());
        List<Chapter> courseList = chapterService.list(chapter);
        System.out.println(transTree(courseList).toString());
        map.put("course", course);
        map.put("chapter", transTree(courseList));
        return "/front/shuati";
    }

    /**
     * 进入章节刷题
     */
    @RequestMapping("/brushQuestion")
    private String brushQuestion(HttpServletRequest request, ModelMap map)throws IOException {
        String myQuestionsIds="";
        String chid=request.getParameter("chid");
        System.out.println("!!!!!!!!!!!!!!!!!!"+chid);
        MyQuestions myQuestions=new MyQuestions();
        myQuestions.setChapterId(chid);
        List<MyQuestions> list=myQuestionsService.selectList(myQuestions);
        Chapter chapter=new Chapter();
        chapter.setPid(chid);
        List<Chapter> chapters=chapterService.selectList(chapter);
        for(Chapter s : chapters){
            System.out.println("******************"+s.getId());
            myQuestions.setChapterId(s.getId());
            list.addAll(myQuestionsService.selectList(myQuestions));
        }
        for (MyQuestions q : list){
            System.out.println("??????????"+q);
        }
        if(list.size()==0){
            System.out.println("此章节没有答案");
            return null;
        }
        List<Shuati> shuatis=new ArrayList<Shuati>();
        for (MyQuestions q : list){
            myQuestionsIds=myQuestionsIds+q.getId()+",";
            Shuati shuati=new Shuati();
            shuati.setMyQuestions(q);
            shuatis.add(shuati);
        }
        List<MyOptionAnswer[]> myOptionAnswers=new ArrayList<MyOptionAnswer[]>();
        for(int i=0;i<list.size();i++){
            String myoptionAnswerArrString=list.get(i).getMyoptionAnswerArr();
            String[] myoptionAnswer=myoptionAnswerArrString.split(";");
            List<MyOptionAnswer> myOptionAnswersArr=new ArrayList<MyOptionAnswer>();
            for(int j=0;j<myoptionAnswer.length;j++){
                MyOptionAnswer myOptionAnswer= myOptionAnswerService.getById(myoptionAnswer[j]);
                myOptionAnswersArr.add(myOptionAnswer);
            }
            shuatis.get(i).setMyOptionAnswerList(myOptionAnswersArr);
        }
        map.put("myQuestionsIds",myQuestionsIds);
        map.put("shuatis",shuatis);
        map.put("listsize",shuatis.size());
        for(Shuati s: shuatis){
            System.out.println(s.toString());
        }
        return "/front/shuatishow";
    }


    /**
     * 提交刷题
     */
    @RequestMapping("/submitShuati")
    private @ResponseBody Integer submitShuati(@RequestBody List<ShuatiHistory> shuatiHistories){
        System.out.println("进入提交答案");
          for(ShuatiHistory s : shuatiHistories){
              s.setUserId(getUserId());
          }
          System.out.println(shuatiHistories.toString());
          try{
              shuatiHistoryService.saveBatch(shuatiHistories);
          }catch (Exception e){
              e.printStackTrace();
              return 0;
          }
        return 1;
    }
}
