package cn.wstom.web.controller.school.onlineExam;

import cn.wstom.common.annotation.Log;
import cn.wstom.common.base.Data;
import cn.wstom.common.enums.ActionType;
import cn.wstom.jiaowu.entity.Course;
import cn.wstom.jiaowu.service.CourseService;
import cn.wstom.main.web.base.BaseController;
import cn.wstom.main.web.page.TableDataInfo;
import cn.wstom.onlineexam.entity.MyTitleType;
import cn.wstom.onlineexam.entity.TitleType;
import cn.wstom.onlineexam.service.MyTitleTypeService;
import org.apache.ibatis.annotations.Param;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 我的题型 信息操作处理
 *
 * @author hzh
 * @date 20190307
 */
@Controller
@RequestMapping("/school/onlineExam/myTitleType")
public class MyTitleTypeController extends BaseController {
    private String prefix = "school/onlineExam/myTitleType";

    @Autowired
    private MyTitleTypeService myTitleTypeService;

    @Autowired
    private cn.wstom.onlineexam.service.TitleTypeService TitleTypeService;

    @Autowired
    private CourseService courseService;


    @RequiresPermissions("teacher:course:view")
    @GetMapping("/find//{cid}")
    public String toList(@PathVariable("cid") String cid, ModelMap mmap) {
        List<MyTitleType> list = myTitleTypeService.findByCid(cid);
        mmap.put("TitleType", list);
        mmap.put("cid", cid);
        return prefix + "/list";
    }


    /**
     * 查询我的题型列表
     */
    @RequiresPermissions("teacher:course:view")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(@Param("cid") String cid) {
        startPage();
        MyTitleType myTitleType = new MyTitleType();
        myTitleType.setCreateId(getUser().getUserAttrId());
        myTitleType.setCid(cid);
        List<MyTitleType> list = myTitleTypeService.list(myTitleType);
        return wrapTable(list);
    }

    /**
     * 新增我的题型
     */
    @RequiresPermissions("teacher:course:view")
    @GetMapping("/add/{id}")
    public String toAdd(@PathVariable("id") String id, ModelMap modelMap) {
        List<TitleType> titleType = TitleTypeService.list(new TitleType());
        Course course = courseService.getById(id);
        modelMap.put("titleType", titleType);
        modelMap.put("course", course);
        return prefix + "/add";
    }

    /**
     * 新增保存我的题型
     */
    @RequiresPermissions("teacher:course:view")
    @Log(title = "我的题型", actionType = ActionType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public Data add(MyTitleType myTitleType) throws Exception {
        myTitleType.setCreateBy(getLoginName());
        myTitleType.setCreateId(getUser().getUserAttrId());
        return toAjax(myTitleTypeService.save(myTitleType));
    }


    /**
     * 修改我的题型
     */
    @GetMapping("/edit/{id}")
    @RequiresPermissions("teacher:course:view")
    public String toEdit(@PathVariable("id") Integer id, ModelMap mmap) {

        MyTitleType myTitleType = myTitleTypeService.getById(id);
        TitleType titleType = TitleTypeService.getById(myTitleType.getPublicTitleId());
        mmap.put("titleType", titleType);
        mmap.put("myTitleType", myTitleType);
        return prefix + "/edit";
    }

    /**
     * 修改保存我的题型
     */
    @RequiresPermissions("teacher:course:view")
    @Log(title = "我的题型", actionType = ActionType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public Data edit(MyTitleType myTitleType) throws Exception {
        myTitleType.setUpdateBy(getLoginName());
        myTitleType.setUpdateId(getUser().getUserAttrId());
        return toAjax(myTitleTypeService.update(myTitleType));
    }

    /**
     * 删除我的题型
     */
    @RequiresPermissions("teacher:course:view")
    @Log(title = "我的题型", actionType = ActionType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public Data remove(String ids) throws Exception {
        return toAjax(myTitleTypeService.removeById(ids));
    }

}
