package cn.wstom.web.controller.common;

import cn.wstom.main.web.base.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 评论控制层，将所有评论功能抽取统一处理，成为公共模块
 *
 * @author dws
 * @date 2019/04/07
 */
@Controller
@RequestMapping("/comment")
public class CommentController extends BaseController {


}
