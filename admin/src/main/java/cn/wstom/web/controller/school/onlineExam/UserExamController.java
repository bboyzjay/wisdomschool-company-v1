package cn.wstom.web.controller.school.onlineExam;

import cn.wstom.common.annotation.Log;
import cn.wstom.common.base.Data;
import cn.wstom.common.enums.ActionType;
import cn.wstom.main.util.ShiroUtils;
import cn.wstom.main.web.base.BaseController;
import cn.wstom.main.web.page.TableDataInfo;
import cn.wstom.onlineexam.entity.TestPaper;
import cn.wstom.onlineexam.entity.TestPaperOne;
import cn.wstom.onlineexam.entity.UserExam;
import cn.wstom.onlineexam.entity.UserTest;
import cn.wstom.onlineexam.service.TestPaperOneService;
import cn.wstom.onlineexam.service.TestPaperService;
import cn.wstom.onlineexam.service.UserExamService;
import cn.wstom.onlineexam.service.UserTestService;
import cn.wstom.system.entity.SysUser;
import cn.wstom.system.service.SysUserService;
import cn.wstom.web.controller.school.onlineExam.common.util.Uuid;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 试卷与学生关联 信息操作处理
 *
 * @author hzh
 * @date 20190223
 */
@Controller
@RequestMapping("/school/onlineExam/userExam")
public class UserExamController extends BaseController {
    private String prefix = "school/onlineExam/userExam";

    @Autowired
    private UserExamService userExamService;
    @Autowired
    private TestPaperOneService testPaperOneService;

    @Autowired
    private SysUserService sysUserService;

    @RequiresPermissions("teacher:course:view")
    @GetMapping("/{cid}")
    public String toList(@PathVariable String cid, ModelMap modelMap) {
        System.out.println("cid::::" + cid);
        modelMap.put("cid", cid);
        return prefix + "/list";
    }

    /**
     * 查询测试关联列表
     */
    @RequiresPermissions("teacher:course:view")
    @PostMapping("/list/{cid}")
    @ResponseBody
    public TableDataInfo list(@PathVariable String cid, UserExam userTest) {
        startPage();
        userTest.setcId(cid);
        userTest.setCreateId(getUser().getUserAttrId());
        userTest.setType("1");
        SysUser sysUser = new SysUser();
        sysUser = sysUserService.getById(ShiroUtils.getUserId());
        String tid = sysUser.getUserAttrId();
        userTest.setCreateId(tid);
        List<UserExam> list = userExamService.list(userTest);
        return wrapTable(list);
    }

    @PostMapping("/getlist")
    @ResponseBody
    public List<TestPaperOne> getlist(String cid) {
        startPage();
        TestPaperOne testPaper = new TestPaperOne();
        testPaper.setCreateId(getUser().getUserAttrId());
        testPaper.setCoursrId(cid);
        testPaper.setType("1");
        return testPaperOneService.list(testPaper);
    }

    @PostMapping("/getlistExam")
    @ResponseBody
    public List<TestPaperOne> getlistExam(String cid) {
        startPage();
        TestPaperOne testPaper = new TestPaperOne();
        testPaper.setCreateId(getUser().getUserAttrId());
        testPaper.setCoursrId(cid);
        testPaper.setType("1");
        return testPaperOneService.list(testPaper);
    }



    /**
     * 修改
     */
    @GetMapping("/edit/{id}")
    @RequiresPermissions("teacher:course:view")
    public String toEdit(@PathVariable("id") Integer id, ModelMap mmap) {
        UserExam userTest = userExamService.getById(id);
        TestPaperOne testPaper = testPaperOneService.getById(userTest.getTestPaperOneId());
        mmap.put("userTest",userTest);
        mmap.put("testPaper",testPaper);
        return prefix + "/edit";
    }

    /**
     * 修改保存
     */
    @RequiresPermissions("teacher:course:view")
    @Log(title = "我的测试", actionType = ActionType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public Data edit(UserExam UserTest) throws Exception {
        SysUser sysUser = new SysUser();
        sysUser = sysUserService.getById(ShiroUtils.getUserId());
        String tid1 = sysUser.getUserAttrId();
        UserTest.setUpdateId(tid1);
        UserTest.setUpdateBy(getLoginName());
        try {
            return toAjax(userExamService.update(UserTest));
        } catch (Exception e) {
            System.out.println(e.getCause());
            return error(e.getMessage());
        }
    }

    /**
     * 新增测试关联
     */
    @RequiresPermissions("teacher:course:view")
    @GetMapping("/add/{cid}")
    public String toAdd(@PathVariable String cid, ModelMap modelMap) {
        SysUser sysUser = new SysUser();
        sysUser = sysUserService.getById(ShiroUtils.getUserId());
        String tid = sysUser.getUserAttrId();
        modelMap.put("cid", cid);
        modelMap.put("tid", tid);
        return prefix + "/add";
    }

    /**
     * 新增保存测试关联
     */
    @RequiresPermissions("teacher:course:view")
    @Log(title = "测试关联", actionType = ActionType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public Data add(UserExam userTest) {
        String[] userId = userTest.getUserId().split(",");
        List<UserExam> list = new ArrayList<UserExam>();
        Uuid uuid = new Uuid();
        try {
            for (int i = 0; i < userId.length; i++) {
                UserExam userTest2 = new UserExam();
                userTest2.setTestPaperOneId(userTest.getTestPaperOneId());
                userTest2.setUserId(userId[i]);
                List<UserExam> userTestList = new ArrayList<UserExam>();
                userTestList = userExamService.list(userTest2);
                if (userTestList.size() == 0) {
                    UserExam userTest1 = new UserExam();
                    userTest1.setTestPaperOneId(userTest.getTestPaperOneId());
                    userTest1.setUserId(userId[i]);
                    userTest1.setStuStartTime(userTest.getStuStartTime());
                    userTest1.setStuEndTime(userTest.getStuEndTime());
                    userTest1.setCreateBy(ShiroUtils.getLoginName());
                    userTest1.setSumbitState("0"); //0表示未提交,1为提交
                    userTest1.setcId(userTest.getcId());
                    SysUser sysUser = new SysUser();
                    sysUser = sysUserService.getById(ShiroUtils.getUserId());
                    String tid1 = sysUser.getUserAttrId();
                    userTest1.setCreateId(tid1);
                    list.add(userTest1);
                }
            }
            return toAjax(userExamService.saveBatch(list));
        } catch (Exception e) {
            System.out.println(e.getCause());
            return error("记录已存在");
        }
    }


    @GetMapping("/fabu")
    @RequiresPermissions("teacher:course:view")
    public String fabu(String id,String cid,ModelMap modelMap){
        modelMap.put("id",id);
        TestPaperOne testPaper = testPaperOneService.getById(id);
        modelMap.put("cid",testPaper.getCoursrId());
        modelMap.put("tid", getUser().getUserAttrId());
         modelMap.put("rule",testPaper.getRule());
        System.out.println("###################");
        return prefix + "/fabu";
    }


    @RequiresPermissions("teacher:course:view")
    @Log(title = "测试关联", actionType = ActionType.INSERT)
    @PostMapping("/addfabu")
    @ResponseBody
    public Data addfabu(UserExam userTest) {
        String[] userId = userTest.getUserId().split(",");
        List<UserExam> list = new ArrayList<UserExam>();
        Uuid uuid = new Uuid();

        /**
         *  -  发布后 添加学生试题
         *  -  因为可以重复更新题型，此处进行的操作为若不存在该题型的试卷则insert，存在则更新
         */
        try {
            for (int i = 0; i < userId.length; i++) {
                UserExam userTest2 = new UserExam();
                userTest2.setTestPaperOneId(userTest.getTestPaperOneId());
                userTest2.setUserId(userId[i]);
                List<UserExam> userTestList = new ArrayList<UserExam>();
                userTestList = userExamService.list(userTest2);
                if (userTestList.size() == 0) {
                    UserExam userTest1 = new UserExam();
                    userTest1.setTestPaperOneId(userTest.getTestPaperOneId());
                    userTest1.setUserId(userId[i]);
                    userTest1.setCreateBy(ShiroUtils.getLoginName());
                    userTest1.setSumbitState("0"); //0表示未提交,1为提交
                    userTest1.setcId(userTest.getcId());
                    SysUser sysUser = new SysUser();
                    sysUser = sysUserService.getById(ShiroUtils.getUserId());
                    String tid1 = sysUser.getUserAttrId();
                    userTest1.setCreateId(tid1);
                    list.add(userTest1);
                }
            }
            return toAjax(userExamService.saveBatch(list));
        } catch (Exception e) {
            System.out.println(e.getCause());
            return error("记录已存在");
        }
    }


    /**
     * 新增保存测试关联  期末考
     */
    @RequiresPermissions("teacher:course:view")
    @Log(title = "测试关联", actionType = ActionType.INSERT)
    @PostMapping("/addQ")
    @ResponseBody
    public Data addQ(UserExam userTest) {
        System.out.println("userTest:" + userTest);
        String[] userId = userTest.getUserId().split(",");
        List<UserExam> list = new ArrayList<UserExam>();
        Uuid uuid = new Uuid();
        try {
            for (int i = 0; i < userId.length; i++) {
                UserExam userTest2 = new UserExam();
                userTest2.setTestPaperOneId(userTest.getTestPaperOneId());
                userTest2.setUserId(userId[i]);
                List<UserExam> userTestList = new ArrayList<UserExam>();
                userTestList = userExamService.list(userTest2);
                if (userTestList.size() == 0) {
                    System.out.println("新纪录");
                    UserExam userTest1 = new UserExam();
                    userTest1.setTestPaperOneId(userTest.getTestPaperOneId());
                    userTest1.setUserId(userId[i]);
                    userTest1.setStuStartTime(userTest.getStuStartTime());
                    userTest1.setCreateBy(ShiroUtils.getLoginName());
                    userTest1.setStuEndTime(userTest.getStuEndTime());
                    userTest1.setSumbitState("0"); //0表示未提交,1为提交
                    userTest1.setcId(userTest.getcId());
                    SysUser sysUser = new SysUser();
                    sysUser = sysUserService.getById(ShiroUtils.getUserId());
                    String tid1 = sysUser.getUserAttrId();
                    userTest1.setCreateId(tid1);
                    list.add(userTest1);
                }
            }
            return toAjax(userExamService.saveBatch(list));
        } catch (Exception e) {
            return error(e.getMessage());
        }
    }


    /**
     * 删除测试关联
     */
    @RequiresPermissions("teacher:course:view")
    @Log(title = "测试关联", actionType = ActionType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public Data remove(String ids) throws Exception {
        String[] userId = ids.split(",");
        List<String> list = new ArrayList<String>();
        for (int i = 0; i < userId.length; i++) {
            list.add(userId[i]);
        }
        try {
            return toAjax(userExamService.removeByIds(list));
        } catch (Exception e) {
            System.out.println(e.getCause());
            return error(e.getMessage());
        }
    }


    /**
     * 获取年级
     */
    @PostMapping("/getStudents")
    @ResponseBody
    public List<UserExam> getStudents(String cId, String tid)
    /*UserTest userTest 以后要获取老师和老师绑定的课程*/ {
        //获取老师和老师绑定的课程来获取年级，此处是做测试使用
        UserExam userTest = new UserExam();
        String crid = getUser().getUserAttrId();
        userTest.setCreateId(crid);
        userTest.setTcoId(cId);
        return userExamService.getStudentInfo(crid, cId);

    }

    /**
     * 获取班级
     */
    @PostMapping("/getTcoName")
    @ResponseBody
    public List<UserExam> getTcoName(String cId, String tid) {
        UserExam userTest = new UserExam();
        String crid = getUser().getUserAttrId();
        userTest.setCreateId(crid);
        userTest.setTcoId(cId);
        List<UserExam> list = userExamService.getTcoName(crid, cId);
        System.out.println(list);
        return userExamService.getTcoName(crid, cId);

    }

    /**
     * 获取学生
     */
    @PostMapping("/getTcoStu")
    @ResponseBody
    public List<UserExam> getTcoStu(String cId, String tid) {
        UserExam userTest = new UserExam();
        String crid = getUser().getUserAttrId();
        userTest.setCreateId(crid);
        userTest.setTcoId(cId);
        return userExamService.getTcoStu(crid, cId);

    }

    /**
     * 查询期末试卷列表
     */
    @RequiresPermissions("teacher:course:view")
    @PostMapping("/testPaperlist/{cid}")
    @ResponseBody
    public TableDataInfo testPaperlist(@PathVariable String cid, TestPaperOne testPaper) {
        startPage();
        testPaper.setCoursrId(cid);
        testPaper.setCreateId(getUser().getUserAttrId());
        testPaper.setType("1");
        List<TestPaperOne> list = testPaperOneService.list(testPaper);
        return wrapTable(list);
    }

    /**
     * 查询期末试卷列表
     */
    @RequiresPermissions("teacher:course:view")
    @PostMapping("/testCoursePaperlist/{cid}")
    @ResponseBody
    public TableDataInfo testCoursePaperlist(@PathVariable String cid, TestPaperOne testPaper) {
        startPage();
        testPaper.setCoursrId(cid);
        testPaper.setCreateId(getUser().getUserAttrId());
        testPaper.setType("0");
        List<TestPaperOne> list = testPaperOneService.list(testPaper);
        return wrapTable(list);
    }
    /**
     * 设置提交的状态
     */
    @RequestMapping("/setState")
    @ResponseBody
    public Data setState(@RequestParam String paperId,
                         @RequestParam String studentId, @RequestParam String tutId)throws Exception {
        System.out.println("====================设置提交状态paperId:" +paperId+"--studentId--"+studentId+"--tutId--"+tutId);
        UserExam userTest1 = new UserExam();
            List<UserExam> userTestList;
            userTestList = userExamService.list(userTest1);
            userTest1.setId(tutId);
            userTest1.setSumbitState("1");
            userTest1.setUpdateBy(ShiroUtils.getLoginName());
            userTest1.setUserId(getUser().getUserAttrId());
            userTest1.setUpdateBy(ShiroUtils.getLoginName());
            return toAjax( userExamService.update(userTest1));
    }


}
