package cn.wstom.web.controller.school.onlineExam;

import cn.wstom.common.annotation.Log;
import cn.wstom.common.base.Data;
import cn.wstom.common.enums.ActionType;
import cn.wstom.main.web.base.BaseController;
import cn.wstom.main.web.page.TableDataInfo;
import cn.wstom.onlineexam.entity.TestpaperQuestions;
import cn.wstom.onlineexam.service.TestpaperQuestionsService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 试卷题目 信息操作处理
 *
 * @author hzh
 * @date 20190223
 */
@Controller
@RequestMapping("/module/testpaperQuestions")
public class TestpaperQuestionsController extends BaseController {
    private String prefix = "module/testpaperQuestions";

    @Autowired
    private TestpaperQuestionsService testpaperQuestionsService;

    @RequiresPermissions("module:testpaperQuestions:view")
    @GetMapping()
    public String toList() {
        return prefix + "list";
    }

    /**
     * 查询试卷题目列表
     */
    @RequiresPermissions("module:testpaperQuestions:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(TestpaperQuestions testpaperQuestions) {
        startPage();
        List<TestpaperQuestions> list = testpaperQuestionsService.list(testpaperQuestions);
        return wrapTable(list);
    }

    /**
     * 新增试卷题目
     */
    @GetMapping("/add")
    public String toAdd() {
        return prefix + "/add";
    }

    /**
     * 新增保存试卷题目
     */
    @RequiresPermissions("module:testpaperQuestions:add")
    @Log(title = "试卷题目", actionType = ActionType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public Data add(TestpaperQuestions testpaperQuestions) throws Exception {
        return toAjax(testpaperQuestionsService.save(testpaperQuestions));
    }

    /**
     * 修改试卷题目
     */
    @GetMapping("/edit/{id}")
    public String toEdit(@PathVariable("id") String id, ModelMap mmap) {
        TestpaperQuestions testpaperQuestions = testpaperQuestionsService.getById(id);
        mmap.put("testpaperQuestions", testpaperQuestions);
        return prefix + "/edit";
    }

    /**
     * 修改保存试卷题目
     */
    @RequiresPermissions("module:testpaperQuestions:edit")
    @Log(title = "试卷题目", actionType = ActionType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public Data edit(TestpaperQuestions testpaperQuestions) throws Exception {
        return toAjax(testpaperQuestionsService.update(testpaperQuestions));
    }

    /**
     * 删除试卷题目
     */
    @RequiresPermissions("module:testpaperQuestions:remove")
    @Log(title = "试卷题目", actionType = ActionType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public Data remove(String ids) throws Exception {
        return toAjax(testpaperQuestionsService.removeById(ids));
    }
}
