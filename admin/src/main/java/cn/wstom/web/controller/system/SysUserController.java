package cn.wstom.web.controller.system;

import cn.wstom.common.annotation.Log;
import cn.wstom.common.base.Data;
import cn.wstom.common.constant.UserConstants;
import cn.wstom.common.enums.ActionType;
import cn.wstom.common.support.Convert;
import cn.wstom.common.utils.SendEmailUtil;
import cn.wstom.main.config.CaptchaConfig;
import cn.wstom.main.util.ExcelUtil;
import cn.wstom.common.utils.StringUtil;
import cn.wstom.main.shiro.service.PasswordService;
import cn.wstom.main.util.ShiroUtils;
import cn.wstom.main.web.base.BaseController;
import cn.wstom.main.web.page.TableDataInfo;
import cn.wstom.system.entity.SysUser;
import cn.wstom.system.service.SysRoleService;
import cn.wstom.system.service.SysUserService;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * 用户信息
 *
 * @author dws
 */
@Controller
@RequestMapping("/system/user")
public class SysUserController extends BaseController {
    private String prefix = "system/user";

    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private SysRoleService roleService;

    @Autowired
    private PasswordService passwordService;

    /**
     * 返回用户列表页面
     *
     * @return
     */
    @RequiresPermissions("system:user:view")
    @GetMapping()
    public String toList() {
        return prefix + "/list";
    }

    /**
     * 获取用户列表信息
     *
     * @param sysUser
     * @return
     */
    @RequiresPermissions("system:sysUser:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SysUser sysUser) {
        startPage();
        return wrapTable(sysUserService.list(sysUser));
    }

    /**
     * 导出用户信息
     *
     * @param sysUser
     * @return
     */
    @Log(title = "用户导出", actionType = ActionType.EXPORT)
    @RequiresPermissions("system:sysUser:export")
    @PostMapping("/export")
    @ResponseBody
    public Data export(SysUser sysUser) {
        try {
            List<SysUser> list = sysUserService.list(sysUser);
            ExcelUtil<SysUser> util = new ExcelUtil<>(SysUser.class);
            return util.exportExcel(list, "sysUser");
        } catch (Exception e) {
            e.printStackTrace();
            return Data.error(e.getMessage());
        }
    }

    /**
     * 新增用户
     */
    @GetMapping("/add")
    public String add(ModelMap map) {
        map.put("roles", roleService.list(null));
        return prefix + "/add";
    }

    /**
     * 新增保存用户
     */
    @RequiresPermissions("system:sysUser:add")
    @Log(title = "用户管理", actionType = ActionType.INSERT)
    @PostMapping("/add")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public Data addSave(SysUser sysUser) throws Exception {
        if (StringUtil.isNotNull(sysUser.getId()) && UserConstants.ADMIN_ID.equals(sysUser.getId())) {
            return error("不允许修改超级管理员用户");
        }
        sysUser.setSalt(ShiroUtils.randomSalt());
        sysUser.setPassword(ShiroUtils.encryptPassword(sysUser.getLoginName(), sysUser.getPassword(), sysUser.getSalt()));
        sysUser.setCreateBy(getLoginName());
        return toAjax(sysUserService.save(sysUser));
    }

    /**
     * 修改用户
     */
    @GetMapping("/edit/{userId}")
    public String edit(@PathVariable("userId") String userId, ModelMap mmap) {
        mmap.put("user", sysUserService.getById(userId));
        mmap.put("roles", roleService.selectAllRolesByUserId(userId));
        return prefix + "/edit";
    }

    /**
     * 修改保存用户
     */
    @RequiresPermissions("system:sysUser:edit")
    @Log(title = "用户管理", actionType = ActionType.UPDATE)
    @PostMapping("/edit")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public Data editSave(SysUser sysUser) throws Exception {
        if (StringUtil.isNotNull(sysUser.getId()) && UserConstants.ADMIN_ID.equals(sysUser.getId())) {
            return error("不允许修改超级管理员用户");
        }
        sysUser.setUpdateBy(ShiroUtils.getLoginName());
        return toAjax(sysUserService.update(sysUser, true));
    }

    /**
     * 获取重置用户密码页面
     *
     * @param userId 重置用户的id
     * @param map    模型数据
     * @return 重置密码页面
     */
    @RequiresPermissions("system:user:resetPwd")
    @GetMapping("/resetPwd/{userId}")
    public String resetPwd(@PathVariable("userId") String userId, ModelMap map) {
        map.put("user", sysUserService.getById(userId));
        return prefix + "/resetPwd";
    }

    /**
     * 重置密码
     *
     * @param sysUser
     * @return
     */
    @RequiresPermissions("system:sysUser:resetPwd")
    @Log(title = "重置用户密码", actionType = ActionType.UPDATE)
    @PostMapping("/resetPwd")
    @ResponseBody
    public Data resetPwdSave(SysUser sysUser) throws Exception {
        SysUser user = new SysUser();
        user.setId(sysUser.getId());
        user.setSalt(ShiroUtils.randomSalt());
        user.setPassword(ShiroUtils.encryptPassword(sysUser.getLoginName(), sysUser.getPassword(), sysUser.getSalt()));
        return toAjax(sysUserService.update(user));
    }

    /**
     * 删除用户
     *
     * @param ids
     * @return
     */
    @RequiresPermissions("system:user:remove")
    @Log(title = "删除用户", actionType = ActionType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public Data remove(String ids) throws Exception {
        try {
            String[] userIds = Convert.toStrArray(ids);
            List<String> idList = Arrays.asList(userIds);
            if (idList.contains(UserConstants.ADMIN_ID)) {
                throw new Exception("不允许删除超级管理员用户");
            }
            return toAjax(sysUserService.removeByIds(idList));
        } catch (Exception e) {
            return error(e.getMessage());
        }
    }

    /**
     * 校验用户名
     */
    @PostMapping("/checkLoginNameUnique")
    @ResponseBody
    public boolean checkLoginNameUnique(SysUser sysUser) {
        Map<String, Object> parms = new HashMap<>(1);
        parms.put("loginName", sysUser.getLoginName());
        return sysUserService.count(parms) <= 0;
    }

    /**
     * 校验手机号码
     */
    @PostMapping("/checkPhoneUnique")
    @ResponseBody
    public String checkPhoneUnique(SysUser sysUser) {
        return sysUserService.checkPhoneUnique(sysUser);
    }

    /**
     * 校验email邮箱
     */
    @PostMapping("/checkEmailUnique")
    @ResponseBody
    public String checkEmailUnique(SysUser sysUser) {
        return sysUserService.checkEmailUnique(sysUser);
    }



}