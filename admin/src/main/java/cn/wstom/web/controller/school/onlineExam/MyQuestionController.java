package cn.wstom.web.controller.school.onlineExam;

import cn.wstom.common.annotation.Log;
import cn.wstom.common.base.Data;
import cn.wstom.common.enums.ActionType;
import cn.wstom.common.support.Convert;
import cn.wstom.jiaowu.entity.Chapter;
import cn.wstom.jiaowu.entity.Course;
import cn.wstom.jiaowu.service.ChapterService;
import cn.wstom.jiaowu.service.CourseService;
import cn.wstom.main.util.ExcelUtil;
import cn.wstom.main.util.ShiroUtils;
import cn.wstom.main.web.base.BaseController;
import cn.wstom.main.web.page.TableDataInfo;
import cn.wstom.onlineexam.entity.*;
import cn.wstom.onlineexam.service.*;
import cn.wstom.system.entity.SysUser;
import cn.wstom.system.service.SysUserService;
import cn.wstom.web.controller.school.onlineExam.common.util.MyQuestionVO;
import cn.wstom.web.controller.school.onlineExam.common.util.NumericToChinese;
import cn.wstom.web.controller.school.onlineExam.common.util.Uuid;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;


/**
 * 题库
 *
 * @author hzh
 */
@Controller
@RequestMapping("/school/onlineExam/myQuestion")
public class MyQuestionController extends BaseController {
    private String prefix = "school/onlineExam/myQuestion";

    @Autowired
    private MyQuestionsService myQuestionsService;
    @Autowired
    private PublicQuestionsService publicQuestionsService;
    @Autowired
    private PublicOptionAnswerService publicOptionAnswerService;
    @Autowired
    private TestpaperTestquestionsService paperQuestionsService;
    @Autowired
    private TestpaperOneTestquestionsService paperOneQuestionsService;
    @Autowired
    private MyOptionAnswerService myOptionAnswerService;
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private ChapterService chapterService;
    @Autowired
    private TitleTypeService titleTypeService;
    @Autowired
    private MyTitleTypeService myTitleTypeService;
    @Autowired
    private CourseService courseService;
    private MyOptionAnswer optionAnswer;
    /***
     * 登录ID
     */
    String loginid;
    /**
     * SysUser
     */
    SysUser sysUser;
    /**
     * 用户ID
     */
    String uid;

    /**
     * 获取列表页面
     *
     * @return
     */
    @RequiresPermissions("teacher:course:view")
    @GetMapping("/{cid}")
    public String toList(@PathVariable String cid, ModelMap modelMap) {
        modelMap.put("id", cid);
        String tid = sysUserService.getById(getUserId()).getUserAttrId();
        modelMap.put("tid", tid);
        System.out.println("Hello World");
        return prefix + "/list";
    }


    /**
     * 跳转添加页面
     *
     * @return
     */
    @RequiresPermissions("teacher:course:view")
    @GetMapping("/add/{cid}")
    public String toAdd(@PathVariable String cid, ModelMap modelMap) {
        modelMap.put("id", cid);
        return prefix + "/add";
    }

    @RequiresPermissions("teacher:course:view")
    @PostMapping("/add")
    @ResponseBody
    public Data addQuestion(MyQuestions myQuestions) throws Exception {
        loginid = ShiroUtils.getUserId();
        sysUser = sysUserService.getById(loginid);
        uid = sysUser.getUserAttrId();
        String[] optionanswersArr = null;
        String[] optionanswerArr = null;
        String trimstr = "option=";
        String myoaStr = "";
        String optionanswers = myQuestions.getMyoptionAnswerArr();
        List<MyOptionAnswer> myoalist = new ArrayList<MyOptionAnswer>();
        optionanswersArr = optionanswers.substring(0, optionanswers.length() - 1).split(";");//optionanswersArr按;进行分割，这里分割成4个
        boolean Include=true;
        List<MyTitleType> myTitleTypes = myTitleTypeService.findByCidAndTid(myQuestions.getXzsubjectsId(),uid);
        System.out.println("myTitleTypes"+myTitleTypes);
        try {
            System.out.println("myQuestions.getTitleTypeId()"+myQuestions.getTitleTypeId());
            if (myQuestions.getTitleTypeId().equals(myTitleTypes.get(0).getId()) || myQuestions.getTitleTypeId().equals(myTitleTypes.get(1).getId()) || myQuestions.getTitleTypeId().equals(myTitleTypes.get(2).getId())) {
                for (int i = 0; i < optionanswersArr.length; i++) {
                    String[] optionanswerArrtwo = null;
                    optionanswerArrtwo = optionanswersArr[i].substring(0, optionanswersArr[i].length() - 1).split(":");//按：进行分割
                    String optiontwo = "";
                    if (optionanswerArrtwo[0].contains(trimstr)) {
                        System.out.println(optionanswerArrtwo[0]);
                        optiontwo = optionanswerArrtwo[0].substring(7);// 去掉option=开头字符串
                    } else {
                        optiontwo = optionanswerArrtwo[0];
                    }
                    String answertwo = optionanswerArrtwo[1];
                    System.out.println("answertwo" + answertwo);
                    if (!answertwo.equals("0")) {
                        Include = false;
                        break;
                    }
                }
                System.out.println("ewie" + Include);
                if (Include) {
                    return Data.error("未选择正确答案，添加失败");
                }
            }
        }catch (Exception e){
            System.out.println("异常");
        }
        for (int i = 0; i < optionanswersArr.length; i++) {
            optionanswerArr = optionanswersArr[i].substring(0, optionanswersArr[i].length() - 1).split(":");//按：进行分割
            String optionone = "";
            if (optionanswerArr[0].contains(trimstr)) {
                System.out.println(optionanswerArr[0]);
                optionone = optionanswerArr[0].substring(7);// 去掉option=开头字符串
            } else {
                optionone = optionanswerArr[0];
            }
            String answerone = optionanswerArr[1];

            try {
                String optionstr = java.net.URLDecoder.decode(optionone, "UTF-8");
                optionAnswer = new MyOptionAnswer();
                Uuid answerId = new Uuid();
                optionAnswer.setCreateBy(ShiroUtils.getLoginName());
                optionAnswer.setCreateId(uid);
                optionAnswer.setId(answerId.UId());
                optionAnswer.setStoption(optionstr);
                optionAnswer.setStanswer(answerone);
                toAjax(myOptionAnswerService.save(optionAnswer));
                myoaStr += optionAnswer.getId() + ";";

            } catch (Exception e) {
                System.out.println("乱码解决错误：" + e);
            }
        }
        myQuestions.setCreateBy(ShiroUtils.getLoginName());
        myQuestions.setCreateId(uid);
        myQuestions.setQexposed("0");
        myQuestions.setQmaxexposure("10000");
        myQuestions.setMyoptionAnswerArr(myoaStr);
        System.out.println(myQuestions.getJchapterId());
        return toAjax(myQuestionsService.save(myQuestions));

    }

    /**
     * 编辑
     *
     * @param id
     * @param mmap
     * @return
     */
    @RequiresPermissions("teacher:course:view")
    @GetMapping("/edit/{id}")
    public String toEdit(@PathVariable("id") String id, ModelMap mmap) {
        System.out.println(myQuestionsService.getById(id).getTitle());
        mmap.put("myQuestions", myQuestionsService.getById(id));
        return prefix + "/edit";
    }


    /**
     * 题库页面
     *
     * @param myQuestions
     * @return
     */
    @RequiresPermissions("teacher:course:view")
    @PostMapping("/list/{cid}")
    @ResponseBody
    public TableDataInfo list(@PathVariable String cid, MyQuestions myQuestions) {
        myQuestions.setXzsubjectsId(cid);
        myQuestions.setCreateId(getUser().getUserAttrId());
        System.out.println("tid" + myQuestions.getTitleTypeId());
        startPage();
        List<MyQuestions> list = myQuestionsService.list(myQuestions);
        String easy = "容易";
        String easyNum = "1";
        String relativelyEasy = "较易";
        String relativelyEasyNum = "2";
        String secondary = "中等";
        String secondaryNum = "3";
        String moreDifficult = "较难";
        String moreDifficultNum = "4";
        String difficulty = "困难";
        String difficultyNum = "5";
        if (list.size() != 0) {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getDifficulty().equals(easyNum)) {
                    list.get(i).setDifficulty(easy);
                }
                if (list.get(i).getDifficulty().equals(relativelyEasyNum)) {
                    list.get(i).setDifficulty(relativelyEasy);
                }
                if (list.get(i).getDifficulty().equals(secondaryNum)) {
                    list.get(i).setDifficulty(secondary);
                }
                if (list.get(i).getDifficulty().equals(moreDifficultNum)) {
                    list.get(i).setDifficulty(moreDifficult);
                }
                if (list.get(i).getDifficulty().equals(difficultyNum)) {
                    list.get(i).setDifficulty(difficulty);
                }
            }
        }
        return wrapTable(list);
    }

    @RequiresPermissions("teacher:course:view")
    @Log(title = "删除试题", actionType = ActionType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public Data remove(String ids) throws Exception {
        try {
            List<String> idList = Arrays.asList(Convert.toStrArray(ids));
            for (int i = 0; i < idList.size(); i++) {
                String IdStr = myQuestionsService.getById(idList.get(i)).getMyoptionAnswerArr();
                if (IdStr != null) {
                    String[] answerIds = IdStr.split(";");
                    for (int j = 0; j < answerIds.length; j++) {
                        myOptionAnswerService.removeById(answerIds[j]);
                    }
                }
            }
            return toAjax(myQuestionsService.removeByIds(idList));
        } catch (Exception e) {
            return error(e.getMessage());
        }
    }


    @Log(title = "修改题目", actionType = ActionType.INSERT)
    @RequiresPermissions("teacher:course:view")
    @PostMapping("/update")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public Data addUpdate(MyQuestions myQuestions) throws Exception {

        String[] optionanswersArr;
        String[] optionanswerArr;
        String optionAnswerIDList = "";
        String trimstr = "option=";
        String myoaStr = "";
        String optionanswers = myQuestions.getMyoptionAnswerArrContent();
        List<MyOptionAnswer> myoalist = new ArrayList<MyOptionAnswer>();
        optionanswersArr = optionanswers.substring(0, optionanswers.length() - 1).split(";");//optionanswersArr按;进行分割，这里分割成4个
        boolean Include = true;
        List<MyTitleType> myTitleTypes = myTitleTypeService.findByCidAndTid(myQuestions.getXzsubjectsId(), uid);
        System.out.println("myTitleTypes" + myTitleTypes);
        try {
            System.out.println("myQuestions.getTitleTypeId()" + myQuestions.getTitleTypeId());
            if (myQuestions.getTitleTypeId().equals(myTitleTypes.get(0).getId()) || myQuestions.getTitleTypeId().equals(myTitleTypes.get(1).getId()) || myQuestions.getTitleTypeId().equals(myTitleTypes.get(2).getId())) {
                for (int i = 0; i < optionanswersArr.length; i++) {
                    String[] optionanswerArrtwo = null;
                    optionanswerArrtwo = optionanswersArr[i].substring(0, optionanswersArr[i].length() - 1).split(":");//按：进行分割
                    String optiontwo = "";
                    if (optionanswerArrtwo[0].contains(trimstr)) {
                        System.out.println(optionanswerArrtwo[0]);
                        optiontwo = optionanswerArrtwo[0].substring(7);// 去掉option=开头字符串
                    } else {
                        optiontwo = optionanswerArrtwo[0];
                    }
                    String answertwo = optionanswerArrtwo[1];
                    System.out.println("answertwo" + answertwo);
                    if (!answertwo.equals("0")) {
                        Include = false;
                        break;
                    }
                }
                System.out.println("ewie" + Include);
                if (Include) {
                    return Data.error("未选择正确答案，添加失败");
                }
            }
        } catch (Exception e) {
            System.out.println("异常");
        }
        myoaStr = myQuestions.getMyoptionAnswerArr();
        if (myoaStr != null && !myoaStr.equals("")) {
            String[] myoaArr = myoaStr.substring(0, myoaStr.length() - 1).split(";");
            for (int i = 0; i < myoaArr.length; i++) {
                if (myOptionAnswerService.getById(myoaArr[i]) == null) {
                } else {
                    myoalist.add(myOptionAnswerService.getById(myoaArr[i]));
                }
            }
        }

        if (optionanswers != null && !optionanswers.equals("")) {
            optionanswersArr = optionanswers.substring(0, optionanswers.length() - 1).split(";");
            for (int i = 0; i < optionanswersArr.length; i++) {
                optionanswerArr = optionanswersArr[i].substring(0,
                        optionanswersArr[i].length() - 1).split(":");
                String optionone = "";
                if (optionanswerArr[0].contains(trimstr)) {
                    optionone = optionanswerArr[0].substring(7);// 去掉option=开头字符串
                } else {
                    optionone = optionanswerArr[0];
                }
                try {
                    String optionstr = java.net.URLDecoder.decode(optionone,
                            "UTF-8");
                    String answerone = optionanswerArr[1];
                    if (myoalist.size() != 0 && !myoalist.isEmpty()) {
                        if (i < myoalist.size()) {
                            optionAnswer = myoalist.get(i);
                            optionAnswer.setUpdateBy(ShiroUtils.getLoginName());
                            optionAnswer.setUpdateId(getUser().getUserAttrId());
                            optionAnswer.setStoption(optionstr);
                            optionAnswer.setStanswer(answerone);
                            System.out.println("%%%%%%%%%%%%%%%%%");
                            myOptionAnswerService.update(optionAnswer);
                            System.out.println("*********************");
                        } else {
                            MyOptionAnswer optionAnswer = new MyOptionAnswer();
                            Uuid answerId = new Uuid();
                            optionAnswer.setCreateBy(ShiroUtils.getLoginName());
                            optionAnswer.setCreateId(uid);
                            optionAnswer.setId(answerId.UId());
                            optionAnswer.setStoption(optionstr);
                            optionAnswer.setStanswer(answerone);
                            myOptionAnswerService.save(optionAnswer);
                            myoaStr += optionAnswer.getId() + ";";
                        }
                    }
                } catch (Exception e) {
                    return error("更新出错:" + e.getCause() + ";" + e.getMessage());
                }
            }
        }
        myQuestions.setMyoptionAnswerArr(myoaStr);
        myQuestions.setCreateId(getUser().getUserAttrId());
        myQuestions.setCreateBy(ShiroUtils.getLoginName());
        return toAjax(myQuestionsService.update(myQuestions));

    }


    @Log(title = "新增题目", actionType = ActionType.INSERT)
    @RequiresPermissions("teacher:course:view")
    @PostMapping("/save")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public Data addSave(MyQuestions myQuestions) throws Exception {
        loginid = ShiroUtils.getUserId();
        sysUser = sysUserService.getById(loginid);
        uid = sysUser.getUserAttrId();
        String[] optionanswersArr = null;
        String[] optionanswerArr = null;
        String trimstr = "option=";
        String myoaStr = "";
        String optionanswers = myQuestions.getMyoptionAnswerArr();
        List<MyOptionAnswer> myoalist = new ArrayList<MyOptionAnswer>();
        optionanswersArr = optionanswers.substring(0, optionanswers.length() - 1).split(";");//optionanswersArr按;进行分割，这里分割成4个

        for (int i = 0; i < optionanswersArr.length; i++) {
            optionanswerArr = optionanswersArr[i].substring(0, optionanswersArr[i].length() - 1).split(":");//按：进行分割
            String optionone = "";
            if (optionanswerArr[0].contains(trimstr)) {
                System.out.println(optionanswerArr[0]);
                optionone = optionanswerArr[0].substring(7);// 去掉option=开头字符串
            } else {
                optionone = optionanswerArr[0];
            }
            String answerone = optionanswerArr[1];

            try {
                String optionstr = java.net.URLDecoder.decode(optionone, "UTF-8");
                optionAnswer = new MyOptionAnswer();
                Uuid answerId = new Uuid();
                optionAnswer.setCreateBy(ShiroUtils.getLoginName());
                optionAnswer.setCreateId(uid);
                optionAnswer.setId(answerId.UId());
                optionAnswer.setStoption(optionstr);
                optionAnswer.setStanswer(answerone);
                toAjax(myOptionAnswerService.save(optionAnswer));
                myoaStr += optionAnswer.getId() + ";";

            } catch (Exception e) {
                System.out.println("乱码解决错误：" + e);
            }
        }
        myQuestions.setCreateBy(ShiroUtils.getLoginName());
        myQuestions.setCreateId(getUser().getUserAttrId());
        myQuestions.setQexposed("0");
        myQuestions.setQmaxexposure("10000");
        myQuestions.setStstatus(1);
        myQuestions.setMyoptionAnswerArr(myoaStr);
        System.out.println(myQuestions.getTitleTypeId());
        return toAjax(myQuestionsService.save(myQuestions));

    }


    @RequiresPermissions("teacher:course:view")
    @Log(title = "分享到题库中心")
    @PostMapping("/toPublic")
    @ResponseBody
    public Data toPrivate(String ids) {
        System.out.println("ids:" + ids);
        String ansId;
        loginid = ShiroUtils.getUserId();
        sysUser = sysUserService.getById(loginid);
        uid = sysUser.getUserAttrId();
        MyQuestions myQuestions = new MyQuestions();
        PublicQuestions publicQuestions = new PublicQuestions();
        try {
            List<String> idList = Arrays.asList(Convert.toStrArray(ids));
            String ansString = "";
            for (int i = 0; i < idList.size(); i++) {
                String IdStr = myQuestionsService.getById(idList.get(i)).getMyoptionAnswerArr();
                myQuestions = myQuestionsService.getById(idList.get(i));
                publicQuestions.setCreateBy(ShiroUtils.getLoginName());
                publicQuestions.setCreateId(getUser().getUserAttrId());
                publicQuestions.setDifficulty(myQuestions.getDifficulty());
                publicQuestions.setParsing(myQuestions.getParsing());
                publicQuestions.setQexposed("0");
                publicQuestions.setQmaxexposure("10000");
                publicQuestions.setStstatus(myQuestions.getStstatus());
                publicQuestions.setXzsubjectsId(myQuestions.getXzsubjectsId());
                publicQuestions.setTitle(myQuestions.getTitle());
                publicQuestions.setYear(myQuestions.getYear());
                publicQuestions.setjChapterId(myQuestions.getJchapterId());
                publicQuestions.setTitleTypeId(myQuestions.getPubilcTitleId());
                if (IdStr != null) {
                    String[] answerIds = IdStr.split(";");
                    for (int j = 0; j < answerIds.length; j++) {
                        MyOptionAnswer myOptionAnswer = new MyOptionAnswer();
                        PublicOptionAnswer publicOptionAnswer = new PublicOptionAnswer();
                        myOptionAnswer = myOptionAnswerService.getById(answerIds[j]);
                        Uuid uuid1 = new Uuid();
                        ansId = uuid1.UId();
                        publicOptionAnswer.setId(ansId);
                        ansString += ansId + ";";
                        publicOptionAnswer.setCreateBy(ShiroUtils.getLoginName());
                        publicOptionAnswer.setCreateId(uid);
                        if (myOptionAnswer.getUpdateTime() != null) {
                            publicOptionAnswer.setUpdateTime(myOptionAnswer.getUpdateTime());
                        }
                        if (myOptionAnswer.getUpdateBy() != null) {
                            publicOptionAnswer.setUpdateBy(myOptionAnswer.getUpdateBy());
                        }
                        if (myOptionAnswer.getStanswer() != null) {
                            publicOptionAnswer.setStanswer(myOptionAnswer.getStanswer());

                        }
                        if (myOptionAnswer.getStoption() != null) {
                            publicOptionAnswer.setStoption(myOptionAnswer.getStoption());

                        }
                        publicOptionAnswerService.save(publicOptionAnswer);
                    }
                }
            }
            publicQuestions.setPublicoptionAnswerArr(ansString);
            return toAjax(publicQuestionsService.save(publicQuestions));
        } catch (Exception e) {
            System.out.println(e.getCause());
            return Data.error();
        }
    }

    /**
     * 章
     *
     * @param subId
     * @return
     */
    @ResponseBody
    @RequestMapping("/findchapterName1")
    @RequiresPermissions("teacher:course:view")
    public List<Chapter> findchapterName1(String subId) {
        System.out.println("搜索章。。。。。。");
        loginid = ShiroUtils.getUserId();
        sysUser = sysUserService.getById(loginid);
        uid = sysUser.getUserAttrId();
        List<Chapter> allSem = new ArrayList<Chapter>();
        Chapter chapter = new Chapter();
        chapter.setCid(subId);
        chapter.setPid("");
        allSem = chapterService.list(chapter);
        for (int i = 0; i < allSem.size(); i++) {
            System.out.println("^^" + allSem.get(i).getTitle());
        }
        return allSem;
    }

    /**
     * 节
     *
     * @param chaperId
     * @return
     */
    @ResponseBody
    @RequestMapping("/findchapterName2")
    @RequiresPermissions("teacher:course:view")
    public List<Chapter> findchapterName2(String chaperId) {
        List<Chapter> allSem = new ArrayList<Chapter>();
        if (chaperId.equals("0"))
            return allSem;
        Chapter chapter = new Chapter();
        chapter.setPid(chaperId);
        allSem = chapterService.list(chapter);
        return allSem;
    }

    /**
     * 查所有类型
     *
     * @param cid
     * @return
     */

    @RequestMapping("/findTitleType")
    @ResponseBody
    public List<MyTitleType> findTitleType(String cid) {
        List<MyTitleType> allSem = new ArrayList<MyTitleType>();
        MyTitleType myTitleType = new MyTitleType();
        myTitleType.setCreateId(getUser().getUserAttrId());
        myTitleType.setCid(cid);
        allSem = myTitleTypeService.list(myTitleType);
        System.out.println("*********" + allSem);
        return allSem;
    }

    /**
     * 获取题型
     *
     * @param questionIds
     * @param cid
     * @return
     */
    @ApiOperation("题目集映射题型集合")
    @RequiresPermissions("teacher:course:view")
    @PostMapping("/findTitleTypeByIds")
    @ResponseBody
    public List<Map<String, Object>> findTitleTypeByIds(@RequestBody List<String> questionIds, String cid) {
        List<Map<String, Object>> typeWithId = new ArrayList<>();

        Map<String, MyQuestions> titleMap = new HashMap<>();
        //  取出唯一titleId
        if (questionIds != null) {
            questionIds.forEach(id -> {
                MyQuestions index = myQuestionsService.getById(id);
                titleMap.put(index.getTitleTypeId(), index);
            });
        }

        titleMap.forEach((s, q) -> {
            Map<String, Object> map = new HashMap<>();
            map.put("sort", s);
            map.put("titleTypeId", q.getTitleTypeId());
            map.put("titleTypeName", q.getTitleTypeName());
            //  TODO: 获取缓存的分值

            typeWithId.add(map);
        });

        return typeWithId;
    }

    /**
     * 删除试卷题目关联
     *
     * @return
     */
    @ApiOperation("获取试卷题目")
    @RequiresPermissions("teacher:course:view")
    @RequestMapping(value = "/paperQuestion/list", method = RequestMethod.POST)
    @ResponseBody
    public TableDataInfo listPaperQuestion(String paperId) {
        startPage();//  对第一句sql进行分页
        List<TestpaperTestquestions> result = paperQuestionsService.getPaperQuestionsAndTestPaperInfoByPaperIdWithCreateId(paperId, sysUserService.getById(getUserId()).getUserAttrId());
        return wrapTable(result);
    }

    /**
     * 考试系统
     * @param paperId
     * @return
     */
    @ApiOperation("获取试卷题目")
    @RequiresPermissions("teacher:course:view")
    @RequestMapping(value = "/paperQuestion/list1", method = RequestMethod.POST)
    @ResponseBody
    public TableDataInfo listPaperOneQuestion(String paperId) {
        startPage();//  对第一句sql进行分页
        List<TestpaperOneTestquestions> result = paperOneQuestionsService.getPaperQuestionsAndTestPaperInfoByPaperIdWithCreateId(paperId, sysUserService.getById(getUserId()).getUserAttrId());
        return wrapTable(result);
    }
    /**
     * 删除考试试卷题目关联
     *
     * @return
     */
    @ApiOperation("删除试卷题目")
    @RequiresPermissions("teacher:course:view")
    @RequestMapping(value = "/paperQuestion/remove1", method = RequestMethod.POST)
    @ResponseBody
    public Data removePaperOneQuestion(String id) throws Exception {
        return toAjax(paperOneQuestionsService.removeById(id));
    }
    /**
     * 删除试卷题目关联
     *
     * @return
     */
    @ApiOperation("删除试卷题目")
    @RequiresPermissions("teacher:course:view")
    @RequestMapping(value = "/paperQuestion/remove", method = RequestMethod.POST)
    @ResponseBody
    public Data removePaperQuestion(String id) throws Exception {
        return toAjax(paperQuestionsService.removeById(id));
    }

    @RequestMapping("/stufindTitleType")
    @ResponseBody
    public List<MyTitleType> stufindTitleType(String cid, String tid) {
        List<MyTitleType> allSem = new ArrayList<MyTitleType>();
        MyTitleType myTitleType = new MyTitleType();
        myTitleType.setCreateId(tid);
        myTitleType.setCid(cid);
        allSem = myTitleTypeService.list(myTitleType);
        System.out.println("*********" + allSem);
        return allSem;
    }


    /**
     * 获取某一种题型的数量
     *
     * @param myQuestions
     * @return
     */
    @RequestMapping("/getQuestionSum")
    @RequiresPermissions("teacher:course:view")
    @ResponseBody
    public List<MyQuestions> getQuestionSum(MyQuestions myQuestions) {
        return myQuestionsService.list(myQuestions);
    }

    /*导入*/
    @Log(title = "题目管理", actionType = ActionType.IMPORT)
    @RequiresPermissions("teacher:course:view")
    @PostMapping("/importData")
    @ResponseBody
    public Data importData(MultipartFile file, boolean updateSupport, String tid, String cid) throws Exception {
        StringBuilder errorMsg = new StringBuilder();
        ExcelUtil<MyQuestionVO> util = new ExcelUtil<>(MyQuestionVO.class);
        List<MyQuestionVO> myQuestionVOList = util.importExcel(file.getInputStream());
        System.out.println("控制器输出" + myQuestionVOList.get(0).getMyTitleType());
        System.out.println("tid-----------" + tid);
        System.out.println("cid-----------" + cid);
        List<MyTitleType> mtys = myTitleTypeService.findByCidAndTid(cid, tid);
        System.out.println("mtys:"+mtys);
        String dataone = "";
        for (MyQuestionVO q : myQuestionVOList) {
            String titleTypeName = q.getMyTitleType();
            if (q.getDifficulty().trim().equals("")) {
                dataone = dataone + "题目：：" + q.getTitle() + "难度不能为空";
            }
            if (titleTypeName.trim().equals(mtys.get(0).getTitleTypeName().trim())) {/**为单选题**/

                if (q.getMyOptionA().equals("")||q.getMyOptionB().equals("")||q.getMyOptionC().equals("")||q.getMyOptionD().equals("")) {
                    dataone = "题目：：" + q.getTitle() + "。单项题选项不能为空###";
                }
                else if (q.getMyoptionAnswerArr().equals("")) {
                    dataone = dataone + "题目：：" + q.getTitle() + "。单项题至少要有一个答案###";
                }

            }
            if (titleTypeName.trim().equals(mtys.get(1).getTitleTypeName().trim())) {/**为判断题**/
                if (!q.getMyoptionAnswerArr().trim().equals("")) {

                    if (!q.getMyoptionAnswerArr().trim().equals("T") && !q.getMyoptionAnswerArr().trim().equals("F")) {
                        dataone = dataone + "题目：：" + q.getTitle() + "。判断题答案请输入 “T” or “F” ###";
                    }
                } else dataone = dataone + "题目：：" + q.getTitle() + "。判断题需要填写答案###";
            }
            if (titleTypeName.trim().equals(mtys.get(2).getTitleTypeName().trim())) {/**为多选题与单选题一样**/
                if (q.getMyOptionA().equals("")||q.getMyOptionB().equals("")||q.getMyOptionC().equals("")||q.getMyOptionD().equals("")) {
                    dataone = "题目：：" + q.getTitle() + "。多项题选项不能为空###";
                } else if (q.getMyoptionAnswerArr().equals("")) {
                    dataone = dataone + "题目：：" + q.getTitle() + "。多项题至少要有一个答案###";
                }
            }
            if (!dataone.equals("")) {
                throw new Exception(dataone);
            }
        }
        System.out.println("dataone:"+dataone);
        if (dataone.equals("")) {
            int i = 0;

            for (MyQuestionVO q : myQuestionVOList) {
                String titleTypeId = null;/*题目类型id*/
                String aid = "";/*选项id集*/
                String titleTypeName = q.getMyTitleType();
                if (titleTypeName.trim().equals(mtys.get(0).getTitleTypeName().trim())) {/**为单选题**/
                    titleTypeId = mtys.get(0).getId();
                    if (!q.getMyOptionA().equals("")) {
                        if (q.getMyoptionAnswerArr().trim().equals("A")) {/*判断选项A是否为答案*/
                            Uuid uuid = new Uuid();
                            System.out.println(uuid.UId());
                            MyOptionAnswer myOptionAnswer = new MyOptionAnswer();
                            myOptionAnswer.setId(uuid.UId());
                            myOptionAnswer.setStanswer("A");
                            myOptionAnswer.setStoption(q.getMyOptionA().toString());
                            myOptionAnswerService.save(myOptionAnswer);
                            aid = aid + myOptionAnswer.getId() + ";";
                        } else {
                            Uuid uuid = new Uuid();
                            MyOptionAnswer myOptionAnswer = new MyOptionAnswer();
                            myOptionAnswer.setId(uuid.UId());
                            myOptionAnswer.setStanswer("0");
                            myOptionAnswer.setStoption(q.getMyOptionA().toString());
                            myOptionAnswerService.save(myOptionAnswer);
                            aid += myOptionAnswer.getId() + ";";
                        }
                    }
                    if (!q.getMyOptionB().equals("")) {
                        if (q.getMyoptionAnswerArr().trim().equals("B")) {/*判断选项B是否为答案*/

                            Uuid uuid = new Uuid();
                            MyOptionAnswer myOptionAnswer = new MyOptionAnswer();
                            myOptionAnswer.setId(uuid.UId());
                            myOptionAnswer.setStanswer("B");
                            myOptionAnswer.setStoption(q.getMyOptionB().toString());
                            myOptionAnswerService.save(myOptionAnswer);
                            aid += myOptionAnswer.getId() + ";";
                        } else {
                            Uuid uuid = new Uuid();
                            MyOptionAnswer myOptionAnswer = new MyOptionAnswer();
                            myOptionAnswer.setId(uuid.UId());
                            myOptionAnswer.setStanswer("0");
                            myOptionAnswer.setStoption(q.getMyOptionB().toString());
                            myOptionAnswerService.save(myOptionAnswer);
                            aid += myOptionAnswer.getId() + ";";
                        }
                    }
                    if (!q.getMyOptionC().equals("")) {
                        if (q.getMyoptionAnswerArr().trim().equals("C")) {/*判断选项C是否为答案*/

                            Uuid uuid = new Uuid();
                            MyOptionAnswer myOptionAnswer = new MyOptionAnswer();
                            myOptionAnswer.setId(uuid.UId());
                            myOptionAnswer.setStanswer("C");
                            myOptionAnswer.setStoption(q.getMyOptionC().toString());
                            myOptionAnswerService.save(myOptionAnswer);
                            aid += myOptionAnswer.getId() + ";";
                        } else {
                            Uuid uuid = new Uuid();
                            MyOptionAnswer myOptionAnswer = new MyOptionAnswer();
                            myOptionAnswer.setId(uuid.UId());
                            myOptionAnswer.setStanswer("0");
                            myOptionAnswer.setStoption(q.getMyOptionC().toString());
                            myOptionAnswerService.save(myOptionAnswer);
                            aid += myOptionAnswer.getId() + ";";
                        }
                    }
                    if (!q.getMyOptionD().equals("")) {
                        if (q.getMyoptionAnswerArr().trim().equals("D")) {/*判断选项D是否为答案*/

                            Uuid uuid = new Uuid();
                            MyOptionAnswer myOptionAnswer = new MyOptionAnswer();
                            myOptionAnswer.setId(uuid.UId());
                            myOptionAnswer.setStanswer("D");
                            myOptionAnswer.setStoption(q.getMyOptionD().toString());
                            myOptionAnswerService.save(myOptionAnswer);
                            aid += myOptionAnswer.getId() + ";";
                        } else {
                            Uuid uuid = new Uuid();
                            MyOptionAnswer myOptionAnswer = new MyOptionAnswer();
                            myOptionAnswer.setId(uuid.UId());
                            myOptionAnswer.setStanswer("0");
                            myOptionAnswer.setStoption(q.getMyOptionD().toString());
                            myOptionAnswerService.save(myOptionAnswer);
                            aid += myOptionAnswer.getId() + ";";
                        }
                    }
                } else if (titleTypeName.trim().equals(mtys.get(1).getTitleTypeName().trim())) {/**为判断题**/
                    titleTypeId = mtys.get(1).getId();
                    if (!q.getMyoptionAnswerArr().trim().equals("")) {
                        if (q.getMyoptionAnswerArr().trim().equals("T")) {/*判断该判断题目答案是否为“正确”*/
                            Uuid uuid = new Uuid();
                            MyOptionAnswer myOptionAnswer = new MyOptionAnswer();
                            myOptionAnswer.setId(uuid.UId());
                            myOptionAnswer.setStanswer("T");
                            myOptionAnswer.setStoption("0");
                            myOptionAnswerService.save(myOptionAnswer);
                            aid += myOptionAnswer.getId() + ";";
                        } else if (q.getMyoptionAnswerArr().trim().equals("F")) {/*判断该判断题目答案是否为“错误”*/
                            Uuid uuid = new Uuid();
                            MyOptionAnswer myOptionAnswer = new MyOptionAnswer();
                            myOptionAnswer.setId(uuid.UId());
                            myOptionAnswer.setStanswer("F");
                            myOptionAnswer.setStoption("0");
                            myOptionAnswerService.save(myOptionAnswer);
                            aid += myOptionAnswer.getId() + ";";
                        }
                    }
                } else if (titleTypeName.trim().equals(mtys.get(2).getTitleTypeName().trim())) {/**为多选题与单选题一样**/
                    titleTypeId = mtys.get(2).getId();
                    String answer = q.getMyoptionAnswerArr().replace(",","");
                    System.out.println("answer"+answer);
                    if (!q.getMyOptionA().equals("")) {
                        if (answer.substring(0,1).equals("A")) {/*判断选项A是否为答案*/
                            Uuid uuid = new Uuid();
                            System.out.println(uuid.UId());
                            MyOptionAnswer myOptionAnswer = new MyOptionAnswer();
                            myOptionAnswer.setId(uuid.UId());
                            myOptionAnswer.setStanswer("A");
                            myOptionAnswer.setStoption(q.getMyOptionA().toString());
                            myOptionAnswerService.save(myOptionAnswer);
                            aid = aid + myOptionAnswer.getId() + ";";
                        } else if(answer.substring(1,2).equals("A")) {
                            Uuid uuid = new Uuid();
                            MyOptionAnswer myOptionAnswer = new MyOptionAnswer();
                            myOptionAnswer.setId(uuid.UId());
                            myOptionAnswer.setStanswer("A");
                            myOptionAnswer.setStoption(q.getMyOptionA().toString());
                            myOptionAnswerService.save(myOptionAnswer);
                            aid += myOptionAnswer.getId() + ";";
                        }
                        else {
                            Uuid uuid = new Uuid();
                            MyOptionAnswer myOptionAnswer = new MyOptionAnswer();
                            myOptionAnswer.setId(uuid.UId());
                            myOptionAnswer.setStanswer("0");
                            myOptionAnswer.setStoption(q.getMyOptionA().toString());
                            myOptionAnswerService.save(myOptionAnswer);
                            aid += myOptionAnswer.getId() + ";";
                        }
                    }
                    if (!q.getMyOptionB().equals("")) {
                        if (answer.substring(0,1).equals("B")) {/*判断选项B是否为答案*/

                            Uuid uuid = new Uuid();
                            MyOptionAnswer myOptionAnswer = new MyOptionAnswer();
                            myOptionAnswer.setId(uuid.UId());
                            myOptionAnswer.setStanswer("B");
                            myOptionAnswer.setStoption(q.getMyOptionB().toString());
                            myOptionAnswerService.save(myOptionAnswer);
                            aid += myOptionAnswer.getId() + ";";
                        }
                        else if(answer.substring(1,2).equals("B")) {
                            Uuid uuid = new Uuid();
                            MyOptionAnswer myOptionAnswer = new MyOptionAnswer();
                            myOptionAnswer.setId(uuid.UId());
                            myOptionAnswer.setStanswer("B");
                            myOptionAnswer.setStoption(q.getMyOptionB().toString());
                            myOptionAnswerService.save(myOptionAnswer);
                            aid += myOptionAnswer.getId() + ";";
                        }
                        else {
                            Uuid uuid = new Uuid();
                            MyOptionAnswer myOptionAnswer = new MyOptionAnswer();
                            myOptionAnswer.setId(uuid.UId());
                            myOptionAnswer.setStanswer("0");
                            myOptionAnswer.setStoption(q.getMyOptionB().toString());
                            myOptionAnswerService.save(myOptionAnswer);
                            aid += myOptionAnswer.getId() + ";";
                        }
                    }
                    if (!q.getMyOptionC().equals("")) {
                        if (answer.substring(0,1).equals("C")) {/*判断选项C是否为答案*/

                            Uuid uuid = new Uuid();
                            MyOptionAnswer myOptionAnswer = new MyOptionAnswer();
                            myOptionAnswer.setId(uuid.UId());
                            myOptionAnswer.setStanswer("C");
                            myOptionAnswer.setStoption(q.getMyOptionC().toString());
                            myOptionAnswerService.save(myOptionAnswer);
                            aid += myOptionAnswer.getId() + ";";
                        }else if(answer.substring(1,2).equals("C")) {
                            Uuid uuid = new Uuid();
                            MyOptionAnswer myOptionAnswer = new MyOptionAnswer();
                            myOptionAnswer.setId(uuid.UId());
                            myOptionAnswer.setStanswer("C");
                            myOptionAnswer.setStoption(q.getMyOptionC().toString());
                            myOptionAnswerService.save(myOptionAnswer);
                            aid += myOptionAnswer.getId() + ";";
                        }
                        else {
                            Uuid uuid = new Uuid();
                            MyOptionAnswer myOptionAnswer = new MyOptionAnswer();
                            myOptionAnswer.setId(uuid.UId());
                            myOptionAnswer.setStanswer("0");
                            myOptionAnswer.setStoption(q.getMyOptionC().toString());
                            myOptionAnswerService.save(myOptionAnswer);
                            aid += myOptionAnswer.getId() + ";";
                        }
                    }
                    if (!q.getMyOptionD().equals("")) {
                        if (answer.substring(0,1).equals("D")) {/*判断选项D是否为答案*/

                            Uuid uuid = new Uuid();
                            MyOptionAnswer myOptionAnswer = new MyOptionAnswer();
                            myOptionAnswer.setId(uuid.UId());
                            myOptionAnswer.setStanswer("D");
                            myOptionAnswer.setStoption(q.getMyOptionD().toString());
                            myOptionAnswerService.save(myOptionAnswer);
                            aid += myOptionAnswer.getId() + ";";
                        } else if(answer.substring(1,2).equals("D")) {
                            Uuid uuid = new Uuid();
                            MyOptionAnswer myOptionAnswer = new MyOptionAnswer();
                            myOptionAnswer.setId(uuid.UId());
                            myOptionAnswer.setStanswer("D");
                            myOptionAnswer.setStoption(q.getMyOptionD().toString());
                            myOptionAnswerService.save(myOptionAnswer);
                            aid += myOptionAnswer.getId() + ";";
                        }
                        else {
                            Uuid uuid = new Uuid();
                            MyOptionAnswer myOptionAnswer = new MyOptionAnswer();
                            myOptionAnswer.setId(uuid.UId());
                            myOptionAnswer.setStanswer("0");
                            myOptionAnswer.setStoption(q.getMyOptionD().toString());
                            myOptionAnswerService.save(myOptionAnswer);
                            aid += myOptionAnswer.getId() + ";";
                        }
                    }
                    if (!q.getMyOptionE().equals("")) {
                        if (q.getMyoptionAnswerArr().trim().equals("E")) {/*判断选项A是否为答案*/
                            Uuid uuid = new Uuid();
                            System.out.println(uuid.UId());
                            MyOptionAnswer myOptionAnswer = new MyOptionAnswer();
                            myOptionAnswer.setId(uuid.UId());
                            myOptionAnswer.setStanswer("E");
                            myOptionAnswer.setStoption(q.getMyOptionE().toString());
                            myOptionAnswerService.save(myOptionAnswer);
                            aid = aid + myOptionAnswer.getId() + ";";
                        } else {
                            Uuid uuid = new Uuid();
                            MyOptionAnswer myOptionAnswer = new MyOptionAnswer();
                            myOptionAnswer.setId(uuid.UId());
                            myOptionAnswer.setStanswer("0");
                            myOptionAnswer.setStoption(q.getMyOptionE().toString());
                            myOptionAnswerService.save(myOptionAnswer);
                            aid += myOptionAnswer.getId() + ";";
                        }
                    }
                    if (!q.getMyOptionF().equals("")) {
                        if (q.getMyoptionAnswerArr().trim().equals("F")) {/*判断选项A是否为答案*/
                            Uuid uuid = new Uuid();
                            System.out.println(uuid.UId());
                            MyOptionAnswer myOptionAnswer = new MyOptionAnswer();
                            myOptionAnswer.setId(uuid.UId());
                            myOptionAnswer.setStanswer("F");
                            myOptionAnswer.setStoption(q.getMyOptionF().toString());
                            myOptionAnswerService.save(myOptionAnswer);
                            aid = aid + myOptionAnswer.getId() + ";";
                        } else {
                            Uuid uuid = new Uuid();
                            MyOptionAnswer myOptionAnswer = new MyOptionAnswer();
                            myOptionAnswer.setId(uuid.UId());
                            myOptionAnswer.setStanswer("0");
                            myOptionAnswer.setStoption(q.getMyOptionF().toString());
                            myOptionAnswerService.save(myOptionAnswer);
                            aid += myOptionAnswer.getId() + ";";
                        }
                    }
                } else {
                    System.out.println("异常异常异常异常异常");
                }
                System.out.println("q.getJchapterId()"+q.getJchapterId());
                MyQuestions myQuestions = new MyQuestions();
                Calendar date = Calendar.getInstance();
                String year = String.valueOf(date.get(Calendar.YEAR));
                myQuestions.setYear(Integer.parseInt(year));
                myQuestions.setCreateBy(ShiroUtils.getLoginName());
                SysUser sysUser = sysUserService.getById(ShiroUtils.getUserId());
                Chapter chapter = new Chapter();
                chapter.setCid(cid);
                chapter.setCreateBy(ShiroUtils.getLoginName());
                List<Chapter> chapters = chapterService.selectList(chapter);
                List<Chapter> chapterList = new ArrayList<>();
                for (int j = 0; j <chapters.size(); j++) {
                    if (!chapters.get(j).getPid().equals("0")){
                        chapterList.add(chapters.get(j));
                    }
                }
                String sid = sysUser.getUserAttrId();
                myQuestions.setCreateId(tid);/*教师id*/
                myQuestions.setCreateBy(ShiroUtils.getLoginName());
                myQuestions.setTitleTypeId(titleTypeId);/*题型id*/
                myQuestions.setXzsubjectsId(cid);/*课程id*/
                myQuestions.setJchapterId(chapterList.get(0).getId());/*默认了一个章节*/
                myQuestions.setDifficulty(q.getDifficulty());/*题目难度*/
                myQuestions.setTitle(q.getTitle());/*题目正文*/
                myQuestions.setParsing(q.getParsing());/*题目解析*/
                myQuestions.setStstatus(1);
                myQuestions.setQexposed("0");
                myQuestions.setQmaxexposure("1000");
                myQuestions.setMyoptionAnswerArr(aid);
//                System.out.println(tid);
////                System.out.println(cid);
////                System.out.println(q.getDifficulty());
////                System.out.println(q.getMyQuestions().getTitle());
////                System.out.println(q.getParsing());
////                System.out.println(aid);
                myQuestionsService.save(myQuestions);
                i++;
                System.out.println("成功" + i + "个");
            }


            dataone = dataone + "共成功" + i + "条数据。";
        }
        return Data.success();
    }

    /**
     * 下载导入模板
     *
     * @return
     */
    @RequiresPermissions("teacher:course:view")
    @GetMapping("/importTemplate")
    @ResponseBody
    public Data importTemplate() {
        try {
            ExcelUtil<MyQuestionVO> util = new ExcelUtil<>(MyQuestionVO.class);
            return util.importTemplateExcel("myQuestions");
        } catch (Exception e) {
            e.printStackTrace();
            return Data.error(e.getMessage());
        }
    }

    private Map<String, MyTitleType> transMyTitleType(List<MyTitleType> myTitleTypeList) {
        Map<String, MyTitleType> myTitleTypeMap = new HashMap<>(myTitleTypeList.size());
        myTitleTypeList.forEach(d -> myTitleTypeMap.put(d.getTitleTypeName(), d));
        return myTitleTypeMap;
    }


    private Map<String, MyQuestions> transMyQuestions(List<MyQuestions> myQuestionsList) {
        Map<String, MyQuestions> myQuestionsMap = new HashMap<>(myQuestionsList.size());
        myQuestionsList.forEach(d -> myQuestionsMap.put(d.getTitle(), d));
        return myQuestionsMap;
    }

    private Map<String, MyOptionAnswer> transMyOptionAnswer(List<MyOptionAnswer> myOptionAnswerList) {
        Map<String, MyOptionAnswer> myOptionAnswerMap = new HashMap<>(myOptionAnswerList.size());
        myOptionAnswerList.forEach(d -> myOptionAnswerMap.put(d.getStoption(), d));
        return myOptionAnswerMap;
    }

    private Map<String, Course> transCourse(List<Course> courseList) {
        Map<String, Course> courseMap = new HashMap<>(courseList.size());
        courseList.forEach(d -> courseMap.put(d.getName(), d));
        return courseMap;
    }

    private Map<String, Chapter> transChapter(List<Chapter> chapterList) {
        Map<String, Chapter> chapterMap = new HashMap<>(chapterList.size());
        chapterList.forEach(d -> chapterMap.put(d.getName(), d));
        return chapterMap;
    }

}
