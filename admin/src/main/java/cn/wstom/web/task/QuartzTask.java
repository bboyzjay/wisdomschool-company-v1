package cn.wstom.web.task;

import cn.wstom.common.utils.AtomicIntegerUtil;
import cn.wstom.jiaowu.entity.Recourse;
import cn.wstom.jiaowu.service.RecourseService;
import cn.wstom.square.entity.Topic;
import cn.wstom.square.service.TopicService;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

//@Service
@Slf4j
public class QuartzTask {

    private final static Logger LOGGER = LogManager.getLogger(QuartzTask.class);

    @Autowired
    private TopicService topicService;

    @Autowired
    private RecourseService recourseService;

    /**
     *  - topic对象定时持久化到数据库
     *  - resource对象定时持久化到数据库
     */
    public void topicPersistence() {
        AtomicIntegerUtil.getIntegerMap().forEach((key, util) -> {
            if (key.contains(Topic.class.getSimpleName())) {
                String[] str = AtomicIntegerUtil.splitKey(key);
                Topic topic = (Topic) AtomicIntegerUtil.transformToEntity(str[0]);
                topic.setId(str[1]);
                topic.setBrowse((long) util.get());
                try {
                    topicService.update(topic);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (key.contains(Recourse.class.getSimpleName())) {
                String[] str = AtomicIntegerUtil.splitKey(key);
                Recourse recourse = (Recourse) AtomicIntegerUtil.transformToEntity(str[0]);
                recourse.setId(str[1]);
                recourse.setCount(util.get());
                try {
                    recourseService.update(recourse);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        log.info("【QuartzTask定时任务】 task=[{}, {}], date=[{}]", topicService.getClass(), recourseService.getClass(), new Date());
    }
}
