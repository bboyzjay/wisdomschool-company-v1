package cn.wstom.web.hook.controllerHook;

import cn.wstom.main.hook.interceptor.BaseInterceptorHookSupport;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * ArticleController拦截钩子
 * 事件处理由根据业务逻辑实现监听器实现
 *
 * @author dws 2018/10/30
 */
@Component
public class ArticleControllerHook extends BaseInterceptorHookSupport {

    @Override
    public void init() {
        this.plugins = getPlugins(ArticleControllerInterceptorListener.class);
    }

    @Override
    public String[] getInterceptor() {
        //说明要拦截的controller
        return new String[]{"cn.wstom.web.controller.front.ArticleController#detail"};
    }

    @Override
    public void preHandle(HttpServletRequest request, HttpServletResponse response, HandlerMethod handler) throws Exception {
        super.onPreHandle(request, response, handler);
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, HandlerMethod handler, ModelAndView modelAndView) throws Exception {
        super.onPostHandle(request, response, handler, modelAndView);

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, HandlerMethod handler, Exception ex) throws Exception {
        super.onAfterCompletion(request, response, handler, ex);
    }

    @Override
    public void afterConcurrentHandlingStarted(HttpServletRequest request, HttpServletResponse response, HandlerMethod handler) throws Exception {
        super.onAfterConcurrentHandlingStarted(request, response, handler);
    }

    public interface ArticleControllerInterceptorListener extends InterceptorListener {

    }
}
