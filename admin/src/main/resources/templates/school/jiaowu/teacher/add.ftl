<#include "/common/style.ftl"/>
<body class="white-bg">
<div class="container-div clearfix main-content-inner">
    <div class="wrapper wrapper-content animated fadeInRight ibox-content">
        <form class="form-horizontal m" id="form-teacher-add">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-4 control-label "><i class="text-danger">*</i> 职工号：</label>
                        <div class="col-sm-8">
                            <input class="form-control" type="text" name="loginName" id="loginName"/>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-4 control-label"><i class="text-danger">*</i> 教师姓名：</label>
                        <div class="col-sm-8">
                            <input class="form-control" type="text" name="userName" id="userName">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-4 control-label"><i class="text-danger">*</i> 所属院系：</label>
                        <div class="col-sm-8">
                            <@selectPage id='department' name='department.id' init="true" url=ctx+'/jiaowu/department/listpage' nextId='major'/>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-4 control-label"><i class="text-danger">*</i> 专业：</label>
                        <div class="col-sm-8">
                            <@selectPage id='major' name='major.id' init="false" url=ctx+'/jiaowu/major/listpage' nextId='clbum'/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-4 control-label "><i class="text-danger">*</i> 性别：</label>
                        <div class="col-sm-8">
                            <select id="sex" name="sex" class="form-control m-b">
                                <@dictionary dictType="sys_user_sex">
                                    <#list dictData as row>
                                        <option value="${row.dictValue}">${row.dictLabel}</option>
                                    </#list>
                                </@dictionary>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-4 control-label"><i class="text-danger">*</i> 密码：</label>
                        <div class="col-sm-8">
                            <input class="form-control" type="password" name="password" id="password">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-2 control-label"> 入职时间：</label>
                <div class="col-sm-10">
                    <div class="select-time">
                        <input type="text" class="time-input" id="onceTime" placeholder="入职时间"
                               name="teacher.workDate"/>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label "> 角色：</label>
                <div class="col-sm-10">
                    <div class="check-context" data-name="roleIds" data-label="roleName"
                         data-url="${ctx}/system/role/list"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label "> 选课：</label>
                <div class="col-sm-10">
                    <div class="check-context" data-name="courseIds" data-label="name"
                         data-url="${ctx}/jiaowu/course/listpage"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">备注：</label>
                <div class="col-sm-10">
                    <input id="remark" name="remark" class="form-control" type="text">
                </div>
            </div>
        </form>
    </div>
</div><!--/.container-div-->
<#include "/common/stretch.ftl"/>
<script type="text/javascript">
    $(function () {
        $.common.initBind();
    });
    let prefix = "${ctx}/jiaowu/teacher";

    $("#form-teacher-add").validate({
        rules: {
            "loginName": {
                required: true,
                remote: {
                    url: "${ctx}/system/user/checkLoginNameUnique",
                    type: "post",
                    dataType: "json",
                    data: {
                        "loginName": function () {
                            return $.common.trim($("#loginName").val());
                        }
                    },
                }
            },
            "userName": {
                required: true
            },
            "department.id": {
                required: true
            },
            "major.id": {
                required: true
            },
            "sex": {
                required: true
            },
            "password": {
                required: true
            }
        },
        messages: {
            "loginName": {
                remote: "该账号已存在"
            },
            "userName": {
                required: "请输入学生姓名"
            },
            "department.id": {
                required: "请选择系部"
            },
            "major.id": {
                required: "请选择专业"
            },
            "sex": {
                required: "请选择性别"
            },
            "password": {
                required: "请输入密码"
            }
        }
    });

    function submitHandler() {
        if ($.validate.form()) {
            add();
        }
    }

    function add() {
        $.operate.saveModal(prefix + "/add", $('#form-teacher-add').serialize());
    }

</script>
</body>

