<#macro adminLayout pageTitle title keywords description>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <meta charset="utf-8"/>
        <title>${pageTitle}</title>

        <meta name="description" content="${description}"/>
        <meta name="keywords" content="${keywords}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>

        <#include "/common/style.ftl"/>
        <style type="text/css">
            .type1 {
                position: fixed;
                left: 50%;
                top: 50%;
                width: 300px;
                height: 100px;
                margin: -100px 0px 0px -150px;
                border: 5px solid #888;
                background-color: rgba(255, 140, 0, 0.5);
                text-align: center;
            }

            .type2 {
                background-color: rgba(255, 140, 0, 0.1);
                border-style: groove;
                border-color: rgb(184, 134, 11)
            }
        </style>
    </head>

    <#--
        -   admin的管理页面
        -
        -
        -->

    <body class="no-skin gray-bg">

    <div id="navbar" class="navbar navbar-default ace-save-state navbar-fixed-top">
        <div class="navbar-container ace-save-state" id="navbar-container">
            <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
                <span class="sr-only">Toggle sidebar</span>

                <span class="icon-bar"></span>

                <span class="icon-bar"></span>

                <span class="icon-bar"></span>
            </button>

            <div class="navbar-header pull-left">
                <a href="/admin" class="navbar-brand">
                    <small>
                        <i class="fa fa-leaf"></i>
                        智慧教育云服务平台后台管理
                    </small>
                </a>
            </div>
            <@shiro.hasRole name="sds_admin">

            </@shiro.hasRole>
            <div class="navbar-buttons navbar-header pull-right" role="navigation">
                <ul class="nav ace-nav">
                    <@shiro.hasRole name="sds_admin">
                        <li class="purple dropdown-modal">
                            <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                                <i class="ace-icon fa fa-exchange"></i>
                                <small>切换角色</small>
                                <i class="ace-icon fa fa-caret-down"></i>
                                <ul class="sysUser-menu dropdown-menu-right dropdown-menu dropdown-caret dropdown-close">

                                    <li class="divider"></li>
                                    <li>
                                        <a>
                                            <i class="ace-icon glyphicon glyphicon-check"></i>
                                            ${nowrole}
                                        </a>
                                    </li>
                                    <#list roles as item>
                                        <li>
                                            <a href="javascript:SclickLoad('${item.roleKey}')">
                                                <i class="ace-icon glyphicon glyphicon-unchecked"></i>
                                                ${item.roleName}
                                            </a>
                                        </li>
                                    </#list>
                                </ul>
                            </a>
                        </li>
                    </@shiro.hasRole>
                    <li class="light-blue dropdown-modal">
                        <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                            <img class="nav-sysUser-photo" src="/img/avatars/user.jpg" alt="Jason's Photo"/>
                            <span class="sysUser-info">
									<small>欢迎您：</small>
									<@shiro.principal property="userName"/>
								</span>

                            <i class="ace-icon fa fa-caret-down"></i>
                        </a>

                        <ul class="sysUser-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">

                            <li class="divider"></li>
                            <li>
                                <a href="/logout">
                                    <i class="ace-icon fa fa-power-off"></i>
                                    登出
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div><!-- /.navbar-container -->
    </div>

    <div class="main-container ace-save-state" id="main-container">
        <script type="text/javascript">
            try{ace.settings.loadState('main-container')}catch(e){}
        </script>


        <#--    过滤其他用户 -->
        <@shiro.hasRole name="admin">
        <#--    <@shiro.lacksRole name="teacher">-->
            <div id="sidebar" class="sidebar responsive ace-save-state sidebar-fixed sidebar-scroll">
                <script type="text/javascript">
                    try{ace.settings.loadState('sidebar')}catch(e){}
                </script>
                <div class="sidebar-shortcuts" id="sidebar-shortcuts">
                    <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
                        <button class="btn btn-success">
                            <i class="ace-icon fa fa-signal"></i>
                        </button>

                        <button class="btn btn-info">
                            <i class="ace-icon fa fa-pencil"></i>
                        </button>

                        <button class="btn btn-warning">
                            <i class="ace-icon fa fa-users"></i>
                        </button>

                        <button class="btn btn-danger">
                            <i class="ace-icon fa fa-cogs"></i>
                        </button>
                    </div>

                    <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
                        <span class="btn btn-success"></span>

                        <span class="btn btn-info"></span>

                        <span class="btn btn-warning"></span>

                        <span class="btn btn-danger"></span>
                    </div>
                </div><!-- /.sidebar-shortcuts -->

                <ul class="nav nav-list main-nav-list">

                    <#list menus as row>
                        <li class="">
                            <a href="${row.url}" class="dropdown-toggle">
                                <i class="menu-icon ${row.icon}"></i>

                                ${row.menuName}
                                <b class="arrow fa fa-angle-down"></b>
                            </a>

                            <b class="arrow"></b>

                            <#if row.children??>
                                <ul class="submenu">
                                    <#list row.children as c>
                                        <li class="">
                                            <a href="${c.url}">
                                                <i class="menu-icon fa fa-caret-right ${c.icon}"></i>
                                                ${c.menuName}
                                            </a>
                                            <#if c.children??>
                                                <ul class="submenu">
                                                    <#list c.children as cc>
                                                        <li class="">
                                                            <a href="${cc.url}">
                                                                <i class="menu-icon fa fa-caret-right ${cc.icon}"></i>
                                                                ${cc.menuName}
                                                            </a>

                                                            <b class="arrow"></b>
                                                        </li>
                                                    </#list>
                                                </ul>
                                            </#if>
                                            <b class="arrow"></b>
                                        </li>
                                    </#list>
                                </ul>
                            </#if>
                        </li>
                    </#list>
                    <#--<@shiro.hasRole name="teacher">
                        <li class="active">
                            <a href="redirect:/admin">
                                <i class="menu-icon fa fa-tachometer"></i>
                                <span class="menu-text"> 我的课程 </span>
                            </a>

                            <b class="arrow"></b>
                        </li>
                    </@shiro.hasRole>-->
                </ul><!-- /.nav-list -->

                <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
                    <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state"
                       data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
                </div>
            </div>
        </@shiro.hasRole>
        <#--    </@shiro.lacksRole>-->

        <div class="main-content">
            <div class="main-content-inner">
                <div class="breadcrumbs ace-save-state breadcrumbs-fixed" id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="ace-icon fa fa-home home-icon"></i>
                            <a href="/admin">首页</a>
                        </li>
                        <li class="active "></li>

                    </ul><!-- /.breadcrumb -->

                    <#--<div class="nav-search" id="nav-search">-->
                    <#--<form class="form-search">-->
                    <#--<span class="input-icon">-->
                    <#--<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input"-->
                    <#--autocomplete="off"/>-->
                    <#--<i class="ace-icon fa fa-search nav-search-icon"></i>-->
                    <#--</span>-->
                    <#--</form>-->
                    <#--</div><!-- /.nav-search &ndash;&gt;-->
                </div>
                <#nested />
            </div>
        </div><!-- /.main-content -->

        <#include "/common/footer.ftl">
    </div><!-- /.main-container -->

    <!-- basic scripts -->



    </body>

    </html>
</#macro>

<#--<#macro pager url p spans>-->
<#--    <#if p??>-->
<#--        <#local span = (spans - 3)/2 />-->
<#--        <#if (url?index_of("?") != -1)>-->
<#--            <#local cURL = (url + "&pn=") />-->
<#--        <#else>-->
<#--            <#local cURL = (url + "?pn=") />-->
<#--        </#if>-->

<#--        <ul class="pagination">-->
<#--            <#assign pageNo = p.number + 1/>-->
<#--            <#assign pageCount = p.totalPages />-->
<#--            <#if (pageNo > 1)>-->
<#--                <li><a href="${cURL}${pageNo - 1}" pageNo="${pageNo - 1}" class="prev">上一页</a></li>-->
<#--            <#else>-->
<#--                <li class="disabled"><span>上一页</span></li>-->
<#--            </#if>-->

<#--            <#local totalNo = span * 2 + 3 />-->
<#--            <#local totalNo1 = totalNo - 1 />-->
<#--            <#if (pageCount > totalNo)>-->
<#--                <#if (pageNo <= span + 2)>-->
<#--                    <#list 1..totalNo1 as i>-->
<#--                        <@pagelink pageNo, i, cURL/>-->
<#--                    </#list>-->
<#--                    <@pagelink 0, 0, "#"/>-->
<#--                    <@pagelink pageNo, pageCount, cURL />-->
<#--                <#elseif (pageNo > (pageCount - (span + 2)))>-->
<#--                    <@pagelink pageNo, 1, cURL />-->
<#--                    <@pagelink 0, 0, "#"/>-->
<#--                    <#local num = pageCount - totalNo + 2 />-->
<#--                    <#list num..pageCount as i>-->
<#--                        <@pagelink pageNo, i, cURL/>-->
<#--                    </#list>-->
<#--                <#else>-->
<#--                    <@pagelink pageNo, 1, cURL />-->
<#--                    <@pagelink 0 0 "#" />-->
<#--                    <#local num = pageNo - span />-->
<#--                    <#local num2 = pageNo + span />-->
<#--                    <#list num..num2 as i>-->
<#--                        <@pagelink pageNo, i, cURL />-->
<#--                    </#list>-->
<#--                    <@pagelink 0, 0, "#"/>-->
<#--                    <@pagelink pageNo, pageCount, cURL />-->
<#--                </#if>-->
<#--            <#elseif (pageCount > 1)>-->
<#--                <#list 1..pageCount as i>-->
<#--                    <@pagelink pageNo, i, cURL />-->
<#--                </#list>-->
<#--            <#else>-->
<#--                <@pagelink 1, 1, cURL/>-->
<#--            </#if>-->

<#--            <#if (pageNo < pageCount)>-->
<#--                <li><a href="${cURL}${pageNo + 1}" pageNo="${pageNo + 1}" class="next">下一页</a></li>-->
<#--            <#else>-->
<#--                <li class="disabled"><span>下一页</span></li>-->
<#--            </#if>-->
<#--        </ul>-->
<#--    </#if>-->
<#--</#macro>-->
