<link rel="stylesheet" href="/favicon.ico">

<!-- bootstrap & fontawesome -->
<link rel="stylesheet" href="/css/bootstrap.min.css" />
<link rel="stylesheet" href="/js/font-awesome/4.5.0/css/font-awesome.min.css" />

<!-- page specific plugin styles -->
<link rel="stylesheet" type="text/css" href="/css/animate.min.css">
<link rel="stylesheet" type="text/css" href="/js/plugins/bootstrap-treetable/bootstrap-treetable.css">
<link rel="stylesheet" type="text/css" href="/css/style.css">

<!-- text fonts -->
<link rel="stylesheet" href="/js/ace/css/fonts.googleapis.com.css" />

<!-- ace styles -->
<link rel="stylesheet" href="/js/ace/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

<!--[if lte IE 9]>
<link rel="stylesheet" href="/js/ace/css/ace-part2.min.css" class="ace-main-stylesheet" />
<![endif]-->

<!--[if lte IE 9]>
<link rel="stylesheet" href="/js/ace/css/ace-ie.min.css" />
<![endif]-->

<!-- inline styles related to this page -->
<link rel="stylesheet" href="/js/plugins/iCheck/custom.css">
<link rel="stylesheet" href="/js/plugins/bootstrap-table/bootstrap-table.min.css">
<link rel="stylesheet" href="/js/plugins/jquery-ztree/3.5/css/metro/zTreeStyle.css"/>
<link rel="stylesheet" href="/js/plugins/select/select2.css"/>
<link rel="stylesheet" href="/js/plugins/bootstrap-fileinput/css/fileinput.css"/>

<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

<!--[if lte IE 8]>
<script src="/js/ace/js/html5shiv.min.js"></script>
<script src="/js/ace/js/respond.min.js"></script>
<script src="/js/ace/js/jquery-ui.custom.min.js"></script>
<script src="/js/ace/js/jquery.ui.touch-punch.min.js"></script>
<script src="/js/bootbox.min.js"></script>
<script src="/js/ace/js/jquery.easypiechart.min.js"></script>
<script src="/js/ace/js/jquery.gritter.min.js"></script>
<script src="/js/ace/js/spin.min.js"></script>
<![endif]-->
<script>
    let ctx = "${ctx}/";
</script>
<#include "/common/components/btn.ftl"/>
<#include "/common/components/select.ftl"/>
<#include "/common/components/pagination.ftl"/>