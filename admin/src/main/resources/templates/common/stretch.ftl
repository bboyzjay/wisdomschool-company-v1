<!--[if !IE]> -->
<script src="/js/jquery.min.js"></script>

<!-- <![endif]-->

<!--[if IE]>
<script src="/js/jquery-1.11.3.min.js"></script>
<![endif]-->
<script type="text/javascript">
    if ('ontouchstart' in document.documentElement) document.write("<script src='js/ace/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
</script>
<script src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/bootbox.min.js"></script>
<!-- page specific plugin scripts -->

<!--[if lte IE 8]>
<script src="/js/excanvas.min.js"></script>
<![endif]-->

<!-- ace scripts -->
<script type="text/javascript" src="/js/ace/js/ace-elements.min.js"></script>
<script type="text/javascript" src="/js/ace/js/ace.min.js"></script>
<script type="text/javascript" src="/js/plugins/bootstrap-treetable/bootstrap-treetable.js"></script>
<!-- bootstrap-table 表格插件 -->
<script src="/js/plugins/bootstrap-table/bootstrap-table.min.js"></script>
<script src="/js/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>
<script src="/js/plugins/jquery-ztree/3.5/js/jquery.ztree.all-3.5.js"></script>
<script src="/js/plugins/bootstrap-table/extensions/mobile/bootstrap-table-mobile.min.js"></script>
<script src="/js/plugins/bootstrap-table/extensions/toolbar/bootstrap-table-toolbar.min.js"></script>

<script type="text/javascript" src="/js/plugins/layer/layer.js"></script>
<script type="text/javascript" src="/js/plugins/layui/layui.js"></script>
<script type="text/javascript" src="/js/plugins/blockUI/jquery.blockUI.min.js"></script>
<script type="text/javascript" src="/js/plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript" src="/js/plugins/jquery-validation/jquery.validate.min.js"></script>
<script type="text/javascript" src="/js/plugins/jquery-validation/messages_zh.min.js"></script>
<script type="text/javascript" src="/js/plugins/jquery-validation/jquery.validate.extend.js"></script>
<script type="text/javascript" src="/js/plugins/select/select2.js"></script>
<script type="text/javascript" src="/js/plugins/bootstrap-fileinput/js/fileinput.js"></script>
<script type="text/javascript" src="/js/plugins/bootstrap-fileinput/js/locales/zh.js"></script>
<script type="text/javascript" src="/js/common.js"></script>
<script type="text/javascript" src="/js/admin/index.js"></script>
<script type="text/javascript" src="/js/ace/js/ace.min.js"></script>
<script src="/js/page.js"></script>

<@shiro.hasRole name="teacher">
    <script type="text/javascript">


        function TinitPagerEvent() {

            //属性 用于加载分页数据
            let TcontentItem = [{
                key: 'course.id',
                replace: 'redirect:/teacher/course/{%}',
                attr: 'href'
            }, {
                key: 'thumbnailPath',
                replace: '${storage}/showCondensedPicture?fileId={%}',
                attr: 'src'
            }, {
                key: 'course.name'
            }];
            let TpagerOptions = {
                loadURL: '${ctx}/teacher/course/list',
                item: TcontentItem
            };
            let page = $.fn.startPage(TpagerOptions);
        }

        function TclickLoad(e) {
            let that = e.currentTarget;
            let url = $.common.trim(that.href);
            if (url !== '#' && url !== '' && url !== undefined) {
                $.common.loadPage('main-page-container', url);
            }
            e.preventDefault();
        }

        $(document).ready(function () {
            TinitPagerEvent();
        })
        function SclickLoad(item) {
            let wp = window;
            wp.location.href='/admin/'+item;
        }

    </script>
</@shiro.hasRole>




