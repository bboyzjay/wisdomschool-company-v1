package cn.wstom.main.util;

import cn.wstom.common.utils.StringUtil;
import cn.wstom.common.utils.bean.BeanUtils;
import cn.wstom.main.shiro.realm.Realm;
import cn.wstom.system.entity.SysUser;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.mgt.RealmSecurityManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.apache.shiro.subject.Subject;

/**
 * shiro 工具类
 *
 * @author dws
 */
public class ShiroUtils {

    /**
     * 获取当前账户信息
     * @return
     */
    public static Subject getSubjct() {
        return SecurityUtils.getSubject();
    }

    /**
     * 获取会话
     * @return
     */
    public static Session getSession() {
        return SecurityUtils.getSubject().getSession();
    }

    /**
     * 退出登陆
     */
    public static void logout() {
        getSubjct().logout();
    }

    /**
     * 获取当前用户
     * @return
     */
    public static SysUser getUser() {
        SysUser sysUser = null;
        Object obj = getSubjct().getPrincipal();
        if (StringUtil.isNotNull(obj)) {
            sysUser = new SysUser();
            BeanUtils.copyBeanProp(sysUser, obj);
        }
        return sysUser;
    }

    /**
     * 设置当前用户
     * @param sysUser
     */
    public static void setUser(SysUser sysUser) {
        Subject subject = getSubjct();
        PrincipalCollection principalCollection = subject.getPrincipals();
        String realmName = principalCollection.getRealmNames().iterator().next();
        PrincipalCollection newPrincipalCollection = new SimplePrincipalCollection(sysUser, realmName);
        // 重新加载Principal
        subject.runAs(newPrincipalCollection);
    }

    /**
     * 清空权限缓存
     */
    public static void clearCachedAuthorizationInfo() {
        RealmSecurityManager rsm = (RealmSecurityManager) SecurityUtils.getSecurityManager();
        Realm realm = (Realm) rsm.getRealms().iterator().next();
        realm.clearCachedAuthorizationInfo();
    }

    public static boolean checkAuthorization(String permitted) {
        return SecurityUtils.getSubject().isPermitted(permitted);
    }

    /**
     * 获取用户id
     * @return
     */
    public static String getUserId() {
        return getUser().getId();
    }

    /**
     * 获取登陆账号
     * @return
     */
    public static String getLoginName() {
        return getUser().getLoginName();
    }

    /**
     * 获取登陆ip
     * @return
     */
    public static String getIp() {
        return getSubjct().getSession().getHost();
    }

    /**
     * 获取会话id
     * @return
     */
    public static String getSessionId() {
        return String.valueOf(getSubjct().getSession().getId());
    }

    /**
     * 生成随机盐
     */
    public static String randomSalt() {
        // 一个Byte占两个字节，此处生成的3字节，字符串长度为6
        SecureRandomNumberGenerator secureRandom = new SecureRandomNumberGenerator();
        String hex = secureRandom.nextBytes(3).toHex();
        return hex;
    }

    /**
     * 加密密码
     *
     * @param username
     * @param password
     * @param salt
     * @return
     */
    public static String encryptPassword(String username, String password, String salt) {
        return new Md5Hash(username + password + salt).toHex();
    }
}
