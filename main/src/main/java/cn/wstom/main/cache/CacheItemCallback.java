package cn.wstom.main.cache;

/**
 * @author dws
 * @date 2019/01/30
 */
public interface CacheItemCallback<T> {

    /**
     * 获取缓存项
     *
     * @return 缓存项
     */
    T getItem();
}
