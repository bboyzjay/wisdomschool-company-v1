package cn.wstom.main.shiro.web.filter;

import cn.wstom.common.constant.Constants;
import cn.wstom.common.utils.StringUtil;
import cn.wstom.main.async.AsyncManager;
import cn.wstom.main.async.factory.AsyncFactory;
import cn.wstom.main.util.MessageUtils;
import cn.wstom.main.util.ShiroUtils;
import cn.wstom.system.entity.SysUser;
import org.apache.shiro.session.SessionException;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * 退出过滤器
 *
 * @author dws
 */
public class LogoutFilter extends org.apache.shiro.web.filter.authc.LogoutFilter {
    private static final Logger log = LoggerFactory.getLogger(LogoutFilter.class);

    /**
     * 退出后重定向的地址
     */
    private String loginUrl;

    public String getLoginUrl() {
        return loginUrl;
    }

    public void setLoginUrl(String loginUrl) {
        this.loginUrl = loginUrl;
    }

    @Override
    protected boolean preHandle(ServletRequest request, ServletResponse response) throws Exception {
        try {
            Subject subject = getSubject(request, response);
            String redirectUrl = getRedirectUrl(request, response, subject);
            try {
                SysUser sysUser = ShiroUtils.getUser();
                if (StringUtil.isNotNull(sysUser)) {
                    String loginName = sysUser.getLoginName();
                    // 记录用户退出日志
                    AsyncManager.async().execute(AsyncFactory.recordLoginInfo(loginName, Constants.LOGOUT, MessageUtils.message("sysUser.logout.success")));
                }
                // 退出登录
                subject.logout();
            } catch (SessionException ise) {
                log.error("logout fail.", ise);
            }
            issueRedirect(request, response, redirectUrl);
        } catch (Exception e) {
            log.error("Encountered session exception during logout.  This can generally safely be ignored.", e);
        }
        return false;
    }

    /**
     * 退出跳转URL
     */
    @Override
    protected String getRedirectUrl(ServletRequest request, ServletResponse response, Subject subject) {
        String url = getLoginUrl();
        if (StringUtil.isNotEmpty(url)) {
            return url;
        }
        return super.getRedirectUrl(request, response, subject);
    }
}
