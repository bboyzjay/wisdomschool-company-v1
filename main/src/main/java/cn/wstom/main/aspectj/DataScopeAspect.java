package cn.wstom.main.aspectj;

import cn.wstom.common.annotation.DataScope;
import cn.wstom.common.base.entity.BaseEntity;
import cn.wstom.common.constant.UserConstants;
import cn.wstom.common.utils.StringUtil;
import cn.wstom.main.util.ShiroUtils;
import cn.wstom.system.entity.SysRole;
import cn.wstom.system.entity.SysUser;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * 数据过滤处理
 *
 * @author dws
 */
@Aspect
@Component
public class DataScopeAspect {
    /**
     * 全部数据权限
     */
    public static final String DATA_SCOPE_ALL = "1";

    /**
     * 自定数据权限
     */
    public static final String DATA_SCOPE_CUSTOM = "2";

    /**
     * 数据权限过滤关键字
     */
    public static final String DATA_SCOPE = "dataScope";

    /**
     * 数据范围过滤
     *
     * @param alias 部门表别名
     * @return 标准连接条件对象
     */
    public static void dataScopeFilter(JoinPoint joinPoint, SysUser sysUser, String alias) {
        StringBuilder sqlString = new StringBuilder();

        for (SysRole role : sysUser.getRoles()) {
            String dataScope = role.getDataScope();
            if (DATA_SCOPE_ALL.equals(dataScope)) {
                sqlString = new StringBuilder();
                break;
            } else if (DATA_SCOPE_CUSTOM.equals(dataScope)) {
                sqlString.append(StringUtil.format(
                        " OR %1$s.dept_id IN ( SELECT dept_id FROM sys_role_dept WHERE role_id = %2$s ) ", alias,
                        role.getId()));
            }
        }

        if (StringUtil.isNotBlank(sqlString.toString())) {
            BaseEntity baseEntity = (BaseEntity) joinPoint.getArgs()[0];
//            baseEntity.getParams().put(DATA_SCOPE, " AND (" + sqlString.substring(4) + ")");
        }
    }

    /**
     * 配置织入点
     */
    @Pointcut("@annotation(cn.wstom.common.annotation.DataScope)")
    public void dataScopePointCut() {
    }

    @Before("dataScopePointCut()")
    public void doBefore(JoinPoint point) throws Throwable {
        handleDataScope(point);
    }

    protected void handleDataScope(final JoinPoint joinPoint) {
        // 获得注解
        DataScope controllerDataScope = getAnnotationLog(joinPoint);
        if (controllerDataScope == null) {
            return;
        }
        // 获取当前的用户
        SysUser currentSysUser = ShiroUtils.getUser();
        if (currentSysUser != null) {
            // 如果是超级管理员，则不过滤数据
            if (UserConstants.ADMIN_ID.equals(currentSysUser.getId())) {
                //dataScopeFilter(joinPoint, currentSysUser, controllerDataScope.tableAlias());
            }
        }
    }

    /**
     * 是否存在注解，如果存在就获取
     */
    private DataScope getAnnotationLog(JoinPoint joinPoint) {
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();

        if (method != null) {
            return method.getAnnotation(DataScope.class);
        }
        return null;
    }
}
