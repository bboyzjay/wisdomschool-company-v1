-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: localhost    Database: widdoms
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `qrtz_blob_triggers`
--

DROP TABLE IF EXISTS `qrtz_blob_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qrtz_blob_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `blob_data` blob,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qrtz_calendars`
--

DROP TABLE IF EXISTS `qrtz_calendars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qrtz_calendars` (
  `sched_name` varchar(120) NOT NULL,
  `calendar_name` varchar(200) NOT NULL,
  `calendar` blob NOT NULL,
  PRIMARY KEY (`sched_name`,`calendar_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qrtz_cron_triggers`
--

DROP TABLE IF EXISTS `qrtz_cron_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qrtz_cron_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `cron_expression` varchar(200) NOT NULL,
  `time_zone_id` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qrtz_fired_triggers`
--

DROP TABLE IF EXISTS `qrtz_fired_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qrtz_fired_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `entry_id` varchar(95) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `instance_name` varchar(200) NOT NULL,
  `fired_time` bigint(13) NOT NULL,
  `sched_time` bigint(13) NOT NULL,
  `priority` int(11) NOT NULL,
  `state` varchar(16) NOT NULL,
  `job_name` varchar(200) DEFAULT NULL,
  `job_group` varchar(200) DEFAULT NULL,
  `is_nonconcurrent` varchar(1) DEFAULT NULL,
  `requests_recovery` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`sched_name`,`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qrtz_job_details`
--

DROP TABLE IF EXISTS `qrtz_job_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qrtz_job_details` (
  `sched_name` varchar(120) NOT NULL,
  `job_name` varchar(200) NOT NULL,
  `job_group` varchar(200) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `job_class_name` varchar(250) NOT NULL,
  `is_durable` varchar(1) NOT NULL,
  `is_nonconcurrent` varchar(1) NOT NULL,
  `is_update_data` varchar(1) NOT NULL,
  `requests_recovery` varchar(1) NOT NULL,
  `job_data` blob,
  PRIMARY KEY (`sched_name`,`job_name`,`job_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qrtz_locks`
--

DROP TABLE IF EXISTS `qrtz_locks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qrtz_locks` (
  `sched_name` varchar(120) NOT NULL,
  `lock_name` varchar(40) NOT NULL,
  PRIMARY KEY (`sched_name`,`lock_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qrtz_paused_trigger_grps`
--

DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qrtz_paused_trigger_grps` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  PRIMARY KEY (`sched_name`,`trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qrtz_scheduler_state`
--

DROP TABLE IF EXISTS `qrtz_scheduler_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qrtz_scheduler_state` (
  `sched_name` varchar(120) NOT NULL,
  `instance_name` varchar(200) NOT NULL,
  `last_checkin_time` bigint(13) NOT NULL,
  `checkin_interval` bigint(13) NOT NULL,
  PRIMARY KEY (`sched_name`,`instance_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qrtz_simple_triggers`
--

DROP TABLE IF EXISTS `qrtz_simple_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qrtz_simple_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `repeat_count` bigint(7) NOT NULL,
  `repeat_interval` bigint(12) NOT NULL,
  `times_triggered` bigint(10) NOT NULL,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qrtz_simprop_triggers`
--

DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qrtz_simprop_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `str_prop_1` varchar(512) DEFAULT NULL,
  `str_prop_2` varchar(512) DEFAULT NULL,
  `str_prop_3` varchar(512) DEFAULT NULL,
  `int_prop_1` int(11) DEFAULT NULL,
  `int_prop_2` int(11) DEFAULT NULL,
  `long_prop_1` bigint(20) DEFAULT NULL,
  `long_prop_2` bigint(20) DEFAULT NULL,
  `dec_prop_1` decimal(13,4) DEFAULT NULL,
  `dec_prop_2` decimal(13,4) DEFAULT NULL,
  `bool_prop_1` varchar(1) DEFAULT NULL,
  `bool_prop_2` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qrtz_triggers`
--

DROP TABLE IF EXISTS `qrtz_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qrtz_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `job_name` varchar(200) NOT NULL,
  `job_group` varchar(200) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `next_fire_time` bigint(13) DEFAULT NULL,
  `prev_fire_time` bigint(13) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `trigger_state` varchar(16) NOT NULL,
  `trigger_type` varchar(8) NOT NULL,
  `start_time` bigint(13) NOT NULL,
  `end_time` bigint(13) DEFAULT NULL,
  `calendar_name` varchar(200) DEFAULT NULL,
  `misfire_instr` smallint(2) DEFAULT NULL,
  `job_data` blob,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  KEY `sched_name` (`sched_name`,`job_name`,`job_group`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `qrtz_job_details` (`sched_name`, `job_name`, `job_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_config`
--

DROP TABLE IF EXISTS `sys_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_config` (
  `id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(100) DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  `version` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 COMMENT='参数配置表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_dict_data`
--

DROP TABLE IF EXISTS `sys_dict_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_dict_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) DEFAULT '0' COMMENT '字典排序',
  `dict_label` varchar(100) DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) DEFAULT '' COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) DEFAULT '' COMMENT '表格回显样式',
  `is_default` char(1) DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 COMMENT='字典数据表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_dict_type`
--

DROP TABLE IF EXISTS `sys_dict_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_dict_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `dict_type` (`dict_type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COMMENT='字典类型表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_job`
--

DROP TABLE IF EXISTS `sys_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_job` (
  `job_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL DEFAULT '' COMMENT '任务组名',
  `method_name` varchar(500) DEFAULT '' COMMENT '任务方法',
  `method_params` varchar(200) DEFAULT '' COMMENT '方法参数',
  `cron_expression` varchar(255) DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) DEFAULT '1' COMMENT '计划执行错误策略（1继续 2等待 3放弃）',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`,`job_name`,`job_group`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 COMMENT='定时任务调度表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_job_log`
--

DROP TABLE IF EXISTS `sys_job_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_job_log` (
  `job_log_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL COMMENT '任务组名',
  `method_name` varchar(500) DEFAULT NULL COMMENT '任务方法',
  `method_params` varchar(200) DEFAULT '' COMMENT '方法参数',
  `job_message` varchar(500) DEFAULT NULL COMMENT '日志信息',
  `status` char(1) DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` text COMMENT '异常信息',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2378 DEFAULT CHARSET=utf8 COMMENT='定时任务调度日志表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_logininfor`
--

DROP TABLE IF EXISTS `sys_logininfor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_logininfor` (
  `info_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `login_name` varchar(50) DEFAULT '' COMMENT '登录账号',
  `ipaddr` varchar(50) DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) DEFAULT '' COMMENT '操作系统',
  `status` char(10) DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) DEFAULT '' COMMENT '提示消息',
  `login_time` datetime DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8803 DEFAULT CHARSET=utf8 COMMENT='系统访问记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_menu`
--

DROP TABLE IF EXISTS `sys_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_menu` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) NOT NULL COMMENT '菜单名称',
  `parent_id` int(11) DEFAULT '0' COMMENT '父菜单ID',
  `order_num` int(4) DEFAULT '0' COMMENT '显示顺序',
  `url` varchar(200) DEFAULT '#' COMMENT '请求地址',
  `menu_type` char(1) DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `perms` varchar(100) DEFAULT '' COMMENT '权限标识',
  `icon` varchar(100) DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2145 DEFAULT CHARSET=utf8 COMMENT='菜单权限表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_notice`
--

DROP TABLE IF EXISTS `sys_notice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_notice` (
  `id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) NOT NULL COMMENT '公告标题',
  `notice_type` char(2) NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` varchar(500) NOT NULL COMMENT '公告内容',
  `status` char(1) DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='通知公告表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_oper_log`
--

DROP TABLE IF EXISTS `sys_oper_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_oper_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) DEFAULT '0' COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) DEFAULT '' COMMENT '方法名称',
  `operator_type` int(1) DEFAULT '0' COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) DEFAULT '' COMMENT '操作人员',
  `oper_url` varchar(255) DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(30) DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(255) DEFAULT '' COMMENT '请求参数',
  `status` int(1) DEFAULT '0' COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=342 DEFAULT CHARSET=utf8 COMMENT='操作日志记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_role`
--

DROP TABLE IF EXISTS `sys_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_role` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限）',
  `status` char(1) NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=142 DEFAULT CHARSET=utf8 COMMENT='角色信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_role_menu`
--

DROP TABLE IF EXISTS `sys_role_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_role_menu` (
  `role_id` int(11) unsigned NOT NULL COMMENT '角色ID',
  `menu_id` int(11) unsigned NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`,`menu_id`),
  KEY `fk_rm_menu` (`menu_id`) USING BTREE,
  CONSTRAINT `sys_role_menu_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `sys_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sys_role_menu_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `sys_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色和菜单关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_user`
--

DROP TABLE IF EXISTS `sys_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `login_name` varchar(30) NOT NULL COMMENT '登录账号',
  `user_name` varchar(30) NOT NULL COMMENT '用户昵称',
  `email` varchar(50) DEFAULT '' COMMENT '用户邮箱',
  `phone_number` varchar(11) DEFAULT '' COMMENT '手机号码',
  `sex` char(1) DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) DEFAULT NULL COMMENT '头像路径',
  `password` varchar(50) DEFAULT '' COMMENT '密码',
  `salt` varchar(20) DEFAULT '' COMMENT '盐加密',
  `status` char(1) DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(20) DEFAULT '' COMMENT '最后登陆IP',
  `login_date` datetime DEFAULT NULL COMMENT '最后登陆时间',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  `user_type` int(6) DEFAULT NULL COMMENT '用户属性类型 \r\n0：学生用户 1：教师用户 2：教务员用户',
  `user_attr_id` int(11) DEFAULT NULL COMMENT '用户属性id',
  `version` int(8) DEFAULT NULL COMMENT '乐观锁',
  `posts` int(11) DEFAULT '0' COMMENT '文章数',
  `signature` varchar(150) DEFAULT NULL COMMENT '个性签名',
  `answer` int(11) DEFAULT NULL COMMENT '问答数',
  `topic` int(11) DEFAULT NULL COMMENT '话题数',
  `question` int(11) DEFAULT NULL COMMENT '问题数',
  `question_follow` int(11) DEFAULT NULL COMMENT '关注问题数',
  `share` int(11) DEFAULT NULL COMMENT '分享数',
  `comment` int(11) DEFAULT NULL COMMENT '评论数',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ind_sys_user_id` (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=595 DEFAULT CHARSET=utf8 COMMENT='用户信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_user_online`
--

DROP TABLE IF EXISTS `sys_user_online`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user_online` (
  `id` varchar(50) NOT NULL DEFAULT '' COMMENT '用户会话id',
  `login_name` varchar(50) DEFAULT '' COMMENT '登录账号',
  `ip` varchar(50) DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) DEFAULT '' COMMENT '操作系统',
  `status` varchar(10) DEFAULT '' COMMENT '在线状态on_line在线off_line离线',
  `start_timestamp` datetime DEFAULT NULL COMMENT 'session创建时间',
  `last_access_time` datetime DEFAULT NULL COMMENT 'session最后访问时间',
  `expire_time` int(5) DEFAULT '0' COMMENT '超时时间，单位为分钟',
  `create_by` varchar(50) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_by` varchar(50) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `version` int(5) DEFAULT NULL,
  `avatar` varchar(100) DEFAULT NULL COMMENT '头像',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='在线用户记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_user_role`
--

DROP TABLE IF EXISTS `sys_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int(11) unsigned NOT NULL COMMENT '用户ID',
  `role_id` int(11) unsigned NOT NULL COMMENT '角色ID',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  KEY `fk_ur_user` (`user_id`) USING BTREE,
  KEY `fk_ur_role` (`role_id`) USING BTREE,
  CONSTRAINT `sys_user_role_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `sys_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sys_user_role_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `sys_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=634 DEFAULT CHARSET=utf8 COMMENT='用户和角色关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_answer`
--

DROP TABLE IF EXISTS `tb_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_answer` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `parent_id` int(11) unsigned DEFAULT '0' COMMENT '评论父级id，针对答案评论的内容',
  `question_id` int(11) unsigned NOT NULL COMMENT '回答问题的ID',
  `user_id` int(11) unsigned NOT NULL COMMENT '用户id',
  `content` text NOT NULL COMMENT '评论内容',
  `status` int(2) DEFAULT '0',
  `weight` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='回答表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_answer_count`
--

DROP TABLE IF EXISTS `tb_answer_count`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_answer_count` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `answer_id` int(11) unsigned NOT NULL COMMENT '答案id',
  `count_dig` int(11) DEFAULT '0' COMMENT '赞同数量',
  `count_bury` int(11) DEFAULT '0' COMMENT '踩数量',
  `count_comment` int(11) DEFAULT '0' COMMENT '评论数量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='回答统计表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_answer_votes`
--

DROP TABLE IF EXISTS `tb_answer_votes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_answer_votes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `user_id` int(11) unsigned NOT NULL,
  `answer_id` int(11) unsigned NOT NULL,
  `dig` int(11) DEFAULT '0' COMMENT '0未顶，1为顶',
  `bury` int(11) DEFAULT '0' COMMENT '0未踩，1为踩',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='答案记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_article`
--

DROP TABLE IF EXISTS `tb_article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_article` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `user_id` int(11) unsigned DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL COMMENT '标题',
  `content` longtext COMMENT '内容',
  `keywords` varchar(255) DEFAULT NULL COMMENT '关键词',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `style` tinyint(1) DEFAULT '0' COMMENT '标题字体 0正常 1粗体,2斜体',
  `color` varchar(7) DEFAULT NULL COMMENT '标题颜色',
  `recommend` int(5) DEFAULT '0' COMMENT '推荐设置，数字越大越靠前',
  `status` tinyint(2) DEFAULT '0' COMMENT '0正常状态 1删除',
  PRIMARY KEY (`id`),
  KEY `index_article` (`id`,`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8 COMMENT='文章表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_article_category`
--

DROP TABLE IF EXISTS `tb_article_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_article_category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `parent_id` int(11) unsigned DEFAULT '0' COMMENT '上级类目ID，顶级栏目为0',
  `name` varchar(50) DEFAULT NULL COMMENT '栏目名称',
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `recomment` tinyint(2) DEFAULT '0',
  `status` tinyint(2) DEFAULT '1' COMMENT '状态，1禁用，0启用',
  `sort` int(10) DEFAULT '0' COMMENT '排序，越大越靠前',
  PRIMARY KEY (`id`),
  KEY `followid` (`parent_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8 COMMENT='文章类别表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_article_category_merge`
--

DROP TABLE IF EXISTS `tb_article_category_merge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_article_category_merge` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `article_id` int(11) unsigned NOT NULL COMMENT '文章id',
  `category_id` varchar(255) NOT NULL COMMENT '包括父级、子级分类id',
  `type_id` int(11) unsigned DEFAULT '0' COMMENT '最终分类',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COMMENT='文章类表表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_article_comment`
--

DROP TABLE IF EXISTS `tb_article_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_article_comment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `article_id` int(11) unsigned DEFAULT NULL,
  `parent_id` int(11) unsigned DEFAULT '0' COMMENT '评论归属id，默认为0，',
  `user_id` int(11) unsigned DEFAULT NULL,
  `content` text,
  `status` tinyint(2) DEFAULT '0' COMMENT '0未审核 1正常状态 2审核未通过 3删除',
  `count_dig` int(11) DEFAULT '0' COMMENT '顶统计',
  `count_bury` int(11) DEFAULT '0' COMMENT '踩统计',
  `count_comment` int(11) DEFAULT '0' COMMENT '评论统计',
  `reply_id` int(11) unsigned DEFAULT '0' COMMENT ' 评论回复id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8 COMMENT='文章评论表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_article_count`
--

DROP TABLE IF EXISTS `tb_article_count`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_article_count` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `article_id` int(11) unsigned NOT NULL COMMENT '答案id',
  `count_dig` int(11) DEFAULT '0' COMMENT '赞同数量',
  `count_bury` int(11) DEFAULT '0' COMMENT '踩数量',
  `count_comment` int(11) DEFAULT '0' COMMENT '评论数量',
  `count_view` int(11) DEFAULT '0' COMMENT '访问次数',
  `weight` double(11,2) DEFAULT '0.00' COMMENT '文章权重',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COMMENT='文章统计表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_article_votes`
--

DROP TABLE IF EXISTS `tb_article_votes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_article_votes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `info_type` tinyint(2) NOT NULL,
  `info_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '文章id',
  `user_id` int(11) unsigned NOT NULL,
  `comment_id` int(11) unsigned DEFAULT '0' COMMENT '评论id',
  `dig` int(11) DEFAULT '0' COMMENT '0未顶，1为顶',
  `bury` int(11) DEFAULT '0' COMMENT '0未踩，1为踩',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='顶和踩记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_channel`
--

DROP TABLE IF EXISTS `tb_channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_channel` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '0' COMMENT '乐观锁',
  `key` varchar(32) DEFAULT NULL COMMENT '关键标识',
  `name` varchar(32) DEFAULT NULL COMMENT '栏目名',
  `status` int(3) NOT NULL COMMENT '状态，0：正常，1：禁用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='栏目表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_chapter`
--

DROP TABLE IF EXISTS `tb_chapter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_chapter` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `title` varchar(100) NOT NULL COMMENT '标题',
  `name` varchar(100) NOT NULL COMMENT '名称',
  `pid` int(11) unsigned DEFAULT NULL COMMENT '父id',
  `cid` int(11) unsigned NOT NULL COMMENT '课程id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8 COMMENT='课程章节表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_chapter_resource`
--

DROP TABLE IF EXISTS `tb_chapter_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_chapter_resource` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `rid` int(11) unsigned NOT NULL COMMENT '资源id',
  `resource_type` int(3) NOT NULL COMMENT '资源类型：1、视频，2、课件，3、试题',
  `cid` int(11) unsigned NOT NULL COMMENT '章节id',
  `order` int(5) NOT NULL DEFAULT '0' COMMENT '排序',
  `name` varchar(50) DEFAULT NULL COMMENT '资源名称',
  `ext` varchar(20) DEFAULT NULL COMMENT '资源后缀名',
  `tc_id` int(11) unsigned DEFAULT NULL COMMENT '教师课程id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8 COMMENT='章节资源表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_clbum`
--

DROP TABLE IF EXISTS `tb_clbum`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_clbum` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '0' COMMENT '乐观锁',
  `name` varchar(40) NOT NULL COMMENT '班级名称',
  `mid` int(11) unsigned NOT NULL COMMENT '专业id',
  PRIMARY KEY (`id`),
  KEY `fk_clbum_major` (`mid`) USING BTREE,
  CONSTRAINT `tb_clbum_ibfk_1` FOREIGN KEY (`mid`) REFERENCES `tb_major` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COMMENT='班级表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_clbum_course`
--

DROP TABLE IF EXISTS `tb_clbum_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_clbum_course` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `tcid` int(11) unsigned NOT NULL COMMENT '教师课程id',
  `cid` int(11) unsigned NOT NULL COMMENT '班级id',
  `gid` int(11) unsigned NOT NULL COMMENT '年级id',
  PRIMARY KEY (`id`),
  KEY `fk_cc_clbum` (`cid`) USING BTREE,
  KEY `fk_cc_course` (`tcid`) USING BTREE,
  KEY `fk_cc_grades` (`gid`) USING BTREE,
  CONSTRAINT `tb_clbum_course_ibfk_1` FOREIGN KEY (`cid`) REFERENCES `tb_clbum` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `tb_clbum_course_ibfk_2` FOREIGN KEY (`tcid`) REFERENCES `tb_teacher_course` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tb_clbum_course_ibfk_3` FOREIGN KEY (`gid`) REFERENCES `tb_grades` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12455 DEFAULT CHARSET=utf8 COMMENT='班级课程';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_comment`
--

DROP TABLE IF EXISTS `tb_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_comment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `type_id` int(11) unsigned DEFAULT NULL,
  `parent_id` int(11) unsigned DEFAULT '0' COMMENT '评论归属id，默认为0，',
  `user_id` int(11) unsigned DEFAULT NULL,
  `content` text,
  `status` tinyint(2) DEFAULT '0' COMMENT '0未审核 1正常状态 2审核未通过 3删除',
  `count_dig` int(11) DEFAULT '0' COMMENT '顶统计',
  `count_bury` int(11) DEFAULT '0' COMMENT '踩统计',
  `count_comment` int(11) DEFAULT '0' COMMENT '评论统计',
  `reply_id` int(11) unsigned DEFAULT '0' COMMENT ' 评论回复id',
  `type` int(4) DEFAULT NULL,
  `reply_user_id` int(11) DEFAULT NULL COMMENT '回复用户id，即直接父级评论id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=utf8 COMMENT='评论表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_course`
--

DROP TABLE IF EXISTS `tb_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_course` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '0' COMMENT '乐观锁',
  `name` varchar(32) NOT NULL COMMENT '课程名称',
  `period` varchar(3) NOT NULL COMMENT '学时',
  `credit` decimal(4,0) NOT NULL COMMENT '学分',
  `course_type` varchar(32) DEFAULT NULL COMMENT '课程类型',
  `course_brief_introduction` varchar(100) DEFAULT NULL COMMENT '课程简介',
  `course_info` longtext COMMENT '课程信息',
  `course_num` int(11) DEFAULT NULL COMMENT '课程容量',
  `course_status` varchar(32) DEFAULT NULL COMMENT '课程现状',
  `course_time` date DEFAULT NULL COMMENT '开课时间',
  `thumbnail_path` varchar(255) DEFAULT NULL COMMENT '预览图',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='注释';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_course_comment`
--

DROP TABLE IF EXISTS `tb_course_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_course_comment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '0' COMMENT '乐观锁',
  `uid` int(11) unsigned DEFAULT NULL COMMENT '用户id',
  `content` varchar(255) DEFAULT NULL COMMENT '评论内容',
  `pid` int(11) unsigned NOT NULL COMMENT '父id',
  `tc_id` int(11) DEFAULT NULL COMMENT '教师课程id',
  `cid` int(11) DEFAULT NULL COMMENT '章节id',
  `count_comment` varchar(255) DEFAULT NULL COMMENT '评论统计',
  `user_type` int(3) DEFAULT NULL COMMENT '用户类型',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COMMENT='评论表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_deck`
--

DROP TABLE IF EXISTS `tb_deck`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_deck` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `create_name` varchar(64) DEFAULT NULL,
  `modify_name` varchar(64) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `ip_addr` varchar(255) DEFAULT NULL,
  `thumbs_up` bigint(20) DEFAULT NULL,
  `to_user_id` varchar(255) DEFAULT NULL,
  `to_user_name` varchar(255) DEFAULT NULL,
  `to_user_type` varchar(255) DEFAULT NULL,
  `user_type` varchar(255) DEFAULT NULL,
  `reply_id` varchar(64) DEFAULT NULL,
  `tcid` int(11) DEFAULT NULL COMMENT '教师课程id',
  PRIMARY KEY (`id`),
  KEY `FK_213xeo79slqtttyagej51xj5y` (`reply_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='楼中楼';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_department`
--

DROP TABLE IF EXISTS `tb_department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_department` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(50) NOT NULL COMMENT '系别/部门名称',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '最近一次更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(8) DEFAULT '0' COMMENT '乐观锁',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_discuss`
--

DROP TABLE IF EXISTS `tb_discuss`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_discuss` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `view` int(200) DEFAULT NULL COMMENT '浏览量',
  `content` varchar(255) DEFAULT NULL COMMENT '内容',
  `essence` int(3) DEFAULT NULL COMMENT '精华',
  `ip_addr` varchar(50) DEFAULT NULL COMMENT 'ip地址',
  `thumbs_up` varchar(255) DEFAULT NULL COMMENT '点赞数',
  `reply_count` int(11) DEFAULT NULL COMMENT '回复数',
  `title` varchar(200) DEFAULT NULL COMMENT '标题',
  `type` int(3) DEFAULT NULL COMMENT '类型',
  `user_type` varchar(50) DEFAULT NULL,
  `forum_id` int(11) DEFAULT NULL,
  `last_reply_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='讨论表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_favorite`
--

DROP TABLE IF EXISTS `tb_favorite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_favorite` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `user_id` bigint(20) NOT NULL,
  `info_type` int(11) NOT NULL COMMENT '收藏类型：1为问题，2为文章',
  `info_id` bigint(20) NOT NULL COMMENT '被收藏的资源 id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='收藏表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_feed`
--

DROP TABLE IF EXISTS `tb_feed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_feed` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int(11) unsigned NOT NULL,
  `info_type` tinyint(5) NOT NULL COMMENT '信息类型，问答0，文章1，分享2，答案3',
  `info_id` int(11) unsigned NOT NULL COMMENT '信息id',
  `status` int(2) NOT NULL DEFAULT '0' COMMENT '状态，0正常，1删除',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8 COMMENT='关注表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_filter_keyword`
--

DROP TABLE IF EXISTS `tb_filter_keyword`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_filter_keyword` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `keyword` varchar(255) CHARACTER SET utf8mb4 NOT NULL COMMENT '需要过滤的关键词',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=507 DEFAULT CHARSET=utf8 COMMENT='注释';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_forum`
--

DROP TABLE IF EXISTS `tb_forum`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_forum` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `name` varchar(100) DEFAULT NULL COMMENT '板块名称',
  `type` int(3) DEFAULT NULL COMMENT '可见类型',
  `description` varchar(100) DEFAULT NULL COMMENT '描述',
  `tcid` int(11) DEFAULT NULL COMMENT '教师课程id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='板块表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_grades`
--

DROP TABLE IF EXISTS `tb_grades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_grades` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT 'createTime',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT 'updateTime',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '0' COMMENT '乐观锁',
  `name` varchar(40) NOT NULL COMMENT '年级名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COMMENT='年级表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_images`
--

DROP TABLE IF EXISTS `tb_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_images` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `img_url` varchar(255) NOT NULL,
  `img_name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `file_size` int(11) DEFAULT NULL,
  `img_width` int(11) DEFAULT NULL,
  `img_height` int(11) DEFAULT NULL,
  `signature` varchar(255) DEFAULT NULL,
  `img_delete` int(1) DEFAULT NULL,
  `sort` int(5) DEFAULT NULL,
  `picflag` int(1) DEFAULT NULL,
  `info_count` int(11) DEFAULT '0' COMMENT '图片使用次数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='图片表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_images_info_merge`
--

DROP TABLE IF EXISTS `tb_images_info_merge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_images_info_merge` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `info_id` varchar(50) NOT NULL,
  `img_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `pic_id` int(11) unsigned DEFAULT NULL,
  `info_type` tinyint(5) DEFAULT NULL COMMENT '信息类型，0问题，1答案，2文章，3分享',
  `info_count` int(11) DEFAULT '0' COMMENT '统计信息被使用次数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='图片信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_integral`
--

DROP TABLE IF EXISTS `tb_integral`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_integral` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `code` varchar(30) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '编码',
  `name` varchar(40) DEFAULT NULL COMMENT '名称',
  `order_num` int(5) DEFAULT NULL COMMENT '排序',
  `reward_type` int(2) unsigned DEFAULT NULL COMMENT '积分类型，0：奖励，1：惩罚',
  `cycle_type` int(3) DEFAULT NULL COMMENT '周期类型',
  `cycle_time` int(5) DEFAULT NULL COMMENT '间隔时间',
  `reward_num` int(8) DEFAULT NULL COMMENT '最多奖励次数',
  `credit` int(8) DEFAULT NULL COMMENT '积分',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='积分规则表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_integral_detail`
--

DROP TABLE IF EXISTS `tb_integral_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_integral_detail` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `user_id` int(11) unsigned NOT NULL COMMENT '用户id',
  `user_name` varchar(20) NOT NULL COMMENT '用户名',
  `integral_id` int(11) unsigned NOT NULL COMMENT '积分规则id',
  `type` int(3) DEFAULT NULL COMMENT '积分类型',
  `credit` int(5) DEFAULT NULL COMMENT '积分值',
  `type_name` varchar(50) DEFAULT NULL COMMENT '积分类型名',
  `content` varchar(255) DEFAULT NULL COMMENT '积分内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=181 DEFAULT CHARSET=utf8 COMMENT='积分明细表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_lead`
--

DROP TABLE IF EXISTS `tb_lead`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_lead` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `tc_id` int(11) DEFAULT NULL COMMENT '教师课程id',
  `content` longtext COMMENT '大纲内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='开篇导学';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_major`
--

DROP TABLE IF EXISTS `tb_major`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_major` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '0' COMMENT '乐观锁',
  `name` varchar(40) NOT NULL COMMENT '专业名称',
  `did` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_major_department` (`did`) USING BTREE,
  CONSTRAINT `tb_major_ibfk_1` FOREIGN KEY (`did`) REFERENCES `tb_department` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='专业表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_message`
--

DROP TABLE IF EXISTS `tb_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_message` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `source_user_id` int(11) unsigned NOT NULL COMMENT '来源用户id',
  `target_user_id` int(11) unsigned NOT NULL COMMENT '收件人id',
  `content` text COMMENT '信息内容',
  `has_view` tinyint(1) DEFAULT '0' COMMENT '阅读状态',
  `is_admin` tinyint(1) DEFAULT '0' COMMENT '是否为系统信息',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_outline`
--

DROP TABLE IF EXISTS `tb_outline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_outline` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `tc_id` int(11) DEFAULT NULL COMMENT '教师课程id',
  `content` longtext COMMENT '大纲内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='教学大纲';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_question`
--

DROP TABLE IF EXISTS `tb_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_question` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `user_id` int(11) unsigned DEFAULT NULL,
  `title` varchar(100) NOT NULL,
  `content` text,
  `score` int(11) DEFAULT '0' COMMENT '最佳答案奖励金币数量',
  `recommend` int(5) DEFAULT '0' COMMENT '推荐设置，数字越大越靠前',
  `status` tinyint(2) DEFAULT '0' COMMENT '0正常状态 1删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COMMENT='问题表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_question_count`
--

DROP TABLE IF EXISTS `tb_question_count`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_question_count` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `question_id` int(11) unsigned NOT NULL,
  `count_answer` int(11) DEFAULT '0' COMMENT '答案数量',
  `count_view` int(11) DEFAULT '0' COMMENT '该问题浏览数量',
  `count_dig` int(11) DEFAULT '0',
  `count_bury` int(11) DEFAULT '0',
  `count_follow` int(11) DEFAULT '0' COMMENT '关注该问题数量',
  `weight` double(11,0) DEFAULT '0' COMMENT '问题权重',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COMMENT='问题统计表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_question_follow_merge`
--

DROP TABLE IF EXISTS `tb_question_follow_merge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_question_follow_merge` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `question_id` int(11) unsigned NOT NULL COMMENT '被关注的问题',
  `user_id` int(11) unsigned NOT NULL COMMENT '关注者id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `question_follow_index` (`id`,`question_id`,`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='问题关注表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_recourse`
--

DROP TABLE IF EXISTS `tb_recourse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_recourse` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `name` varchar(50) DEFAULT NULL COMMENT '资源名称',
  `tc_id` int(11) unsigned NOT NULL COMMENT '课程',
  `attr_id` varchar(100) DEFAULT NULL COMMENT '附件',
  `count` int(11) DEFAULT NULL COMMENT '观看次数',
  `recourse_type` int(5) NOT NULL COMMENT '资源类型\r\n     * 1、视频\r\n     * 2、音频\r\n     * 3、excel\r\n     * 4、word\r\n     * 5、ppt\r\n     * 6、图片',
  `file_name` varchar(50) NOT NULL,
  `ext` varchar(30) DEFAULT NULL COMMENT '资源后缀名',
  `category` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_recourse_tc` (`tc_id`) USING BTREE,
  CONSTRAINT `tb_recourse_ibfk_1` FOREIGN KEY (`tc_id`) REFERENCES `tb_teacher_course` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8 COMMENT='资源表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_recourse_type`
--

DROP TABLE IF EXISTS `tb_recourse_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_recourse_type` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` int(11) unsigned NOT NULL COMMENT '创建者id',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` int(11) unsigned DEFAULT NULL COMMENT '最近一次更新者id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `name` varchar(50) NOT NULL,
  `tc_id` int(11) unsigned DEFAULT NULL,
  `order` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='资源类型表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_reply`
--

DROP TABLE IF EXISTS `tb_reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_reply` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `create_name` varchar(64) DEFAULT NULL,
  `modify_name` varchar(64) DEFAULT NULL,
  `adopt` int(11) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `ip_addr` varchar(255) DEFAULT NULL,
  `thumbs_up` bigint(20) DEFAULT NULL,
  `user_type` varchar(255) DEFAULT NULL,
  `topic_id` varchar(64) DEFAULT NULL,
  `tcid` int(11) DEFAULT NULL COMMENT '教师课程id',
  PRIMARY KEY (`id`),
  KEY `FK_fceuq1lqxowdjpucekwi404b3` (`topic_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_score_detail`
--

DROP TABLE IF EXISTS `tb_score_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_score_detail` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `type` varchar(100) DEFAULT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `balance` int(11) DEFAULT NULL,
  `foreign_id` int(11) unsigned DEFAULT NULL,
  `score_rule_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='积分明细表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_score_rule`
--

DROP TABLE IF EXISTS `tb_score_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_score_rule` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `name` varchar(100) DEFAULT NULL COMMENT '规则名称',
  `score` int(11) DEFAULT NULL COMMENT '变化积分',
  `type` varchar(100) DEFAULT 'unlimite' COMMENT '奖励次数类型，day每天一次，week每周一次，month每月一次，year每年一次，one只有一次，unlimite不限次数',
  `status` tinyint(2) DEFAULT '1' COMMENT '状态，0禁用，1启用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='积分规则表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_student`
--

DROP TABLE IF EXISTS `tb_student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_student` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '最近一次更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) DEFAULT '0' COMMENT '乐观锁',
  `enrollment_date` datetime DEFAULT NULL COMMENT '入学时间',
  `gid` int(11) unsigned NOT NULL COMMENT '年级id',
  `cid` int(11) unsigned NOT NULL COMMENT '班级id',
  PRIMARY KEY (`id`),
  KEY `fk_student_clbum` (`cid`) USING BTREE,
  KEY `fk_student_grades` (`gid`) USING BTREE,
  CONSTRAINT `tb_student_ibfk_1` FOREIGN KEY (`cid`) REFERENCES `tb_clbum` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tb_student_ibfk_2` FOREIGN KEY (`gid`) REFERENCES `tb_grades` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=475 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_tag`
--

DROP TABLE IF EXISTS `tb_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_tag` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '0' COMMENT '乐观锁',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `name` varchar(32) NOT NULL COMMENT '标签名',
  `posts` int(11) NOT NULL COMMENT '文章数',
  `thumbnail` varchar(128) DEFAULT NULL COMMENT '预览图',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ind_tab` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='标签表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_teacher`
--

DROP TABLE IF EXISTS `tb_teacher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_teacher` (
  `now_school_degree` varchar(50) DEFAULT NULL COMMENT '在职修读学历',
  `major_degree_get_time` datetime DEFAULT '1000-01-01 00:00:00' COMMENT '专业技术职称获取时间',
  `join_party_time` datetime DEFAULT '1000-01-01 00:00:00' COMMENT '加入党派时间',
  `teach_year` int(2) DEFAULT '-1' COMMENT '任教年限',
  `teacher_qualify_type` varchar(50) DEFAULT '' COMMENT '教师资格种类      1：幼儿园教师资格；      2：小学教师资格；      3：初级中学教师和初级职业学校文化课、专业课教师资格；      4：高级中学教师资格；      5：中等专业学校、技工学校、职业高级中学文化课、专业课教师资格；      6：中等专业学校、技工学校、职业高级中学实习指导教师资格；      7：高等学校教师资格。      8：成人/大学教育的教师资格，按照成人教育的层次，依照上款规定确定类别',
  `get_qualify_time` datetime DEFAULT '1000-01-01 00:00:00' COMMENT '取得教师资格证时间',
  `identity` varchar(50) DEFAULT '' COMMENT '身份',
  `first_education` varchar(50) DEFAULT '' COMMENT '第一学历',
  `first_graduate_school` varchar(50) DEFAULT '' COMMENT '第一学历毕业院校',
  `first_major` varchar(50) DEFAULT '' COMMENT '第一学历所学专业',
  `first_graduate_time` date DEFAULT NULL COMMENT '第一学历毕业时间',
  `hightst_degree` varchar(50) DEFAULT '' COMMENT '最高学位',
  `hight_graduate_time` date DEFAULT NULL COMMENT '最高学历毕业时间',
  `work_date` date DEFAULT '1000-01-01' COMMENT '参加工作时间',
  `marry_status` varchar(50) DEFAULT '' COMMENT '婚姻状况',
  `education` varchar(50) DEFAULT '' COMMENT '学历',
  `title` varchar(60) DEFAULT '' COMMENT '职称',
  `graduate_institution` varchar(60) DEFAULT '' COMMENT '毕业院校',
  `mid` varchar(60) DEFAULT '' COMMENT '专业',
  `duty` varchar(60) DEFAULT '' COMMENT '职务',
  `edited` varchar(50) DEFAULT '' COMMENT '编制',
  `on_job_status` int(1) NOT NULL DEFAULT '1' COMMENT '在职状态，1：在职，2：离职',
  `dimission_date` datetime DEFAULT '1000-01-01 00:00:00' COMMENT '离职日期',
  `dimission_type` varchar(50) DEFAULT '' COMMENT '离职类型',
  `dimission_reason` varchar(100) DEFAULT '' COMMENT '离职原因',
  `learning_experience` varchar(255) DEFAULT '' COMMENT '学习经历',
  `work_experience` varchar(255) DEFAULT '' COMMENT '工作经历',
  `training_experience` varchar(255) DEFAULT '' COMMENT '培训经历',
  `professional_certificate` varchar(100) DEFAULT '' COMMENT '专业证书',
  `punish_situation` varchar(100) DEFAULT '' COMMENT '奖惩情况',
  `create_archive` int(12) DEFAULT '-1' COMMENT '建档情况 0：未建，1：已建',
  `other_dept` varchar(50) DEFAULT '' COMMENT '其他部门',
  `dept_name` varchar(50) DEFAULT '' COMMENT '部门名称序列',
  `job_ids` varchar(50) DEFAULT '' COMMENT '岗位id序列',
  `other_job_name` varchar(50) DEFAULT '' COMMENT '其他岗位名称',
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) NOT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '最近一次更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(8) NOT NULL DEFAULT '0' COMMENT '乐观锁',
  `clbum_id` int(11) unsigned DEFAULT NULL COMMENT '班级',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8 COMMENT='教师属性表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_teacher_course`
--

DROP TABLE IF EXISTS `tb_teacher_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_teacher_course` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '0' COMMENT '乐观锁',
  `course_brief_introduction` varchar(100) DEFAULT NULL COMMENT '课程简介',
  `thumbnail_path` varchar(255) DEFAULT NULL COMMENT '预览图',
  `tid` int(11) unsigned NOT NULL,
  `cid` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tc_course` (`cid`) USING BTREE,
  KEY `fk_tc_teacher` (`tid`) USING BTREE,
  CONSTRAINT `tb_teacher_course_ibfk_1` FOREIGN KEY (`cid`) REFERENCES `tb_course` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `tb_teacher_course_ibfk_2` FOREIGN KEY (`tid`) REFERENCES `tb_teacher` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=245 DEFAULT CHARSET=utf8 COMMENT='教师课程';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_topic`
--

DROP TABLE IF EXISTS `tb_topic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_topic` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `create_name` varchar(64) DEFAULT NULL,
  `modify_name` varchar(64) DEFAULT NULL,
  `browse` bigint(20) DEFAULT '0',
  `content` varchar(255) DEFAULT NULL,
  `essence` int(11) unsigned DEFAULT '0',
  `ip_addr` varchar(255) DEFAULT NULL,
  `thumbs_up` bigint(20) unsigned DEFAULT '0',
  `reply_count` int(11) unsigned DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `user_type` varchar(255) DEFAULT NULL,
  `forum_id` varchar(64) DEFAULT NULL,
  `last_reply_id` varchar(64) DEFAULT NULL,
  `tcid` int(11) DEFAULT NULL COMMENT '教师id',
  PRIMARY KEY (`id`),
  KEY `FK_baib441t3yn3ru6oxkj6exi0l` (`forum_id`) USING BTREE,
  KEY `FK_7ljjyk3dn8ynq6eny0eaoi8d0` (`last_reply_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_topic_category`
--

DROP TABLE IF EXISTS `tb_topic_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_topic_category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `father_id` int(11) unsigned DEFAULT '0' COMMENT '父级id',
  `name` varchar(100) NOT NULL COMMENT '栏目名称',
  `custom_url` varchar(255) DEFAULT NULL COMMENT '自定义URL，如果为空直接调用ID',
  `keywords` varchar(255) DEFAULT NULL COMMENT '分类关键词，SEO',
  `description` varchar(255) DEFAULT NULL COMMENT '分类说明',
  `is_recommend` tinyint(2) DEFAULT '0' COMMENT '是否推荐',
  `sort` int(5) DEFAULT '0' COMMENT '排序',
  `status` tinyint(2) DEFAULT '1' COMMENT '是否在导航显示',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='话题类别表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_topic_category_merge`
--

DROP TABLE IF EXISTS `tb_topic_category_merge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_topic_category_merge` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `topic_id` int(11) unsigned NOT NULL,
  `type_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_topic_edlt`
--

DROP TABLE IF EXISTS `tb_topic_edlt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_topic_edlt` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `user_id` int(11) unsigned NOT NULL,
  `topic_id` int(11) unsigned NOT NULL COMMENT '话题id',
  `content` text COMMENT '话题内容',
  `status` int(2) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_topic_follow`
--

DROP TABLE IF EXISTS `tb_topic_follow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_topic_follow` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `topic_id` int(11) unsigned NOT NULL COMMENT '用户关注标签id',
  `user_id` int(11) unsigned NOT NULL COMMENT '用户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='话题关注表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_topic_info_merge`
--

DROP TABLE IF EXISTS `tb_topic_info_merge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_topic_info_merge` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `info_id` int(11) unsigned DEFAULT NULL,
  `topic_id` int(11) unsigned DEFAULT NULL,
  `info_type` smallint(2) NOT NULL DEFAULT '0' COMMENT '信息类型，0问题，1文章，2分享',
  `status` tinyint(1) DEFAULT '1' COMMENT '信息显示状态，默认为显示，0不显示，1显示',
  PRIMARY KEY (`id`),
  UNIQUE KEY `topic_index` (`info_id`,`topic_id`,`info_type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_user_count`
--

DROP TABLE IF EXISTS `tb_user_count`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_user_count` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `user_id` int(11) unsigned NOT NULL COMMENT '用户id',
  `count_question` int(11) DEFAULT '0' COMMENT '发布问题数量',
  `count_question_follw` int(11) DEFAULT '0' COMMENT '关注的问题数量',
  `count_topic` int(11) DEFAULT '0' COMMENT '关注话题数量',
  `count_answer` int(11) DEFAULT '0' COMMENT '回答问题数量',
  `count_share` int(11) DEFAULT '0' COMMENT '发布分享数量',
  `count_article` int(11) DEFAULT '0' COMMENT '发布文章数量',
  `count_fans` int(11) DEFAULT '0' COMMENT '所有粉丝数量',
  `count_follw` int(11) DEFAULT '0' COMMENT '关注数量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='用户信息统计表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_user_fans`
--

DROP TABLE IF EXISTS `tb_user_fans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_user_fans` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '最近一次更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `version` int(12) NOT NULL DEFAULT '-1' COMMENT '乐观锁',
  `user_follow_id` int(11) unsigned DEFAULT '0' COMMENT '被关注者用户id',
  `user_fans_id` int(11) unsigned DEFAULT '0' COMMENT '粉丝id，关注者的id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_follow_who_who_follow` (`id`,`user_follow_id`,`user_fans_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='用户粉丝表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tk_center_paper`
--

DROP TABLE IF EXISTS `tk_center_paper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tk_center_paper` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_id` int(11) unsigned DEFAULT NULL COMMENT '创建人Id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新人',
  `update_id` int(11) DEFAULT NULL COMMENT '更新人Id',
  `version` int(11) DEFAULT NULL COMMENT '乐观锁',
  `end_time` varchar(255) DEFAULT NULL COMMENT '结束时间',
  `headline` longtext COMMENT '标题',
  `start_time` varchar(255) DEFAULT NULL COMMENT ' 开始时间',
  `test_name` varchar(255) DEFAULT NULL COMMENT '试卷名',
  `test_year` varchar(255) DEFAULT NULL COMMENT '年份',
  `end_num` int(11) NOT NULL DEFAULT '0' COMMENT '结束人数',
  `review_num` int(11) NOT NULL DEFAULT '0' COMMENT '在考人数',
  `start_num` int(11) NOT NULL DEFAULT '0' COMMENT '开始人数',
  `state` int(11) NOT NULL DEFAULT '0' COMMENT '状态',
  `bei_zhu` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='期末考试试卷表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tk_chapter_questionscore`
--

DROP TABLE IF EXISTS `tk_chapter_questionscore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tk_chapter_questionscore` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_id` int(11) unsigned DEFAULT NULL COMMENT '创建ID',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_id` varchar(64) DEFAULT NULL COMMENT '更新ID',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新人',
  `version` int(11) DEFAULT NULL COMMENT '乐观锁',
  `questionscore` int(11) NOT NULL DEFAULT '0',
  `chapter_id` int(11) DEFAULT NULL COMMENT '节id',
  `test_paper_questions` varchar(64) DEFAULT NULL COMMENT '得分',
  `stu_id` int(11) NOT NULL COMMENT '学生Id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='章节测试分数表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tk_chapter_testlog`
--

DROP TABLE IF EXISTS `tk_chapter_testlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tk_chapter_testlog` (
  `id` int(11) NOT NULL COMMENT '主键',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_id` int(11) DEFAULT NULL COMMENT '创建人ID',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_id` int(11) DEFAULT NULL COMMENT '更新人ID',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `paper_id` int(11) DEFAULT NULL COMMENT '试卷ID',
  `score` int(6) DEFAULT NULL COMMENT '总分\n',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tk_chaptertest_optinanswer`
--

DROP TABLE IF EXISTS `tk_chaptertest_optinanswer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tk_chaptertest_optinanswer` (
  `id` varchar(64) NOT NULL COMMENT '主键',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_id` int(11) DEFAULT NULL COMMENT '创建人Id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新人',
  `update_id` int(11) DEFAULT NULL COMMENT '更新人Id',
  `version` int(11) DEFAULT NULL COMMENT '乐观锁',
  `question_score` int(11) NOT NULL DEFAULT '0' COMMENT '题目分数',
  `stanswer` varchar(255) DEFAULT NULL COMMENT '答案',
  `stoption` varchar(255) DEFAULT NULL COMMENT '选项',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='章节试卷答案';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tk_chaptertest_questions`
--

DROP TABLE IF EXISTS `tk_chaptertest_questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tk_chaptertest_questions` (
  `id` varchar(64) NOT NULL COMMENT '主键',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_id` int(11) DEFAULT NULL COMMENT '创建Id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新人',
  `update_id` int(11) DEFAULT NULL COMMENT '更新人Id',
  `version` int(11) DEFAULT NULL COMMENT '乐观锁',
  `difficulty` varchar(64) DEFAULT NULL COMMENT '难度',
  `parsing` varchar(255) DEFAULT NULL COMMENT '解析',
  `qexposed` varchar(64) DEFAULT NULL COMMENT '试题已曝光数',
  `qmaxexposure` varchar(64) DEFAULT NULL COMMENT '试题最大曝光',
  `ststatus` int(11) DEFAULT NULL COMMENT '状态0：待审核、1：审核通过、2：审核不通过',
  `title` longtext COMMENT '名',
  `year` int(11) DEFAULT NULL COMMENT '年份',
  `chapter_id` int(11) unsigned NOT NULL COMMENT '节Id',
  `title_type_id` int(11) unsigned NOT NULL COMMENT '题型Id',
  `xzsubjects_id` int(11) unsigned NOT NULL COMMENT '课程Id',
  `test_paper_option_answer_arr` varchar(255) DEFAULT NULL COMMENT '答案',
  `question_score` int(11) NOT NULL DEFAULT '0' COMMENT '题目分数',
  PRIMARY KEY (`id`),
  KEY `chapter_id` (`chapter_id`) USING BTREE,
  KEY `title_type_id` (`title_type_id`) USING BTREE,
  KEY `xzsubjects_id` (`xzsubjects_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='章节试卷题目';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tk_chaptertest_testquestions`
--

DROP TABLE IF EXISTS `tk_chaptertest_testquestions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tk_chaptertest_testquestions` (
  `id` varchar(64) NOT NULL COMMENT '主键',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_id` int(11) DEFAULT NULL COMMENT '创建人Id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新人',
  `update_id` int(11) DEFAULT NULL COMMENT '更新Id',
  `version` int(11) DEFAULT NULL COMMENT '乐观锁',
  `school_no` varchar(255) DEFAULT NULL COMMENT '学校No',
  `chapter_id` int(11) unsigned NOT NULL COMMENT '章节Id',
  `test_questions_id` varchar(64) NOT NULL COMMENT '题目Id',
  KEY `chapter_id` (`chapter_id`) USING BTREE,
  KEY `test_questions_id` (`test_questions_id`) USING BTREE,
  CONSTRAINT `tk_chaptertest_testquestions_ibfk_1` FOREIGN KEY (`chapter_id`) REFERENCES `tb_chapter` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='章节与测试卷关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tk_coursepaper_coursequestions`
--

DROP TABLE IF EXISTS `tk_coursepaper_coursequestions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tk_coursepaper_coursequestions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` timestamp NULL DEFAULT NULL,
  `create_by` tinytext,
  `create_id` int(11) DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL,
  `update_by` tinytext,
  `update_id` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `school_no` tinytext,
  `test_paper_id` int(10) unsigned DEFAULT NULL,
  `test_questions_id` tinytext,
  `student_id` int(11) DEFAULT NULL COMMENT '学生ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2711 DEFAULT CHARSET=utf8mb4 COMMENT='试卷做的题目答案';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tk_coursetest_stuoptionanswer`
--

DROP TABLE IF EXISTS `tk_coursetest_stuoptionanswer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tk_coursetest_stuoptionanswer` (
  `id` varchar(64) NOT NULL COMMENT '主键',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_id` int(11) unsigned NOT NULL COMMENT '创建人Id',
  `create_by` varchar(64) NOT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_id` int(11) DEFAULT NULL COMMENT '更新人ID',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新人',
  `version` int(11) DEFAULT NULL COMMENT '乐观锁',
  `questionscore` int(11) NOT NULL DEFAULT '0' COMMENT '题目分数',
  `stuanswer` longtext COMMENT '学生答案',
  `testPaperOptionAnswer` varchar(64) DEFAULT NULL COMMENT '题目答案',
  `stoption` varchar(255) DEFAULT NULL COMMENT '答案内容',
  PRIMARY KEY (`id`),
  KEY `tk_coursetest_stuoptionanswer_tk_testpaper_optinanswer_id_fk` (`testPaperOptionAnswer`) USING BTREE,
  CONSTRAINT `tk_coursetest_stuoptionanswer_ibfk_1` FOREIGN KEY (`testPaperOptionAnswer`) REFERENCES `tk_testpaper_optinanswer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='学生答案（课程考试）';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tk_my_chapter`
--

DROP TABLE IF EXISTS `tk_my_chapter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tk_my_chapter` (
  `id` varchar(64) NOT NULL,
  `create_time` datetime DEFAULT NULL,
  `create_id` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_id` int(11) DEFAULT NULL,
  `chapterName` varchar(255) DEFAULT NULL,
  `chapterNo` int(11) DEFAULT NULL,
  `chaptertitle` varchar(255) DEFAULT NULL,
  `serialNo` int(11) DEFAULT NULL,
  `XZsubjectsID` varchar(64) DEFAULT NULL,
  `parent` varchar(64) DEFAULT NULL,
  `schoolNo` varchar(255) DEFAULT NULL,
  `version` int(12) DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_kcbyh7hedy3e2fmjqwy4oaqbm` (`XZsubjectsID`) USING BTREE,
  KEY `FK_cla3o9a7rvfwn3dnmk8hsc1n4` (`parent`) USING BTREE,
  CONSTRAINT `tk_my_chapter_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `tk_my_chapter` (`id`),
  CONSTRAINT `tk_my_chapter_ibfk_2` FOREIGN KEY (`XZsubjectsID`) REFERENCES `tk_my_xzsubjects` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='我的题库分类（章节表）';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tk_my_optinanswer`
--

DROP TABLE IF EXISTS `tk_my_optinanswer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tk_my_optinanswer` (
  `id` varchar(64) NOT NULL,
  `create_time` datetime DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `create_id` int(11) unsigned DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL,
  `update_id` int(11) unsigned DEFAULT NULL,
  `version` int(12) DEFAULT NULL,
  `schoolNo` varchar(255) DEFAULT NULL,
  `stanswer` varchar(255) DEFAULT NULL,
  `stoption` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='我的题库答案表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tk_my_questions`
--

DROP TABLE IF EXISTS `tk_my_questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tk_my_questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_time` datetime DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `create_id` int(11) unsigned DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL,
  `update_id` int(11) DEFAULT NULL,
  `version` int(12) DEFAULT NULL,
  `schoolNo` varchar(255) DEFAULT NULL,
  `difficulty` varchar(255) DEFAULT NULL,
  `parsing` varchar(255) DEFAULT NULL,
  `qexposed` varchar(255) DEFAULT NULL,
  `qmaxexposure` varchar(255) DEFAULT NULL,
  `ststatus` int(11) DEFAULT NULL,
  `title` longtext CHARACTER SET utf8,
  `year` int(11) DEFAULT NULL,
  `chapterId` int(11) unsigned NOT NULL COMMENT '章节Id',
  `titleTypeId` int(11) unsigned NOT NULL,
  `xzsubjectsId` int(11) unsigned NOT NULL,
  `myoptionAnswerArr` varchar(255) DEFAULT NULL,
  `questionscore` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tk_my_questions_ibfk_1` (`xzsubjectsId`) USING BTREE,
  KEY `create_id` (`create_id`) USING BTREE,
  KEY `tk_my_questions_tk_my_title_type_id_fk` (`titleTypeId`) USING BTREE,
  KEY `chapterId` (`chapterId`) USING BTREE,
  CONSTRAINT `tk_my_questions_ibfk_1` FOREIGN KEY (`xzsubjectsId`) REFERENCES `tb_course` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tk_my_questions_ibfk_2` FOREIGN KEY (`create_id`) REFERENCES `tb_teacher` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tk_my_questions_ibfk_3` FOREIGN KEY (`chapterId`) REFERENCES `tb_chapter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=165 DEFAULT CHARSET=utf8mb4 COMMENT='我的题库表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tk_my_title_type`
--

DROP TABLE IF EXISTS `tk_my_title_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tk_my_title_type` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_id` int(11) DEFAULT NULL COMMENT '教师Id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_id` int(11) DEFAULT NULL COMMENT '更新人Id',
  `version` int(12) DEFAULT NULL COMMENT '乐观锁',
  `public_title_id` varchar(64) NOT NULL COMMENT '平台题型ID',
  `title_type_name` varchar(255) DEFAULT NULL COMMENT '题型名',
  `cid` int(11) unsigned NOT NULL COMMENT '课程ID',
  PRIMARY KEY (`id`),
  KEY `public_title_id` (`public_title_id`) USING BTREE,
  KEY `tk_my_title_type_ibfk_2` (`cid`) USING BTREE,
  CONSTRAINT `tk_my_title_type_ibfk_1` FOREIGN KEY (`cid`) REFERENCES `tb_course` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `tk_my_title_type_ibfk_2` FOREIGN KEY (`public_title_id`) REFERENCES `tk_title_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COMMENT='我的题型表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tk_my_xzsubjects`
--

DROP TABLE IF EXISTS `tk_my_xzsubjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tk_my_xzsubjects` (
  `id` varchar(64) NOT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `departmentName` varchar(255) DEFAULT NULL,
  `professionalName` varchar(255) DEFAULT NULL,
  `subjectsName` varchar(255) DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL,
  `update_id` int(11) unsigned DEFAULT NULL,
  `create_id` int(11) unsigned DEFAULT NULL,
  `version` int(12) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='我的专业表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tk_public_optinanswer`
--

DROP TABLE IF EXISTS `tk_public_optinanswer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tk_public_optinanswer` (
  `id` varchar(64) NOT NULL,
  `create_time` datetime DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `create_id` int(11) unsigned NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL,
  `update_id` int(11) unsigned DEFAULT NULL,
  `version` int(12) DEFAULT NULL,
  `school_no` varchar(255) DEFAULT NULL,
  `stanswer` varchar(255) DEFAULT NULL,
  `stoption` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='公共题库答案表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tk_public_questions`
--

DROP TABLE IF EXISTS `tk_public_questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tk_public_questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `create_id` int(11) unsigned NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL,
  `update_id` int(11) unsigned DEFAULT NULL,
  `version` int(12) DEFAULT NULL,
  `school_no` varchar(255) DEFAULT NULL,
  `difficulty` varchar(255) DEFAULT NULL,
  `parsing` varchar(255) DEFAULT NULL,
  `public_option_answer_arr` varchar(255) DEFAULT NULL,
  `qexposed` varchar(255) DEFAULT NULL,
  `qmaxexposure` varchar(255) DEFAULT NULL,
  `ststatus` int(11) DEFAULT NULL,
  `title` longtext,
  `year` int(11) NOT NULL,
  `chapter_id` int(11) unsigned NOT NULL,
  `title_type_id` varchar(64) NOT NULL,
  `xzsubjectsId` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `create_id` (`create_id`) USING BTREE,
  KEY `tk_public_questions_ibfk_1` (`chapter_id`) USING BTREE,
  KEY `tk_public_questions_ibfk_2` (`xzsubjectsId`) USING BTREE,
  CONSTRAINT `tk_public_questions_ibfk_1` FOREIGN KEY (`xzsubjectsId`) REFERENCES `tb_course` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COMMENT='公共题库表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tk_stu_optionanswer`
--

DROP TABLE IF EXISTS `tk_stu_optionanswer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tk_stu_optionanswer` (
  `id` varchar(64) NOT NULL DEFAULT '主键',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人名',
  `create_id` int(11) unsigned NOT NULL COMMENT '创建人Id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_id` varchar(64) DEFAULT NULL COMMENT '更新人Id',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新人名',
  `version` int(11) DEFAULT NULL COMMENT '乐观锁',
  `question_score` int(11) NOT NULL DEFAULT '0' COMMENT '题目分数',
  `stu_answer` varchar(255) DEFAULT NULL COMMENT '学生答案',
  `testpaper_optionanswer` varchar(64) DEFAULT NULL COMMENT '试卷答案',
  `chapter_id` int(11) DEFAULT NULL COMMENT '章节 节的id',
  `paper_id` int(11) DEFAULT NULL COMMENT '试卷ID',
  `stoption` varchar(255) DEFAULT NULL COMMENT '答案',
  PRIMARY KEY (`id`),
  KEY `FK_f69tsrq084wkqv6s48j0x1h6i` (`testpaper_optionanswer`) USING BTREE,
  KEY `create_id` (`create_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='学生考试答案表（节测试）';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tk_stuquestionscore`
--

DROP TABLE IF EXISTS `tk_stuquestionscore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tk_stuquestionscore` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_id` int(11) unsigned DEFAULT NULL COMMENT '创建ID',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_id` varchar(64) DEFAULT NULL COMMENT '更新ID',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新人',
  `version` int(11) DEFAULT NULL COMMENT '乐观锁',
  `questionscore` int(11) NOT NULL DEFAULT '0',
  `chapter_id` int(11) DEFAULT NULL COMMENT '节id',
  `test_paper_questions` varchar(64) DEFAULT NULL COMMENT '得分',
  `stu_id` int(11) NOT NULL COMMENT '学生Id',
  PRIMARY KEY (`id`),
  KEY `FK_awqwttlh45rpd5eaw6kjsl5a0` (`chapter_id`) USING BTREE,
  KEY `create_id` (`create_id`) USING BTREE,
  CONSTRAINT `tk_stuquestionscore_ibfk_1` FOREIGN KEY (`create_id`) REFERENCES `tb_student` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=utf8 COMMENT='分数表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tk_test_chapter_paper`
--

DROP TABLE IF EXISTS `tk_test_chapter_paper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tk_test_chapter_paper` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(64) DEFAULT NULL COMMENT '名字',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `paper_id` int(11) unsigned NOT NULL COMMENT '试卷Id',
  `c_id` int(11) unsigned NOT NULL COMMENT '节id',
  PRIMARY KEY (`id`),
  KEY `tk_test_chapter_paper_tk_test_paper_id_fk` (`paper_id`) USING BTREE,
  KEY `c_id` (`c_id`) USING BTREE,
  CONSTRAINT `tk_test_chapter_paper_ibfk_1` FOREIGN KEY (`c_id`) REFERENCES `tb_chapter` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COMMENT='节的试卷';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tk_test_paper`
--

DROP TABLE IF EXISTS `tk_test_paper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tk_test_paper` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_id` int(11) unsigned DEFAULT NULL COMMENT '创建人Id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新人',
  `update_id` int(11) DEFAULT NULL COMMENT '更新人Id',
  `version` int(11) DEFAULT NULL COMMENT '乐观锁',
  `school_no` varchar(255) DEFAULT NULL COMMENT '学校No',
  `end_time` varchar(255) DEFAULT NULL COMMENT '结束时间',
  `headline` longtext COMMENT '标题',
  `start_time` varchar(255) DEFAULT NULL COMMENT ' 开始时间',
  `test_name` varchar(255) DEFAULT NULL COMMENT '试卷名',
  `test_year` varchar(255) DEFAULT NULL COMMENT '年份',
  `end_num` int(11) NOT NULL DEFAULT '0' COMMENT '结束人数',
  `review_num` int(11) NOT NULL DEFAULT '0' COMMENT '在考人数',
  `start_num` int(11) NOT NULL DEFAULT '0' COMMENT '开始人数',
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '状态',
  `bei_zhu` varchar(255) DEFAULT NULL COMMENT '备注',
  `coursr_id` int(11) unsigned NOT NULL COMMENT '课程ID',
  `type` int(11) NOT NULL COMMENT '0：章节作业 1：课程作业 2：课程考试',
  `set_scored` varchar(11) DEFAULT '0' COMMENT '是否已改卷 0：否 1：是',
  `score` int(6) DEFAULT '0' COMMENT '试卷总分',
  `time` varchar(10) DEFAULT NULL COMMENT '考试时长',
  `rule` varchar(6) DEFAULT NULL COMMENT '组卷规则\n1 随机\n0固定\n\n',
  `set_exam` varchar(6) DEFAULT '0' COMMENT '是否开始考试\n0，否\n1，是',
  PRIMARY KEY (`id`),
  KEY `create_id` (`create_id`) USING BTREE,
  KEY `coursr_id` (`coursr_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8mb4 COMMENT='试卷表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tk_test_stuoptionanswer`
--

DROP TABLE IF EXISTS `tk_test_stuoptionanswer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tk_test_stuoptionanswer` (
  `id` varchar(64) NOT NULL COMMENT '主键',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_id` int(11) unsigned NOT NULL COMMENT '创建人Id',
  `create_by` varchar(64) NOT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_id` int(11) NOT NULL COMMENT '更新人ID',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新人',
  `version` int(11) DEFAULT NULL COMMENT '乐观锁',
  `questionscore` int(11) NOT NULL DEFAULT '0' COMMENT '题目分数',
  `stuanswer` varchar(255) DEFAULT NULL COMMENT '学生答案',
  `testPaperOptionAnswer` varchar(64) DEFAULT NULL COMMENT '题目答案',
  `chapter_id` int(11) unsigned NOT NULL COMMENT '节ID',
  `stoption` varchar(255) DEFAULT NULL COMMENT '答案内容',
  PRIMARY KEY (`id`),
  KEY `FK_f69tsrq084wkqv6s48j0x1h6i` (`testPaperOptionAnswer`) USING BTREE,
  CONSTRAINT `tk_test_stuoptionanswer_ibfk_1` FOREIGN KEY (`testPaperOptionAnswer`) REFERENCES `tk_testpaperoptinanswer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='学生答案（节测试）';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tk_testpaper_optinanswer`
--

DROP TABLE IF EXISTS `tk_testpaper_optinanswer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tk_testpaper_optinanswer` (
  `id` varchar(64) NOT NULL COMMENT '主键',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_id` int(11) DEFAULT NULL COMMENT '创建人Id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新人',
  `update_id` int(11) DEFAULT NULL COMMENT '更新人Id',
  `version` int(11) DEFAULT NULL COMMENT '乐观锁',
  `school_no` varchar(255) DEFAULT NULL COMMENT '学校No',
  `question_score` int(11) NOT NULL DEFAULT '0' COMMENT '题目分数',
  `stanswer` varchar(255) DEFAULT NULL COMMENT '答案',
  `stoption` varchar(255) DEFAULT NULL COMMENT '选项',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='试卷答案';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tk_testpaper_questions`
--

DROP TABLE IF EXISTS `tk_testpaper_questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tk_testpaper_questions` (
  `id` varchar(64) NOT NULL COMMENT '主键',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_id` int(11) DEFAULT NULL COMMENT '创建Id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新人',
  `update_id` int(11) DEFAULT NULL COMMENT '更新人Id',
  `version` int(11) DEFAULT NULL COMMENT '乐观锁',
  `school_no` varchar(255) DEFAULT NULL COMMENT '学校No',
  `difficulty` varchar(255) DEFAULT NULL COMMENT '难度',
  `parsing` varchar(255) DEFAULT NULL COMMENT '解析',
  `qexposed` varchar(255) DEFAULT NULL COMMENT '试题已曝光数',
  `qmaxexposure` varchar(255) DEFAULT NULL COMMENT '试题最大曝光',
  `ststatus` int(11) DEFAULT NULL COMMENT '状态0：待审核、1：审核通过、2：审核不通过',
  `title` longtext COMMENT '名',
  `year` int(11) DEFAULT NULL COMMENT '年份',
  `chapter_id` varchar(64) DEFAULT NULL COMMENT '章Id',
  `title_type_id` varchar(64) DEFAULT NULL COMMENT '题型Id',
  `xzsubjects_id` varchar(64) DEFAULT NULL,
  `test_paper_option_answer_arr` varchar(255) DEFAULT NULL COMMENT '答案',
  `question_score` int(11) NOT NULL DEFAULT '0' COMMENT '题目分数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='试卷题目';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tk_testpaper_testquestions`
--

DROP TABLE IF EXISTS `tk_testpaper_testquestions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tk_testpaper_testquestions` (
  `id` varchar(64) NOT NULL COMMENT '主键',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_id` int(11) DEFAULT NULL COMMENT '创建人Id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新人',
  `update_id` int(11) DEFAULT NULL COMMENT '更新Id',
  `version` int(11) DEFAULT NULL COMMENT '乐观锁',
  `school_no` varchar(255) DEFAULT NULL COMMENT '学校No',
  `test_paper_id` int(11) unsigned NOT NULL COMMENT '试卷Id',
  `test_questions_id` varchar(64) NOT NULL COMMENT '题目Id',
  PRIMARY KEY (`id`),
  KEY `FK_miput3bu2mvuwngwj2s9iuuil` (`test_paper_id`) USING BTREE,
  KEY `FK_6bkqnyegga3ikdj9eunkrdg7b` (`test_questions_id`) USING BTREE,
  CONSTRAINT `tk_testpaper_testquestions_ibfk_1` FOREIGN KEY (`test_paper_id`) REFERENCES `tk_test_paper` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tk_testpaper_testquestions_ibfk_2` FOREIGN KEY (`test_questions_id`) REFERENCES `tk_testpaper_questions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='试卷做的题目答案';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tk_title_type`
--

DROP TABLE IF EXISTS `tk_title_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tk_title_type` (
  `id` varchar(64) NOT NULL,
  `create_time` datetime DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `create_id` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL,
  `update_id` int(11) DEFAULT NULL,
  `version` int(12) DEFAULT NULL,
  `titleTypeBZ` varchar(255) DEFAULT NULL,
  `titleTypeName` varchar(255) DEFAULT NULL,
  `titleTypeNum` varchar(255) DEFAULT NULL,
  `schoolNo` varchar(255) DEFAULT NULL,
  `templateNum` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='平台题型表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tk_user_test`
--

DROP TABLE IF EXISTS `tk_user_test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tk_user_test` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '''主键''',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_id` int(11) unsigned DEFAULT NULL COMMENT '创建Id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新人',
  `update_id` int(11) DEFAULT NULL COMMENT '更新人Id',
  `version` int(11) DEFAULT NULL COMMENT '客观锁',
  `school_no` varchar(255) DEFAULT NULL COMMENT '学校No',
  `stu_start_time` datetime DEFAULT NULL,
  `stu_end_time` datetime DEFAULT NULL COMMENT '学生结束时间',
  `test_time` int(11) DEFAULT NULL COMMENT '测试时间',
  `sumscore` int(11) DEFAULT '0' COMMENT '总分',
  `user_id` int(11) unsigned NOT NULL COMMENT '用户Id',
  `test_paper_id` int(11) unsigned NOT NULL COMMENT '试卷Id',
  `sumbit_state` varchar(20) DEFAULT '0' COMMENT '提交状态 0没有 1是',
  `c_id` int(11) unsigned NOT NULL COMMENT '课程ID',
  `made_score` varchar(20) DEFAULT '0' COMMENT '是否已改卷 0：未，1：是',
  `stu_score` int(11) DEFAULT '0' COMMENT '学生分数',
  PRIMARY KEY (`id`),
  KEY `FK_r5ql7y0hcm1u05wnf999u5vah` (`test_paper_id`) USING BTREE,
  KEY `create_id` (`create_id`) USING BTREE,
  KEY `tk_user_test_ibfk_2` (`user_id`) USING BTREE,
  KEY `tk_user_test_ibfk_3` (`c_id`) USING BTREE,
  CONSTRAINT `tk_user_test_ibfk_1` FOREIGN KEY (`c_id`) REFERENCES `tb_course` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tk_user_test_ibfk_2` FOREIGN KEY (`test_paper_id`) REFERENCES `tk_test_paper` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=455 DEFAULT CHARSET=utf8mb4 COMMENT='测试关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tk_websocket`
--

DROP TABLE IF EXISTS `tk_websocket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tk_websocket` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(64) DEFAULT NULL COMMENT '名字',
  `tc_id` varchar(64) DEFAULT NULL COMMENT '试卷或者章节的ID',
  `create_time` varchar(64) DEFAULT NULL COMMENT '创建时间',
  `stu_id` int(11) unsigned NOT NULL COMMENT '考生ID',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_by` varchar(64) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `stu_num` varchar(64) DEFAULT NULL COMMENT '考生学号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=178 DEFAULT CHARSET=utf8 COMMENT='存放考试信息的临时表';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-18 16:31:38
