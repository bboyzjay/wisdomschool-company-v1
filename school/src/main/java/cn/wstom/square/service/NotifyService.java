package cn.wstom.square.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.system.entity.Notify;

/**
 * @author dws
 */
public interface NotifyService extends BaseService<Notify> {
}
