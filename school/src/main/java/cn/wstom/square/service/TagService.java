package cn.wstom.square.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.square.entity.Tag;

/**
 * @author : dws
 */
public interface TagService extends BaseService<Tag> {
}
