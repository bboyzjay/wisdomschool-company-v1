package cn.wstom.square.service.impl;

import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.square.entity.ArticleVotes;
import cn.wstom.square.mapper.ArticleVotesMapper;
import cn.wstom.square.service.ArticleVotesService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author dws
 * @date 2019/03/07
 */
@Service
public class ArticleVotesServiceImpl extends BaseServiceImpl
        <ArticleVotesMapper, ArticleVotes>
        implements ArticleVotesService {

    @Resource
    private ArticleVotesMapper articleVotesMapper;
}
