package cn.wstom.square.service.impl;

import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.square.entity.Channel;
import cn.wstom.square.mapper.ChannelMapper;
import cn.wstom.square.service.ChannelService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 栏目 服务层实现
 *
 * @author dws
 * @date 20190227
 */
@Service
public class ChannelServiceImpl extends BaseServiceImpl
        <ChannelMapper, Channel>
        implements ChannelService {

    @Resource
    private ChannelMapper channelMapper;
}
