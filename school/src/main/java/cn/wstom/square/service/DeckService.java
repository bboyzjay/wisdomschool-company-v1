package cn.wstom.square.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.square.entity.Deck;

/**
 * @author dws
 * @date 2019/03/07
 */
public interface DeckService extends BaseService<Deck> {
}
