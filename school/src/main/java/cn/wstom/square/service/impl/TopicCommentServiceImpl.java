package cn.wstom.square.service.impl;

import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.square.entity.TopicComment;
import cn.wstom.square.mapper.TopicCommentMapper;
import cn.wstom.square.service.TopicCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * @author liniec
 * @date 2020/01/26 12:24
 */
@Service
public class TopicCommentServiceImpl extends BaseServiceImpl<TopicCommentMapper, TopicComment> implements TopicCommentService {
    @Autowired
    private TopicCommentMapper commentMapper;
}
