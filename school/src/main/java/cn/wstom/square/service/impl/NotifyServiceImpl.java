package cn.wstom.square.service.impl;

import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.square.mapper.NotifyMapper;
import cn.wstom.square.service.NotifyService;
import cn.wstom.system.entity.Notify;
import cn.wstom.system.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 消息 服务层实现
 *
 * @author dws
 * @date 20190227
 */
@Service
public class NotifyServiceImpl extends BaseServiceImpl
        <NotifyMapper, Notify>
        implements NotifyService {

    @Resource
    private NotifyMapper notifyMapper;

    @Autowired
    protected SysUserService userService;
}
