package cn.wstom.square.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.square.data.PageVo;
import cn.wstom.square.entity.Question;
import cn.wstom.square.entity.QuestionCount;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author dws
 * @date 2019/03/07
 */
public interface QuestionService extends BaseService<Question> {

    /**
     * 按问题id关注或取消
     *
     * @param questionId
     * @param userId
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    @CacheEvict(value = "question", allEntries = true)
    int questionFollow(String questionId, String userId) throws Exception;

    /**
     * 按id删除该问题所信息
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    @CacheEvict(value = "question", allEntries = true)
    int deleteQuestionById(String id) throws Exception;

    int updateQuestionByViewCount(String id);

    @CacheEvict(value = "question", allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    boolean updateQuestionStatusById(String id, Integer status, Integer recommend) throws Exception;

    QuestionCount findQuestionCountById(String questionId);

    boolean checkQuestion(String id, Integer status);

    boolean checkQuestionFollow(String questionId, String userId);

    boolean checkQuestionByTitle(String title, String userId);

    int getQuestionCount(String title, String userId, String createTime, Integer status);

    PageVo<Question> getQuestionListPage(String title, String userId, String createTime, Integer status, String orderby, String order, int pageNum, int rows);

    int getQuestionIndexCount();

    /**
     * 分享索引列表
     *
     * @param pageNum
     * @param rows
     * @return
     */
    List<Question> getQuestionIndexList(int pageNum, int rows);
}
