package cn.wstom.square.service.impl;

import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.square.entity.Tag;
import cn.wstom.square.mapper.TagMapper;
import cn.wstom.square.service.TagService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 标签 服务层实现
 *
 * @author dws
 * @date 20190227
 */
@Service
public class TagServiceImpl extends BaseServiceImpl
        <TagMapper, Tag>
        implements TagService {

    @Resource
    private TagMapper tagMapper;
}
