package cn.wstom.square.mapper;

import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.square.entity.Tag;

/**
 * 标签 数据层
 *
 * @author dws
 * @date 20190227
 */
public interface TagMapper extends BaseMapper<Tag> {

}