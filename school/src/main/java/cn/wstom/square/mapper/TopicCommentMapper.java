package cn.wstom.square.mapper;

import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.square.entity.TopicComment;

public interface TopicCommentMapper extends BaseMapper<TopicComment> {
}
