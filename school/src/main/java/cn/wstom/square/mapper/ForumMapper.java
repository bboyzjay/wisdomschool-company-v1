package cn.wstom.square.mapper;

import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.square.entity.Forum;

/**
 * @author dws
 * @date 2019/03/31
 */
public interface ForumMapper extends BaseMapper<Forum> {
}
