package cn.wstom.square.mapper;


import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.square.entity.VideoChapterUser;
import cn.wstom.square.entity.VideoChapterUserVo;

/**
* 用户观看 数据层
*
* @author dws
* @date 20200204
*/
public interface VideoChapterUserVoMapper extends BaseMapper<VideoChapterUserVo> {
}