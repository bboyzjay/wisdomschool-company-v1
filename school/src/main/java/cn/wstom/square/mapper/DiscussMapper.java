package cn.wstom.square.mapper;

import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.square.entity.Discuss;

/**
 * @author dws
 * @date 2019/03/31
 */
public interface DiscussMapper extends BaseMapper<Discuss> {
}
