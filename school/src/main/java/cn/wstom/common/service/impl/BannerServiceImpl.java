package cn.wstom.common.service.impl;

import java.util.List;

import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.common.entity.Banner;
import cn.wstom.common.mapper.BannerMapper;
import cn.wstom.common.service.BannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* 轮播图片 服务层实现
*
* @author dws
* @date 20200131
*/
@Service
public class BannerServiceImpl extends BaseServiceImpl<BannerMapper, Banner> implements BannerService {

    @Autowired
    private BannerMapper bannerMapper;
}
