package cn.wstom.common.service.impl;

import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.common.entity.Comment;
import cn.wstom.common.mapper.CommentMapper;
import cn.wstom.common.service.CommentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author dws
 * @date 2019/03/07
 */
@Service
public class CommentServiceImpl extends BaseServiceImpl
        <CommentMapper, Comment>
        implements CommentService {

    @Resource
    private CommentMapper articleCommentMapper;

    @Override
    public List<Comment> listByPids(List<String> childrenIds) {
        return articleCommentMapper.listByPids(childrenIds);
    }
}
