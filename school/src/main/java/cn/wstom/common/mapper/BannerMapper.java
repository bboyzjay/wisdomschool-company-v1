package cn.wstom.common.mapper;

import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.common.entity.Banner;

/**
* 轮播图片 数据层
*
* @author dws
* @date 20200131
*/
public interface BannerMapper extends BaseMapper<Banner> {

}