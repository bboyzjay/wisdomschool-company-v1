package cn.wstom.jiaowu.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.jiaowu.entity.ChapterResource;

import java.util.List;

/**
 * 课程章节 服务层
 *
 * @author dws
 * @date 20190223
 */
public interface ChapterResourceService extends BaseService<ChapterResource> {

    /**
     * 根据学生id和课程id查找章节资源
     *
     * @param userId
     * @param courseId
     * @return
     */
    List<ChapterResource> selectByCidAndSid(String userId, String courseId, String chapterId);

    int saveBackId(ChapterResource chapterResource);
    boolean updateState(ChapterResource chapterResource);
    List<ChapterResource> selectTestPaperOneId(String id);
    List<ChapterResource> selectAttrId(ChapterResource chapterResource);
    int updatePptState(String name,String userId);
}
