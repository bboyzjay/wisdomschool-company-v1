package cn.wstom.jiaowu.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.jiaowu.entity.Grades;

/**
 * 年级 服务层
 *
 * @author xyl
 * @date 2019-01-22
 */
public interface GradesService extends BaseService<Grades> {
}
