package cn.wstom.jiaowu.service.impl;

import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.jiaowu.entity.Outline;
import cn.wstom.jiaowu.mapper.OutlineMapper;
import cn.wstom.jiaowu.service.OutlineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author dws
 * @date 2019/03/28
 */
@Service
public class OutlineServiceImpl extends BaseServiceImpl<OutlineMapper, Outline> implements OutlineService {

    @Autowired
    private OutlineMapper outlineMapper;
}
