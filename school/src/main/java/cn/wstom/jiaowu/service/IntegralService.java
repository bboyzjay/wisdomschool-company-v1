package cn.wstom.jiaowu.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.jiaowu.entity.Integral;

/**
 * @author dws
 * @date 2019/03/07
 */
public interface IntegralService extends BaseService<Integral> {
}
