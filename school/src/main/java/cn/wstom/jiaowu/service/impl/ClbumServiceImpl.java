package cn.wstom.jiaowu.service.impl;

import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.jiaowu.entity.Clbum;
import cn.wstom.jiaowu.mapper.ClbumMapper;
import cn.wstom.jiaowu.service.ClbumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 班级 服务层实现
 *
 * @author xyl
 * @date 2019-01-25
 */
@Service
public class ClbumServiceImpl extends BaseServiceImpl<ClbumMapper, Clbum>
        implements ClbumService {

    @Autowired
    private ClbumMapper clbumMapper;
}
