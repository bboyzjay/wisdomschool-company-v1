package cn.wstom.jiaowu.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.jiaowu.entity.Major;


/**
 * 专业 服务层
 *
 * @author xyl
 * @date 2019-01-25
 */
public interface MajorService extends BaseService<Major> {
}
