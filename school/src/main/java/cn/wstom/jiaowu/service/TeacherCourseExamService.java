package cn.wstom.jiaowu.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.jiaowu.entity.TeacherCourse;
import cn.wstom.jiaowu.entity.TeacherCourseExam;

/**
 * 教师课程 服务层
 *
 * @author hyb
 * @date 2019-01-29
 */
public interface TeacherCourseExamService extends BaseService<TeacherCourseExam> {
    TeacherCourseExam selectCourseId(String courseId);
}
