package cn.wstom.jiaowu.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.jiaowu.entity.SdsadminRole;

public interface SdsadminRoleService extends BaseService<SdsadminRole> {
    SdsadminRole selectBySid(Integer sid);
    int deleteBySid(Integer sid);
}
