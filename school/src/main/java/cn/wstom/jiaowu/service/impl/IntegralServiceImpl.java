package cn.wstom.jiaowu.service.impl;

import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.jiaowu.entity.Integral;
import cn.wstom.jiaowu.service.IntegralService;
import cn.wstom.square.mapper.IntegralMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 积分处理服务
 *
 * @author dws
 */
@Service
public class IntegralServiceImpl extends BaseServiceImpl
        <IntegralMapper, Integral>
        implements IntegralService {

    @Resource
    private IntegralMapper integralMapper;
}
