package cn.wstom.jiaowu.service.impl;

import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.jiaowu.entity.Major;
import cn.wstom.jiaowu.mapper.MajorMapper;
import cn.wstom.jiaowu.service.MajorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 专业 服务层实现
 *
 * @author xyl
 * @date 2019-01-25
 */
@Service
public class MajorServiceImpl extends BaseServiceImpl
        <MajorMapper, Major>
        implements MajorService {

    @Autowired
    private MajorMapper majorMapper;
}
