package cn.wstom.jiaowu.service.impl;

import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.jiaowu.data.SdsadminVo;
import cn.wstom.jiaowu.mapper.SdsadminVoMapper;
import cn.wstom.jiaowu.service.SdsadminVoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class SdsadminVoServiceImpl extends BaseServiceImpl<SdsadminVoMapper, SdsadminVo> implements SdsadminVoService {

    @Autowired
    private SdsadminVoMapper sdsadminVoMapper;
    @Override
    public List<SdsadminVo> selectBySdsadminVos(SdsadminVo sdsadminVo) {
        return sdsadminVoMapper.selectBySdsadminVo(sdsadminVo);
    }

    @Override
    public List<SdsadminVo> selectBySdsadminVo2(SdsadminVo sdsadminVo) {
        return sdsadminVoMapper.selectBySdsadminVo2(sdsadminVo);
    }


}
