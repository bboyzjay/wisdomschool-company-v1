package cn.wstom.jiaowu.service;


import cn.wstom.common.base.service.BaseService;
import cn.wstom.jiaowu.entity.Recourse;

import java.util.List;

/**
 * 资源 服务层
 *
 * @author dws
 * @date 20190222
 */
public interface RecourseService extends BaseService<Recourse> {

    Recourse selectByAttrId(String id);

    List<Recourse> getResourcesByType(String tcid, String type);

    List<Recourse> getResources(String studentId);
    List<Recourse> getResourcePpt(String studentId);
    List<Recourse> getResourceVideo(String studentId);
}
