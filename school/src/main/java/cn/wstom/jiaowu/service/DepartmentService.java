package cn.wstom.jiaowu.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.jiaowu.entity.Department;

/**
 * 系部service
 *
 * @author dws
 * @date 2019/01/14
 */
public interface DepartmentService extends BaseService<Department> {
}
