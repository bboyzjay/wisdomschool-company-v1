package cn.wstom.jiaowu.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.jiaowu.entity.Outline;

/**
 * @author dws
 * @date 2019/03/28
 */
public interface OutlineService extends BaseService<Outline> {
}
