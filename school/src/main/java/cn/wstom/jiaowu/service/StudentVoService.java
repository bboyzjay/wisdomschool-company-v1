package cn.wstom.jiaowu.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.jiaowu.data.StudentVo;

import java.util.List;

public interface StudentVoService extends BaseService<StudentVo> {
    List<StudentVo> selectByStudentVos(StudentVo studentVo);
}
