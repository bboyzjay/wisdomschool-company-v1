package cn.wstom.jiaowu.service.impl;

import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.jiaowu.entity.Student;
import cn.wstom.jiaowu.mapper.StudentMapper;
import cn.wstom.jiaowu.service.StudentService;
import cn.wstom.square.entity.Statistics;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author dws
 * @date 2019/01/05
 */
@Service
public class StudentServiceImpl extends BaseServiceImpl<StudentMapper, Student>
        implements StudentService {

    @Resource
    private StudentMapper studentMapper;

    @Override
    public Statistics statisticsById(String userId) {
        return studentMapper.statisticsById(userId);
    }
}
