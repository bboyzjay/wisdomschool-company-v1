package cn.wstom.jiaowu.service.impl;

import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.jiaowu.entity.Lead;
import cn.wstom.jiaowu.mapper.LeadMapper;
import cn.wstom.jiaowu.service.LeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author dws
 * @date 2019/03/28
 */
@Service
public class LeadServiceImpl extends BaseServiceImpl<LeadMapper, Lead> implements LeadService {

    @Autowired
    private LeadMapper leadMapper;
}
