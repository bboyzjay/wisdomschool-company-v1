package cn.wstom.jiaowu.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.jiaowu.entity.Clbum;

/**
 * 班级 服务层
 *
 * @author dws
 * @date 2019-01-25
 */
public interface ClbumService extends BaseService<Clbum> {
}
