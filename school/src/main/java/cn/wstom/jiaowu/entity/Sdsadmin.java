package cn.wstom.jiaowu.entity;

import cn.wstom.common.base.entity.BaseEntity;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
public class Sdsadmin extends BaseEntity {
    private static final long serialVersionUID = -4989759857194731384L;
    private int grades;
    private int department;
    private int rid;
}
