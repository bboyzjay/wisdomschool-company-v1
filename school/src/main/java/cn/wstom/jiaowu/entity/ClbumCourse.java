package cn.wstom.jiaowu.entity;

import cn.wstom.common.base.entity.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import lombok.Data;

/**
 * 班级课程表 tb_clbum_course
 *
 * @author hyb
 * @date 20190218
 */
@Data
public class ClbumCourse extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 教师课程id
     */
    private String tcid;
    /**
     * 班级id
     */
    private String cid;
    /**
     * 年级id
     */
    private String gid;

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("tcid", getTcid())
                .append("cid", getCid())
                .append("gid", getGid())
                .toString();
    }
}
