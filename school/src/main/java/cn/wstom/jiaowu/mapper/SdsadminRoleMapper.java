package cn.wstom.jiaowu.mapper;

import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.jiaowu.entity.Sdsadmin;
import cn.wstom.jiaowu.entity.SdsadminRole;

public interface SdsadminRoleMapper extends BaseMapper<SdsadminRole> {
    SdsadminRole selectBySid(Integer sid);
    int deleteBySid(Integer sid);
}
