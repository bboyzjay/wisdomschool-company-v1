package cn.wstom.jiaowu.mapper;
import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.jiaowu.entity.Outpeizhi;
/**
 * @author zzr
 * @date 2019/03/28
 */
public interface OutpeizhiMapper extends BaseMapper<Outpeizhi> {
}