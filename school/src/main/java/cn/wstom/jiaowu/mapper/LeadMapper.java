package cn.wstom.jiaowu.mapper;

import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.jiaowu.entity.Lead;

/**
 * @author dws
 * @date 2019/03/28
 */
public interface LeadMapper extends BaseMapper<Lead> {
}
