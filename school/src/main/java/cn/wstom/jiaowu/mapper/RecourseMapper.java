package cn.wstom.jiaowu.mapper;


import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.jiaowu.entity.Recourse;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 资源 数据层
 *
 * @author dws
 * @date 20190222
 */
public interface RecourseMapper extends BaseMapper<Recourse> {
    List<Recourse> getResources(@Param("studentId") String studentId);
    List<Recourse> getResourcePpt(@Param("studentId") String studentId);
    List<Recourse> getResourceVideo(@Param("studentId") String studentId);
}