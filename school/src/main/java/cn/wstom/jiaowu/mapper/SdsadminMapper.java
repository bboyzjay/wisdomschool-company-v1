package cn.wstom.jiaowu.mapper;

import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.jiaowu.entity.Sdsadmin;

public interface SdsadminMapper extends BaseMapper<Sdsadmin> {
}
