package cn.wstom.jiaowu.mapper;


import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.jiaowu.entity.ChapterResource;
import cn.wstom.jiaowu.entity.CourseResource;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 课程章节 数据层
 *
 * @author dws
 * @date 20190223
 */
public interface CourseResourceMapper extends BaseMapper<CourseResource> {

    List<CourseResource> selectByCidAndSid(@Param("userId") String userId,
                                            @Param("courseId") String courseId
                                           );

    int insertBackId(CourseResource courseResource);

}