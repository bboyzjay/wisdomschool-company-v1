package cn.wstom.jiaowu.mapper;

import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.jiaowu.entity.ClbumCourse;

/**
 * 班级课程 数据层
 *
 * @author hyb
 * @date 20190218
 */
public interface ClbumCourseMapper extends BaseMapper<ClbumCourse> {

}