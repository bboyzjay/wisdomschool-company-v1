package cn.wstom.jiaowu.mapper;

import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.jiaowu.entity.Major;
/**
 * 专业 数据层
 *
 * @author xyl
 * @date 2019-01-25
 */
public interface MajorMapper extends BaseMapper<Major> {

}