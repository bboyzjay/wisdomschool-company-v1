package cn.wstom.jiaowu.mapper;

import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.jiaowu.entity.Department;

/**
 * 系部mapper
 *
 * @author dws
 * @date 2019/01/14
 */
public interface DepartmentMapper extends BaseMapper<Department> {
}
