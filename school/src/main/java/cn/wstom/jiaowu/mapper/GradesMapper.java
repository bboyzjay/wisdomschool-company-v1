package cn.wstom.jiaowu.mapper;


import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.jiaowu.entity.Grades;

/**
 * 年级 数据层
 *
 * @author xyl
 * @date 2019-01-22
 */
public interface GradesMapper extends BaseMapper<Grades> {

}