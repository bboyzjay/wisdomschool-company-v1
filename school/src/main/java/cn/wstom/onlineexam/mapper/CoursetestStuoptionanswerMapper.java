package cn.wstom.onlineexam.mapper;


import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.onlineexam.entity.CoursetestStuoptionanswer;

/**
 * 学生答案（课程测试） 数据层
 *
 * @author dws
 * @date 20190315
 */
public interface CoursetestStuoptionanswerMapper extends BaseMapper<CoursetestStuoptionanswer> {
    int updateByIdAns(CoursetestStuoptionanswer coursetestStuoptionanswer);

}