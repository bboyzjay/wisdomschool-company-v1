package cn.wstom.onlineexam.mapper;

import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.onlineexam.entity.TestpaperOneTestquestions;

/**
 * 试卷做的题目答案 数据层
 *
 * @author
 * @date
 */
public interface TestpaperOneTestquestionsMapper extends BaseMapper<TestpaperOneTestquestions> {
}
