package cn.wstom.onlineexam.mapper;


import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.onlineexam.entity.TestpaperOptinanswer;

/**
 * 试卷答案 数据层
 *
 * @author hzh
 * @date 20190223
 */
public interface TestpaperOptinanswerMapper extends BaseMapper<TestpaperOptinanswer> {

}