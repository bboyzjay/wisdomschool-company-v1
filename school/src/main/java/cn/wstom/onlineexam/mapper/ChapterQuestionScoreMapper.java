package cn.wstom.onlineexam.mapper;

import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.onlineexam.entity.ChapterQuestionScore;

/**
* 分数 数据层
*
* @author hzh
* @date 20190307
*/
public interface ChapterQuestionScoreMapper extends BaseMapper<ChapterQuestionScore> {

}