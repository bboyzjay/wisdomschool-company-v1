package cn.wstom.onlineexam.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.onlineexam.entity.CoursepaperCoursequestions;

/**
 * 试卷做的题目答案 服务层
 *
 * @author hzh
 * @date 20190223
 */
public interface CoursepaperCoursequestionsService extends BaseService<CoursepaperCoursequestions> {
}
