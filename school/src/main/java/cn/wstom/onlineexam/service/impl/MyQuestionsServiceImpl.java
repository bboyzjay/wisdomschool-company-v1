package cn.wstom.onlineexam.service.impl;


import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.onlineexam.entity.MyQuestions;
import cn.wstom.onlineexam.mapper.MyQuestionsMapper;
import cn.wstom.onlineexam.service.MyQuestionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MyQuestionsServiceImpl extends BaseServiceImpl<MyQuestionsMapper, MyQuestions> implements MyQuestionsService {

    @Autowired
    MyQuestionsMapper myQuestionsMapper;

    /**
     * 按条件查询问题
     */
    public List<MyQuestions> selectList(MyQuestions myQuestions){
        return  myQuestionsMapper.selectList(myQuestions);
    }
    /**
     * 查询未整理的代码
     * @param myQuestions
     * @return
     */
    @Override
    public List<MyQuestions> unList(MyQuestions myQuestions) {
        return myQuestionsMapper.unList(myQuestions);
    }
    /**
     * 由Id查询未整理的代码
     * @param id
     * @return
     */
    @Override
    public MyQuestions getUnListById(String id) {
        return myQuestionsMapper.getUnListById(id);
    }

    /**
     * 查询一门科目中的一种题型的全部试题
     *
     * @param titleTypeId
     * @param xzsubjectsId
     * @return
     */
    @Override
    public List<MyQuestions> selectQuestionByTid(String titleTypeId, String xzsubjectsId) {
        return myQuestionsMapper.selectQuestionByTid(titleTypeId, xzsubjectsId);
    }
}

