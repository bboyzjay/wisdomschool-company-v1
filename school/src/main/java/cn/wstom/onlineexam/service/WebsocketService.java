package cn.wstom.onlineexam.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.onlineexam.entity.Websocket;

/**
* 存放考试的临时 服务层
*
* @author dws
* @date 20190308
*/
public interface WebsocketService extends BaseService<Websocket> {
}
