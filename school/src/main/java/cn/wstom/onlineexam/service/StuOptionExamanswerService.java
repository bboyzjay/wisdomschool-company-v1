package cn.wstom.onlineexam.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.onlineexam.entity.OptionExamSubmitVo;
import cn.wstom.onlineexam.entity.OptionSubmitVo;
import cn.wstom.onlineexam.entity.StuOptionExamanswer;
import cn.wstom.onlineexam.entity.StuOptionanswer;

import java.util.List;

/**
 * 学生考试答案 服务层
 *
 * @author hzh
 * @date 20190304
 */
public interface StuOptionExamanswerService extends BaseService<StuOptionExamanswer> {
    int updateByIdAns(StuOptionExamanswer stuOptionanswer);

    /**
     * 保存学生做题记录
     * @param options
     * @param studentId
     * @return
     */
    int saveOptionAnswers(List<OptionExamSubmitVo> options, String studentId);

    /**
     * 更新做题的分数以及总分
     * @param optionanswers
     * @param testPaperId
     * @return
     */
    int updateListAndTotalScore(List<StuOptionExamanswer> optionanswers, String testPaperId, String studentId);

    int saveOptionAnswersByUserId(List<OptionExamSubmitVo> options, String userId);
}
