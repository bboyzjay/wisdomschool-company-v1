package cn.wstom.onlineexam.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.onlineexam.entity.TestpaperOptinanswer;

/**
 * 试卷答案 服务层
 *
 * @author hzh
 * @date 20190223
 */
public interface TestpaperOptinanswerService extends BaseService<TestpaperOptinanswer> {
}
