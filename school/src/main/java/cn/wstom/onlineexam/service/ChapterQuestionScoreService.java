package cn.wstom.onlineexam.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.onlineexam.entity.ChapterQuestionScore;

/**
* 分数 服务层
*
* @author hzh
* @date 20190307
*/
public interface ChapterQuestionScoreService extends BaseService<ChapterQuestionScore> {
}
