package cn.wstom.onlineexam.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.onlineexam.entity.TestChapterPaper;

/**
* 节的试卷 服务层
*
* @author dws
* @date 20190314
*/
public interface TestChapterPaperService extends BaseService<TestChapterPaper> {
}
