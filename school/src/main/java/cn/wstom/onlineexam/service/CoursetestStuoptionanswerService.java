package cn.wstom.onlineexam.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.onlineexam.entity.CoursetestStuoptionanswer;

/**
 * 学生答案（课程测试） 服务层
 *
 * @author dws
 * @date 20190315
 */
public interface CoursetestStuoptionanswerService extends BaseService<CoursetestStuoptionanswer> {
    int updateByIdAns(CoursetestStuoptionanswer coursetestStuoptionanswer);

}
