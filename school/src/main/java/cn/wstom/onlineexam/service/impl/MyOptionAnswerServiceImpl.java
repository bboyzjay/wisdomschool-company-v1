package cn.wstom.onlineexam.service.impl;



import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.onlineexam.entity.MyOptionAnswer;
import cn.wstom.onlineexam.mapper.MyOptionAnswerMapper;
import cn.wstom.onlineexam.service.MyOptionAnswerService;
import org.springframework.stereotype.Service;

@Service
public class MyOptionAnswerServiceImpl extends BaseServiceImpl<MyOptionAnswerMapper, MyOptionAnswer> implements MyOptionAnswerService {



}
