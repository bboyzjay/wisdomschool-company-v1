package cn.wstom.onlineexam.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.onlineexam.entity.TestPaperOne;
import cn.wstom.system.entity.SysUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 试卷 服务层
 *
 * @author hzh
 * @date 20190223
 */
public interface TestPaperOneService extends BaseService<TestPaperOne> {



    boolean updateWithoutCheckType(TestPaperOne entity) throws Exception;
    /**
     * 校验标题名唯一
     *
     * @param testPaper
     * @return
     */
    String checkTestNameUnique(TestPaperOne testPaper);

    /**
     * 查找年级
     *
     * @param teacherId
     * @return
     */
    List<TestPaperOne> getStudentInfo(@Param("teacherId") String teacherId);

    /**
     * 获取科目
     *
     * @param teacherId
     * @return
     */
    List<TestPaperOne> getTcoName(@Param("teacherId") String teacherId);
    boolean updateSetExam(TestPaperOne testPaper);
    /**
     * 获取学生
     *
     * @param teacherId
     * @return
     */
    List<TestPaperOne> getTcoStu(@Param("teacherId") String teacherId);


    /**
     * 查找学生试卷
     * @param testPaper
     * @return
     */
    List<TestPaperOne> findStuPaper(TestPaperOne testPaper);


    /**
     * 获取作业资源和相关班级的目录
     * @return
     */
    List<Map<String, Object>> getTestPagerAndClbumTree(SysUser user, String cId, String pageType);
}
