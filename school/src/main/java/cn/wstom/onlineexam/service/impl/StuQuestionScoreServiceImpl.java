package cn.wstom.onlineexam.service.impl;



import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.onlineexam.entity.StuQuestionScore;
import cn.wstom.onlineexam.mapper.StuQuestionScoreMapper;
import cn.wstom.onlineexam.service.StuQuestionScoreService;
import org.springframework.stereotype.Service;


/**
* 分数 服务层实现
*
* @author hzh
* @date 20190307
*/
@Service
public class StuQuestionScoreServiceImpl extends BaseServiceImpl
        <StuQuestionScoreMapper, StuQuestionScore>
implements StuQuestionScoreService {

}
