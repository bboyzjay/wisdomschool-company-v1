package cn.wstom.web.controller.exercise;

import cn.wstom.common.base.Data;
import cn.wstom.jiaowu.entity.Shuati;
import cn.wstom.main.web.base.BaseController;
import cn.wstom.onlineexam.entity.*;
import cn.wstom.onlineexam.service.*;
import cn.wstom.web.core.Uuid;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author liniec
 * @date 2019/12/22 00:42
 *  章节练习
 *
 *  特别人才的拼音类名
 *  ps: 不是我写的
 */

@RestController
@RequestMapping("/exercise")
public class ChapterExeController extends BaseController {

    private final String SUBMIT_SUCCESS = "1";
    private final String SUBMIT_FAILURE = "0";

    @Autowired
    private TestpaperTestquestionsService testpaperTestquestionsService;
    @Autowired
    private TestpaperOneTestquestionsService testpaperOneTestquestionsService;
    @Autowired
    private TestpaperOptinanswerService testpaperOptinanswerService;
    @Autowired
    private TestpaperQuestionsService testpaperQuestionsService;
    @Autowired
    private StuOptionanswerService stuOptionanswerService;
    @Autowired
    private StuOptionExamanswerService stuOptionExamanswerService;
    @Autowired
    private MyQuestionsService myQuestionsService;

    @Autowired
    private MyOptionAnswerService myOptionAnswerService;

    @Autowired
    private ShuatiHistoryService shuatiHistoryService;

    @ApiOperation("章节题目信息")
    @RequestMapping(method = RequestMethod.GET, value = "/info/{chapterId}")
    public Data exeInfo(@PathVariable(value = "chapterId") String chapterId) {
        return Data.success();
    }

    @ApiOperation("章节题目")
    @RequestMapping(method = RequestMethod.GET, value = "/{chapterId}")
    public Data question(@PathVariable(value = "chapterId") String chapterId) {
        MyQuestions questions = new MyQuestions();
//        questions.setChapterId(chapterId); //chapterId 为 pId字段
        questions.setJchapterId(chapterId);
        List<MyQuestions> list = myQuestionsService.selectList(questions);

        List<Shuati> shuatis = new ArrayList<>();
        Shuati index;
        for (MyQuestions questions1 : list) {
            String[] answerStr = questions1.getMyoptionAnswerArr().split(";");
            List<MyOptionAnswer> answerList = new ArrayList<>();
            for (String answer :  answerStr) {
                answerList.add(myOptionAnswerService.getById(answer));
            }
            index = new Shuati();
            index.setMyQuestions(questions1);
            index.setMyOptionAnswerList(answerList);
            shuatis.add(index);
        }
        Map<String, Object> result = new HashMap<>();
        result.put("exercises", shuatis);
        return Data.success(result);
    }

    @ApiOperation("章节刷题提交")
    @RequestMapping(method = RequestMethod.POST, value = "/submit")
    public Data submit(@RequestBody List<OptionSubmitVo> optionSubmitVos) {
//        for (ShuatiHistory s : histories) {
//            s.setUserId(getUserId());
//        }

        System.out.println("章节刷题提交" + optionSubmitVos.toString());

//        try {
//            shuatiHistoryService.saveBatch(histories);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return Data.success(SUBMIT_FAILURE);
//        }

        return Data.success(SUBMIT_SUCCESS);
    }

    @ApiOperation("章节试卷")
    @RequestMapping(value = "/test/{paperId}", method = RequestMethod.GET)
    @ResponseBody
    public Data chapterExam(@PathVariable(value = "paperId") String paperId) {
        List<TestpaperQuestions> questions;
        questions = testpaperTestquestionsService.getQuestionsByPaperIdWithUserId(paperId, getUserId());
        return Data.success(questions);
    }
    @ApiOperation("章节考试")
    @RequestMapping(value = "/testOne/{paperId}", method = RequestMethod.GET)
    @ResponseBody
    public Data chapterExamOne(@PathVariable(value = "paperId") String paperId) {
        List<TestpaperQuestions> questions;
        questions = testpaperOneTestquestionsService.getQuestionsByPaperIdWithUserId(paperId, getUserId());
        return Data.success(questions);
    }
    @ApiOperation("章节试题提交")
    @RequestMapping(method = RequestMethod.POST, value = "/test/submit")
    @ResponseBody
    public Data saveChapterTestAnswer(@RequestBody List<OptionSubmitVo> options) {
        options.forEach(o -> {
            Uuid uuid = new Uuid();
            o.setuUid(uuid.UId());
            o.setScore(10);//TODO: 暂时
        });
        return toAjax(stuOptionanswerService.saveOptionAnswersByUserId(options, getUserId()));
    }
    @ApiOperation("章节考试提交")
    @RequestMapping(method = RequestMethod.POST, value = "/exam/submit")
    @ResponseBody
    public Data saveChapterExamAnswer(@RequestBody List<OptionExamSubmitVo> options) {
        options.forEach(o -> {
            Uuid uuid = new Uuid();
            o.setuUid(uuid.UId());
            o.setScore(10);//TODO: 暂时
        });
        return toAjax(stuOptionExamanswerService.saveOptionAnswersByUserId(options, getUserId()));
    }
}
