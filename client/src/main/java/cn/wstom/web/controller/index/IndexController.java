package cn.wstom.web.controller.index;

import cn.wstom.common.base.Data;
import cn.wstom.common.entity.Banner;
import cn.wstom.common.service.BannerService;
import cn.wstom.main.web.base.BaseController;
import cn.wstom.square.entity.Article;
import cn.wstom.square.service.ArticleService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class IndexController extends BaseController {

    @Autowired
    private BannerService bannerService;
    @Autowired
    private ArticleService articleService;

    @ApiOperation("首页数据")
    @RequestMapping(method = RequestMethod.GET, value = "/home")
    public Data home() {
        startPage();
        List<Banner> bannerList = bannerService.list(new Banner());

        Map<String, Object> map = new HashMap<>();
        map.put("banner", bannerList);

        startPage();
        List<Article> articles = articleService.list(new Article());
        map.put("articles", articles);

        return Data.success(map);
    }
}
