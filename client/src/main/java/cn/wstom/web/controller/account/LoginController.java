package cn.wstom.web.controller.account;

import cn.wstom.common.base.Data;
import cn.wstom.common.constant.Constants;
import cn.wstom.common.utils.KeyUtil;
import cn.wstom.main.util.ShiroUtils;
import cn.wstom.main.web.base.BaseController;
import cn.wstom.system.data.LoginInfo;
import cn.wstom.system.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author liniec
 * @date 2019/11/06 22:25
 */
@RestController
public class LoginController extends BaseController {



    /**
     * 登陆超时时间：30s
     */
    private static final long TIME_OUT = 30 * 1000;

    @Autowired
    private SysUserService sysUserService;

    @PostMapping("/login")
    @ApiOperation("登录请求")
    @ResponseBody
    public Data login(String account, String password) {
//        final String loginInfo = DecryptionUtil.decryption(encrypted, KeyUtil.getPrivateKey());
        LoginInfo loginInfo = new LoginInfo();
        loginInfo.setUsername(account);
        loginInfo.setPassword(password.toCharArray());

        if (account == null) {
            return Data.error(Constants.FAILURE, "用户名和密码不能为空！");
        }
//        if (System.currentTimeMillis() - Long.parseLong(loginInfo.getTime()) > TIME_OUT) {
//            return Data.error(Constants.FAILURE, "登陆超时");
//        }
        UsernamePasswordToken token = new UsernamePasswordToken(loginInfo.getUsername(), loginInfo.getPassword(), true);
        Subject subject = SecurityUtils.getSubject();
        try {
            subject.login(token);
            return Data.success(Constants.SUCCESS, Constants.LOGIN_SUCCESS, subject.getSession().getId());
        } catch (AuthenticationException e) {
            String msg = "用户或密码错误";
            if (StringUtils.isNotEmpty(e.getMessage())) {
                msg = e.getMessage();
            }
            return error(Constants.FAILURE, msg);
        }
    }

    @RequestMapping("/logout")
    @ApiOperation("登出")
    @ResponseBody
    public Data logout() {
        Subject currentUser = SecurityUtils.getSubject();
        if (SecurityUtils.getSubject().getSession() != null)
            currentUser.logout();
        return Data.success();
    }

    @RequestMapping("/publicKey")
    @ApiOperation("加密编码")
    @ResponseBody
    public Data getPublicKey() {
        return Data.success().put("k", KeyUtil.getPublicKey()).put("t", System.currentTimeMillis());
    }

    @GetMapping("/remind")
    @ApiOperation("未登录")
    @ResponseBody
    public Data checkSign() {
        return Data.error(Constants.FAILURE, Constants.LOGIN_FAIL);
    }
}
